/**
 * docsify插件 - 左侧文件子导航浮动到右侧显示
 * ********* 使用方法 *******
 * 只需在对应的md文件中加入[ sub-nav-fix ] 标签
 * 该页面的二级导航即可漂浮到内容区域右侧
 * 前提：配置参数中开启二级导航 subMaxLevel: 2,
 */

(function() {

    // 添加二级导航右侧浮动CSS样式
    function addFixnavStyle() {
        const styleCssCode = `.sidebar-nav-fix li.collapse .app-sub-sidebar{display:block;}.sidebar-nav p.active>a{font-weight:bolder;color:#42b983}.sidebar-nav-fix .app-sub-sidebar{position:fixed;right:50%;top:160px;bottom:100px;margin-right:-820px;overflow-y:auto;overflow-x:hidden;width:200px;}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar{position:relative;margin:0;left:0;top:0;bottom:0;}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar li{position:relative;}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar li:before{position:absolute;left:21px;content:"-";}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar li a{padding-left:35px;}.sidebar-nav-fix .app-sub-sidebar::-webkit-scrollbar{width:4px;}.sidebar-nav-fix .app-sub-sidebar::-webkit-scrollbar-thumb{background:transparent;border-radius:4px;}.sidebar-nav-fix .app-sub-sidebar:hover::-webkit-scrollbar-thumb{background:hsla(0,0%,53%,0.4);}.sidebar-nav-fix .app-sub-sidebar:hover::-webkit-scrollbar-track{background:hsla(0,0%,53%,0.1);}.sidebar-nav-fix .app-sub-sidebar li{margin:0;padding-right:10px;border-left:solid 1px #e0e0e0;}.sidebar-nav-fix .app-sub-sidebar li:before{content:"";}.sidebar-nav-fix .app-sub-sidebar li a{text-align:left;padding-left:20px;}.sidebar-nav-fix .app-sub-sidebar li.active>a{border-left:1px solid;border-right:0 solid;margin-left:-1px;}`;
        const styleEl = document.createElement("style");
        styleEl.appendChild(document.createTextNode(styleCssCode));
        const headEl = document.getElementsByTagName("head")[0];
        headEl.appendChild(styleEl);
    };
    addFixnavStyle();

    function plugin(hook, vm) {

        hook.beforeEach(function(content, next) {
            // 每次开始解析 Markdown 内容时调用

            // 判断当前markdown文档是否有[sub-nav-fix]标签
            let isFixtag = content.search(/\[.*(sub-nav-fix)+.*\]/g);
            let sidebarFixnav = document.querySelector(".sidebar-nav");

            if (isFixtag >= 0) {
                content = content.replace(/\[.*(sub-nav-fix)+.*\]/g, "");
                sidebarFixnav.classList.add("sidebar-nav-fix");
            } else {
                sidebarFixnav.classList.remove("sidebar-nav-fix");
            }

            next(content);
        })
    }

    window.$docsify.plugins = window.$docsify.plugins.concat(plugin);

})();