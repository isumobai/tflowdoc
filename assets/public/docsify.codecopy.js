/**
 * docsify插件: 为代码块添加复制功能
 */
(function() {
    function pubilc(hook) {
        hook.doneEach(function() {
            // 添加复制按钮
            $(".markdown-section pre[v-pre]").each(function() {
                $(this).append('<span class="btn-code-copy">[copy]</span>')
            });

            // 点击复制代码
            $(".markdown-section pre[v-pre]").on("click", ".btn-code-copy", function() {
                const $this = $(this);
                const parent = $this.parent();
                const ind = parent.find("tab[active]").index();
                const text = $this.siblings('code[class*="lang"]').text().trim();

                copyFn(text, function(val) {
                    $this.addClass("finish");
                    setTimeout(() => {
                        $this.removeClass("finish");
                    }, 800);
                })
            });

            // 复制Fn
            function copyFn(text, callback) {
                var inputEl = document.createElement("input");
                document.body.appendChild(inputEl);
                inputEl.setAttribute("value", text);
                inputEl.select();
                if (document.execCommand("copy")) {
                    document.execCommand("copy");
                    if (callback) callback(text)
                }
                document.body.removeChild(inputEl)
            };


            /**
             * 代码预览效果
             */
            const previewTab = $('div[type="preview-tab"]');
            // 默认显示第一个选项卡
            previewTab.each(function() {
                $(this).find("tab").eq(0).attr("active", "");
            });
            // 选项卡点击切换效果
            previewTab.find("tab").click(function() {
                const $this = $(this);
                const ind = $this.index();
                const parent = $this.parent();
                const length = parent.find("tab").length;

                $this.siblings().removeAttr("active");
                $this.attr("active", "");

                for (let i = 0; i < length; i++) {
                    parent.nextAll('div[type="preview-code"]').eq(i).hide();
                }

                parent.nextAll('div[type="preview-code"]').eq(ind).show();
            });

        });
    }

    window.$docsify.plugins = window.$docsify.plugins.concat(pubilc);
})();