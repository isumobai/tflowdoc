/**
 * docsify插件: 加载远程MD文档
 */
(function() {

    /*
     * ajax get方法Promise封装
     */
    const ajaxGetPromise = (url) => {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.send(null);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.responseText);
                    } else {
                        reject()
                    }
                }
            }
        })
    }


    function pubilc(hook) {
        // 每次开始解析 Markdown 内容时调用
        hook.beforeEach(function(content, next) {

            // 是否开启浮动导航
            let hasLoadMdTag = content.match(/\[.*(load-md).*\]:\s*\S*\s*\n/gi);
            if (!hasLoadMdTag) {
                next(content);
                return false;
            }

            let url = hasLoadMdTag ? hasLoadMdTag[0].replace(/\[.*(load-md).*\]:\s*\((.*)\)\s*\n+/, "$2") : "";
            let isurl = /^[(http)|(https)]+:\/\/[^\s]*/i.test(url);

            if (!isurl) {
                let errHtml = `<div style="color:red;padding:60px 0;">ERROR: 加载远程MarkDown文档URl地址格式不正确!<br>( ${url} ) </div>`;
                content = content.replace(/\[.*(load-md).*\]:\s*\S*\s*\n/gi, errHtml);
                next(content);
                return false;
            }

            ajaxGetPromise(url).then(res => {
                let mdContent = '\n\n' + marked(res) + '\n\n';
                content = content.replace(/\[.*(load-md).*\]:\s*\S*\s*\n/gi, mdContent);
                next(content);
            }).catch(err => {
                let errHtml = `<div style="color:red;padding:60px 0;">ERROR: 加载远程MarkDown文档失败!<br>( ${url} )</div>`;
                content = content.replace(/\[.*(load-md).*\]:\s*\S*\s*\n/gi, errHtml);
                next(content);
            })
        })
    }

    window.$docsify.plugins = window.$docsify.plugins.concat(pubilc);
})();