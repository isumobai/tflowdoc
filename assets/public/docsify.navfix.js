/**
 * docsify插件: 二级导航浮动到右侧
 */
(function() {
    // 添加二级导航右侧浮动CSS样式
    // @isAdd {Boolean} 是否开启导航
    function addFixnavStyle(isAdd) {
        let sidebarFixnav = document.querySelector(".sidebar-nav");
        isAdd ? sidebarFixnav.classList.add("sidebar-nav-fix") : sidebarFixnav.classList.remove("sidebar-nav-fix");
        if (!isAdd) return false;

        // 添加导航样式
        const fixnavEl = document.querySelector("style[name='fixednav']");
        if (!fixnavEl) {
            const styleCssCode = `.sidebar-nav-fix li.collapse .app-sub-sidebar{display:block;}.sidebar-nav p.active>a{font-weight:bold;color:#42b983}.sidebar-nav-fix .app-sub-sidebar{position:fixed;right:50%;top:160px;bottom:100px;margin-right:-820px;overflow-y:auto;overflow-x:hidden;width:200px;}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar{position:relative;margin:0;left:0;top:0;bottom:0;}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar li{position:relative;}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar li:before{position:absolute;left:21px;content:"-";}.sidebar-nav-fix .app-sub-sidebar .app-sub-sidebar li a{padding-left:35px;}.sidebar-nav-fix .app-sub-sidebar::-webkit-scrollbar{width:4px;}.sidebar-nav-fix .app-sub-sidebar::-webkit-scrollbar-thumb{background:transparent;border-radius:4px;}.sidebar-nav-fix .app-sub-sidebar:hover::-webkit-scrollbar-thumb{background:hsla(0,0%,53%,0.4);}.sidebar-nav-fix .app-sub-sidebar:hover::-webkit-scrollbar-track{background:hsla(0,0%,53%,0.1);}.sidebar-nav-fix .app-sub-sidebar li{margin:0;padding-right:10px;border-left:solid 1px #e0e0e0;}.sidebar-nav-fix .app-sub-sidebar li:before{content:"";}.sidebar-nav-fix .app-sub-sidebar li a{font-size:14px;display:inline-table;text-align:left;padding:0 10px 0 20px;}.sidebar-nav-fix .app-sub-sidebar li.active>a{border-left:1px solid;border-right:0 solid;margin-left:-1px;}`;
            const styleEl = document.createElement("style");
            styleEl.setAttribute("name", "fixednav");
            styleEl.appendChild(document.createTextNode(styleCssCode));
            const headEl = document.getElementsByTagName("head")[0];
            headEl.appendChild(styleEl);
        }
    };

    function pubilc(hook) {
        // 每次开始解析 Markdown 内容时调用
        hook.beforeEach(function(content, next) {

            // 是否开启浮动导航
            let hasFixedNavTag = content.match(/\[.*(fixed-nav).*\]/ig);
            let isFixedNavTag = window.$docsify && window.$docsify.fixedNavTag || false;
            let isFixedNav = hasFixedNavTag && isFixedNavTag;
            addFixnavStyle(isFixedNav);

            next(content)
        })
    }

    window.$docsify.plugins = window.$docsify.plugins.concat(pubilc);
})();