(async function Start() {

    // 导入配置文件
    if (typeof $config == "undefined") await $.getScript("/public/quickQuery/_config.js");

    let list = $config.list;
    const locHost = window.location.host;
    let navHtml = "";
    let iframeListHtml = "";
    let tagsHtml = "";

    function handSelfDoc() {
        const iframeBox = $(".quick-query-wrap").find(".qkq-iframe-box[data-self='true']");
        iframeBox.each(function() {
            let inter = setInterval(() => {
                let doc = $(this).find("iframe").contents();
                doc.find(".sidebar,.sidebar-toggle").css("display", "none");
                doc.find(".content").css("left", "0");
                if (doc.find(".content").length > 0) clearInterval(inter);
            }, 300);
        });
    }

    list.forEach(item => {
        navHtml += '<div class="qkq-nav-title">' + item.title + '</div>';
        item.data.forEach((item2, ind) => {
            const target = item2.target ? " target=" + item2.target : "";
            const href = item2.target ? " href=" + item2.url : "";
            const weight = " data-weight=" + Math.floor(Math.random() * 10);
            const dataSelf = item2.url.search(locHost) > -1 || item2.self == "true" ? " data-self='true'" : "";
            const navliHtml = '<a class="qkq-nav-li" data-name="' + item2.name + '" ' + href + target + weight + '>' + item2.name + '</a>';
            navHtml += navliHtml;
            tagsHtml += navliHtml;

            if (item2.target != "_blank") iframeListHtml += '<div class="qkq-iframe-box" style="display:none;" data-name="' + item2.name + '"' + dataSelf + '><iframe src="' + item2.url + '" frameborder="0"></iframe></div>';
        });
    });

    let qkqHtml = '<div class="qkq-tags">' + tagsHtml + '</div>';
    qkqHtml += '<div class="qkq-nav-wrap">' + navHtml + '</div>';
    qkqHtml += '<div class="qkq-iframe-wrap">' + iframeListHtml + '</div>';
    $('.quick-query-wrap').append(qkqHtml);

    handSelfDoc();

    $(".qkq-nav-wrap .qkq-nav-li").click(function() {
        const $this = $(this);
        const target = $this.attr("target");
        if (target == "_blank") return true;

        $(".qkq-tags").remove();

        const name = $this.attr("data-name");
        const iframeBox = $(".quick-query-wrap").find(".qkq-iframe-box[data-name='" + name + "']");
        iframeBox.show().siblings(".qkq-iframe-box").hide();
        $this.addClass("active").siblings().removeClass("active");
    });

    $(".qkq-tags a").click(function() {
        const name = $(this).attr("data-name");
        $(".qkq-nav-wrap .qkq-nav-li[data-name='" + name + "']").click();
        $(".qkq-tags").remove();
        $(".qkq-loading").show();
    });

})();