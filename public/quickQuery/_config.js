$config = {
    list: [{
            title: "通用",
            data: [
                { name: "VUE2.X", url: "https://cn.vuejs.org" },
                { name: "VueRouter", url: "https://router.vuejs.org/zh/" },
                { name: "ElementUI2.X", url: "https://element.eleme.cn/#/zh-CN" },
                { name: "Iconfont", url: "http://localhost:5000/#/public/iconfont/iconfont" },
                { name: "工具函数", url: "http://localhost:5000/#/docs/utils" },
                { name: "兼容性速查", url: "https://caniuse.com" }
            ]
        },
        {
            title: "JS相关",
            data: [
                { name: "Axios", url: "http://www.axios-js.com" },
                { name: "Lodash速查", url: "http://localhost:5000/#/docs/lodash函数速查", self: "true" },
                // { name: "Lodash中文", url: "https://www.lodashjs.com", target: "_blank" },
                { name: "Lodash中文", url: "http://lodash.think2011.net" },
                { name: "ES6", url: "https://www.css88.com/dev/es6.html" },
                { name: "JavaScript", url: "https://www.w3school.com.cn/jsref/index.asp" },
                { name: "正则手册", url: "https://www.w3school.com.cn/jsref/jsref_obj_regexp.asp" },
                { name: "正则测试", url: "https://c.runoob.com/front-end/854" },
                { name: "Echarts", url: "https://echarts.apache.org/zh/index.html" },
                { name: "Mock.js", url: "http://mockjs.com" }
            ]
        },
        {
            title: "CSS相关",
            data: [
                { name: "CSS选择器", url: "https://www.w3school.com.cn/cssref/css_selectors.asp" },
                { name: "CSS选择器演示", url: "https://www.haorooms.com/tools/css_selecter/" },
                { name: "Flex布局教程", url: "https://www.runoob.com/w3cnote/flex-grammar.html" },
                { name: "SASS速查", url: "https://www.css88.com/dev/sass.html" },
                { name: "FamilyScss", url: "https://www.familyscss.xyz" },
                { name: "CSS动画生成", url: "http://www.miued.com/tools/" }
            ]
        }
    ]
}