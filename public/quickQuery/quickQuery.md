[^ . ^]: 常用速查导航

<div class="quick-query-wrap" style="display:none;"></div>

<style>
    .quick-query-wrap {
        display: block !important;
        position: fixed;
        left: 300px;
        top: 0;
        right: 0;
        bottom: 0;
    }
    
    .qkq-nav-wrap {
        padding: 56px 0 30px 0;
        width: 160px;
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        border-right: 1px solid #eee;
        background-color: #fdfdfd;
        z-index: 99;
        text-align: right;
        overflow-y: auto;
        box-shadow: inset 2px 0 4px #f9f9f9;
    }
    
    .qkq-nav-wrap::-webkit-scrollbar {
        width: 4px;
        background: #e0e0e0;
    }
    
    .qkq-nav-wrap::-webkit-scrollbar-thumb {
        background: transparent;
        background: #ddd;
    }
    
    .qkq-nav-li,
    .qkq-nav-title {
        height: 28px;
        line-height: 28px;
    }
    
    .qkq-nav-title {
        margin-top: 20px;
        padding: 0 20px 6px;
        font-weight: bold;
    }
    
    .qkq-nav-wrap .qkq-nav-li {
        display: block;
        color: #555;
        padding: 0 20px 0 20px;
        overflow: hidden;
        margin: 0;
        cursor: pointer;
        font-size: 14px;
        text-decoration: none;
    }
    
    .qkq-nav-li:hover {
        text-decoration: underline;
    }
    
    .qkq-nav-li.active {
        color: #42b983;
        font-weight: bold;
    }
    
    .qkq-iframe-wrap {
        position: absolute;
        left: 160px;
        top: 0;
        right: 0;
        bottom: 0;
        background-color: #fff;
        z-index: 66;
    }
    
    .qkq-iframe-box iframe,
    .qkq-iframe-box {
        position: absolute;
        right: 0;
        left: 0;
        top: 0;
        bottom: 0;
    }
    
    .qkq-iframe-box iframe {
        display: block;
        width: 100%;
        height: 100%;
        border: 0;
        margin: 0;
    }
    
    .qkq-tags {
        position: absolute;
        left: calc(50% + 80px);
        top: 50%;
        transform: translate(-50%, -50%);
        z-index: 777;
        border: dotted 1px #e0e0e0;
        border-radius: 3px;
        color: #ccc;
        display: flex;
        flex-wrap: wrap;
        align-content: center;
        padding: 30px;
        height: 180px;
        width: 500px;
    }
    
    .qkq-tags a[data-weight="1"] {
        --size: 4.5;
    }
    
    .qkq-tags a[data-weight="2"] {
        --size: 5.1;
    }
    
    .qkq-tags a[data-weight="3"] {
        --size: 3.9;
    }
    
    .qkq-tags a[data-weight="4"] {
        --size: 4;
    }
    
    .qkq-tags a[data-weight="5"] {
        --size: 5.6;
    }
    
    .qkq-tags a[data-weight="6"] {
        --size: 5.8;
    }
    
    .qkq-tags a[data-weight="7"] {
        --size: 6.8;
    }
    
    .qkq-tags a[data-weight="8"] {
        --size: 6.5;
    }
    
    .qkq-tags a[data-weight="9"] {
        --size: 4.6;
    }
    
    .qkq-tags a {
        margin: 6px 6px;
        --size: 4;
        font-size: calc(var(--size) * 0.25rem);
        cursor: pointer;
        text-decoration: none;
    }
    
    .qkq-tags a:nth-child(2n+1) {
        color: #181;
    }
    
    .qkq-tags a:nth-child(3n+1) {
        color: #33a;
    }
    
    .qkq-tags a:nth-child(4n+1) {
        color: #c38;
    }
</style>

<!-- <script src="/public/quickQuery/_config.js"></script> -->
<script src="/public/quickQuery/quickQuery.js"></script>