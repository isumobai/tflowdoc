[^_._^]: webpack打包可视化分析工具

<div class="analyse-wrap">
    <div class="analyse-topbar">
        <div class="analyse-logo">webpackAnalyse</div>
        <div class="analyse-btn-refresh">刷新</div>
        <select class="analyse-select analyse-seletct-level">
            <option value="100">ALL</option>
            <option value="1">1层</option>
            <option value="2">2层</option>
            <option value="3">3层</option>
            <option value="4">4层</option>
            <option value="5">5层</option>
            <option value="6">6层</option>
        </select>
        <select class="analyse-select analyse-seletct-chunk">
            <option value="all" selected>块 > ALL</option>
        </select>
        
    </div>
    <div class="analyse-echarts-wrap"></div>
    <div class="analyse-filelist-wrap"></div>
</div>

<script src="public/analyse/analyse.js"></script>

<style>
.analyse-wrap {
  position: fixed;
  left: 300px;
  top: 0;
  right: 0;
  bottom: 0;
}

body.close .analyse-wrap {
  left: 0;
}

.analyse-topbar {
  position: relative;
  display: flex;
  height: 48px;
  border-bottom: 1px solid #eee;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
}
.analyse-logo {
  position: absolute;
  left: 0;
  font-weight: 700;
  padding-left: 25px;
  font-size: 18px;
  height: 48px;
  line-height: 48px;
  color: #666;
}
.analyse-select {
  box-sizing: content-box;
  z-index: 88;
  height: 30px;
  outline: none;
  color: #555;
  padding: 0 0 0 10px;
  border-radius: 2px;
  border: solid 1px #e0e0e0;
}

.analyse-seletct-chunk {
  width: 217px;
  margin-right: 25px;
}

.analyse-seletct-level {
  width: 50px;
  margin-right: 15px;
}

.analyse-select option {
  padding: 10px 0;
  border: transparent;
  outline: none;
  background-color: #fff;
  color: #333;
  font-size: 14px;
}

.analyse-btn-refresh {
  margin-right: 15px;
  width: 60px;
  height: 30px;
  line-height: 30px;
  border-radius: 2px;
  cursor: default;
  text-align: center;
  font-size: 14px;
  border: solid 1px #e0e0e0;
  color: #666;
}
.analyse-btn-refresh:hover {
  border: solid 1px #bbb;
  color: #333;
}

.analyse-echarts-wrap {
  position: absolute;
  left: 60px;
  right: 390px;
  top: 80px;
  bottom: 30px;
  overflow: auto;
}
.analyse-wrap text {
  margin-top: -30px;
}

.json-body,
.json-content {
  border: 1px solid #999;
  margin: 0 0 20px 20px;
}

.analyse-filelist-wrap {
  position: absolute;
  width: 300px;
  padding: 0 15px;
  right: 0;
  top: 49px;
  bottom: 0;
  overflow-y: auto;
  border-left: solid 1px #e0e0e0;
  background-color: #fff;
}
.analyse-filelist-title {
  border-bottom: solid 1px #e0e0e0;
  font-weight: bold;
  margin: 30px 0 12px;
  padding: 0 0 10px;
}
.analyse-filelist-title:first-child{
    margin-top: 20px;
}
.analyse-filelist-list {
  position: relative;
  background-color: #fff;
  margin-bottom: 12px;
  border-radius: 3px;
  word-wrap: break-word;
  word-break: break-all;
  border: solid 1px #e0e0e0;
  box-shadow: 6px 0 8px rgba(102, 119, 136, 0.03);
}
.analyse-filelist-list .name {
  display: block;
  padding: 10px 72px 10px 10px;
  line-height: 16px;
  color: #222;
}
.analyse-filelist-list span.size {
  position: absolute;
  right: 10px;
  top: 12px;
  font-size: 12px;
}

.analyse-filelist-list span.path {
  display: block;
  padding: 6px 10px;
  line-height: 16px;
  background-color: #f8f9fa;
  border-top: solid 1px rgba(102, 119, 136, 0.05);
  color: #667788;
}
</style>