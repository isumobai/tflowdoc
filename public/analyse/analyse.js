(function() {

    /*
     * ajax get方法Promise封装
     */
    const ajaxGetPromise = function(url) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.send(null);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.responseText);
                    } else {
                        reject()
                    }
                }
            }
        })
    }


    // echarts配置项
    let myChart;
    let option = {
        backgroundColor: '#fff',
        color: ['#4fc08d', '#5470c6', '#fac858', '#ee6666', '#73c0de', '#3ba272', '#fc8452', '#9a60b4', '#ea7ccc', '#e7bcf3', '#9d96f5', '#8378EA', '#96BFFF'],
        tooltip: {
            formatter: function(info) {
                let value = info.value;
                let name = info.name;
                let size = (value / 1024).toFixed(2) + "kb";
                let path = info.data.path || "";
                let childrenLen = info.data.children ? info.data.children.length : 0;

                return [
                    '<b>' + name + '</b><br>',
                    'Size: ' + size + '<br>',
                    'Path: ' + path + '<br>',
                    'Children: ' + childrenLen + '<br>',
                ].join('');
            }
        },

        series: [{
            name: 'HOME',
            type: 'treemap',
            top: 40,
            left: "center",
            bottom: 0,
            right: 0,
            width: "100%",
            drillDownIcon: "▷",
            visibleMin: 0,
            childrenVisibleMin: 0,
            data: [],
            label: {
                show: true,
                formatter: '{b}',
                fontSize: 14,
                width: '100%',
                height: 20,
                overflow: 'truncate',
                lineOverflow: 'truncate'
            },
            // 默认展示层级
            leafDepth: 3,
            // 面包屑
            breadcrumb: {
                height: 30,
                left: "center",
                top: 0,
                fontSize: 14,
                opacity: 0.6,
                itemStyle: {
                    color: "#35495e",
                    textStyle: {
                        fontSize: 14
                    }
                }
            },
            // 标题栏
            upperLabel: {
                show: true,
                height: 36,
                width: '100%',
                fontSize: 16,
                padding: [0, 0, 0, 10],
                color: "#fff",
                overflow: 'truncate',
                lineOverflow: "truncate",
            },
            // 区块
            levels: [{
                upperLabel: {
                    show: false
                },
                itemStyle: {
                    gapWidth: 4,
                    borderColorSaturation: 0.6,
                    borderWidth: 0,
                }
            }, {

                itemStyle: {
                    borderColorSaturation: 0.6,
                    borderWidth: 3,
                    gapWidth: 3
                }
            }, {
                upperLabel: {
                    height: 32
                },
                colorSaturation: [0.35, 0.5],
                itemStyle: {
                    gapWidth: 2,
                    borderColorSaturation: 0.6,
                    borderWidth: 2
                }
            }, {
                upperLabel: {
                    height: 24,
                    fontSize: 14
                },
                colorSaturation: [0.35, 0.5],
                itemStyle: {
                    gapWidth: 2,
                    borderColorSaturation: 0.6,
                    borderWidth: 2
                }
            }, {
                upperLabel: {
                    height: 26,
                    fontSize: 14
                },
                colorSaturation: [0.35, 0.5],
                itemStyle: {
                    gapWidth: 2,
                    borderColorSaturation: 0.6,
                    borderWidth: 2
                }
            }, {
                upperLabel: {
                    height: 24,
                    fontSize: 12
                },
                colorSaturation: [0.35, 0.5],
                itemStyle: {
                    gapWidth: 2,
                    borderColorSaturation: 0.6,
                    borderWidth: 2
                }
            }, {
                colorSaturation: [0.35, 0.5],
                itemStyle: {
                    gapWidth: 2,
                    borderColorSaturation: 0.8,
                    borderWidth: 2
                }
            }],
        }]
    };


    // 从webpack report.json 文件取得数据并解析成echarts规定数据
    function washData(resArr) {

        resArr.forEach((item, ind) => {

            let isUseful = item.path && item.path.search(/^(\.\/src)|^(\.\/node_modules)/);
            let isUnvue = item.path && item.path.search(/(\?vue)/);
            let isHot = item.label && item.label.search(/(hot-update)/);

            delete resArr[ind]['parsedSize'];
            delete resArr[ind]['gzipSize'];
            delete resArr[ind]['id'];
            delete resArr[ind]['isAsset'];

            if (item.label && isHot == -1) {
                resArr[ind]['name'] = resArr[ind]['label'];
                delete resArr[ind]['label'];
            }

            if (item.statSize) {
                resArr[ind]['value'] = resArr[ind]['statSize'];
                delete resArr[ind]['statSize'];
            }

            if (item.statSize) {
                resArr[ind]['value'] = resArr[ind]['statSize'];
                delete resArr[ind]['statSize'];
            }
            if (item.groups) {
                resArr[ind]['children'] = resArr[ind]['groups'];
                delete resArr[ind]['groups'];
            }

            if (item.children) {
                washData(resArr[ind].children)
            }

            if (isUseful == -1) {
                resArr.splice(ind, 1, {});
            }
            if (isUnvue > -1) {
                resArr.splice(ind, 1, {});
            }
            if (isHot > -1) {
                resArr.splice(ind, 1, {});
            }

        });

        return resArr;

    }


    // 获取数据后处理逻辑
    let reportPath = window.$docsify && window.$docsify.reportFile || "public/report.json";
    let getReportData = function() {
        return ajaxGetPromise(reportPath).then((res) => {

            let resArr = JSON.parse(res);
            let resArrNew = washData(resArr);
            let resArrList = {};

            resArrNew.forEach(el => {
                let name = el.name;
                if (name) resArrList[name] = el;
            });

            return {
                all: resArrNew,
                list: resArrList
            };
        });
    }

    // 下拉列表相关
    let selectChunkEl = document.querySelector(".analyse-seletct-chunk");

    // 创建下拉列表
    function createSelect(reportData) {
        for (item in reportData.list) {
            let option = document.createElement("option");
            option.text = "块 > " + item;
            option.value = item;
            selectChunkEl.add(option, null);
        }
    }



    // 更新echarts视图
    async function setEchartsView(reptData) {
        // 获取加工后的数据
        let reportData = reptData ? reptData : await getReportData();


        let selValue = selectChunkEl.value
        let selObj = reportData.list[selValue];
        let seltArr = [];
        seltArr.push(selObj);

        myChart.hideLoading();

        option.series[0].data = selValue == "all" ? reportData.all : seltArr;
        if (option && typeof option === 'object') {
            myChart.clear();
            myChart.setOption(option, true);
        }
    }


    // 显示层级更新
    let selectLevelEl = document.querySelector(".analyse-seletct-level");
    selectLevelEl.value = option.series[0].leafDepth;
    selectLevelEl.addEventListener("change", function() {
        if (option && typeof option === 'object') {
            option.series[0].leafDepth = selectLevelEl.value;
            myChart.clear();
            myChart.setOption(option, true);
        }
    });


    // 刷新数据
    let refreshEl = document.querySelector(".analyse-btn-refresh");
    refreshEl.addEventListener("click", function() {
        myChart.resize();
        setEchartsView();
    });


    // 获取每个chunk包锁引用的文件信息Obj
    function getFilesListObj(data) {
        let fileListObj = {};

        function fileList(data, keya) {

            if (data instanceof Array) {
                data.forEach(item => {
                    if (item.children) {
                        fileList(item.children, keya)
                    } else {
                        if (item.path) {
                            let value = (item.value / 1024).toFixed(2) + "kb";
                            fileListObj[keya].push({
                                path: item.path,
                                name: item.name,
                                size: value
                            });
                        }
                    }
                })
            } else if (typeof data == "object") {
                for (let key in data) {
                    fileListObj[key] = [];
                    let val = data[key];
                    if (val.children) {
                        fileList(val.children, key)
                    }
                }
            }
        }
        fileList(data)

        return fileListObj;
    }

    // 渲染每个chunk包引用的文件列表
    function analyfileListView(data, key) {
        let html = "";

        function arrEc(item) {
            item.forEach(ite => {
                html += '<div class="analyse-filelist-list" data-path="' + ite.path + '">';
                html += '<span class="name">' + ite.name + '</span>';
                html += '<span class="size">' + ite.size + '</span>';
                html += '<span class="path">' + ite.path + '</span>';
                html += '</div>';
            })
        }

        if (key) {
            html += '<div class="analyse-filelist-title">块 > ' + key + '</div>';
            arrEc(data[key])
        } else {
            for (let keya in data) {
                html += '<div class="analyse-filelist-title"> 块 > ' + keya + '</div>';
                let item = data[keya];
                arrEc(item);
            }
        }
        document.querySelector(".analyse-filelist-wrap").innerHTML = html;
    }

    // start
    (async function Start() {


        // 没有加载echarts.js 则先加载echarts.js
        if (typeof echarts == 'undefined') await ajaxGetPromise("/public/analyse/echarts.min.js").then(res => {
            eval(res);
        });

        // 获取加工后的数据
        let reportData = await getReportData();

        // 创建文件引用信息列表
        let fileListRt = getFilesListObj(reportData.list);
        analyfileListView(fileListRt)

        // 创建下拉选项
        createSelect(reportData);
        // 下拉列表改变后更新视图
        document.querySelector(".analyse-seletct-chunk").addEventListener("change", function() {
            let key = this.value == "all" ? "" : this.value;
            analyfileListView(fileListRt, key)
            setEchartsView();
        });

        // 创建echarts实例
        const dom = document.querySelector(".analyse-echarts-wrap");
        myChart = echarts.init(dom);
        myChart.showLoading();

        // 初始化显示echarts树图
        setEchartsView(reportData);

        // 窗口大小改变更新图表
        window.onresize = function() {
            myChart.resize();
        }

    })();


})();