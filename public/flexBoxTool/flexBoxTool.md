<style>
	.markdown-section{
		display:none;
	}
</style>
<link href="/public/flexBoxTool/flexBoxTool.css" type="text/css" rel="stylesheet">
<script src="/public/flexBoxTool/flexBoxTool.js"></script>


<!-- 下面是内容 -->


# FlexBoxTool

<div type="preview">
	<div class="prev-dragbox prev-flexbox-wrap flex ">
		<div class="prev-flexbox flex flex-none justify-center items-center">
			<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
			</svg>
		</div>
		<div class="prev-flexbox flex flex-grow justify-center items-center">
			<svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
				<path fill-rule="evenodd" d="M7.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l2.293 2.293a1 1 0 010 1.414z" clip-rule="evenodd" />
			</svg>
			<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 11V7a4 4 0 118 0m-4 8v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2z" />
			</svg>
			<svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
				<path fill-rule="evenodd" d="M12.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-2.293-2.293a1 1 0 010-1.414z" clip-rule="evenodd" />
			</svg>
		</div>
		<div class="prev-flexbox flex flex-none justify-center items-center">
			<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
			</svg>
		</div>
		<div class="prev-dragbox-thumb"></div>
	</div>
</div>

<div type="preview-tab">
	<tab>html</tab><tab>CSS</tab>
</div>

<div type="preview-code">

```html
<div class="flex ...">
  <div class="flex-none  ...">
    ...
  </div>
  <div class="flex-grow ...">
    ...
  </div>
  <div class="flex-none ...">
    ...
  </div>
</div>
```

</div>

<div type="preview-code">

```css
.flex {
    display: flex;
}
.flex-none {
    flex: none;
}
.flex-grow {
    flex-grow: 1;
}
```

</div>










