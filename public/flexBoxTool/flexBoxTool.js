$(function() {



    $(".prev-dragbox-thumb").mousedown(function(e) {
        const parent = $(".prev-dragbox");
        const width = parent.width();
        const left = $(".prev-dragbox").offset().left;
        $("body").mousemove(function(eve) {
            const x = eve.pageX - left;
            const wid = (x > width) ? width : x;
            const wids = (x < 360) ? 360 : wid;
            $(".prev-dragbox").animate({
                width: wids
            }, 0);

        })
    });
    $("body").mouseup(function(e) {
        $(this).unbind("mousemove");
    });



});