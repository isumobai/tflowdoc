export default function() {
    /**
     * 向Doc JsonFormat postMessage页面通信
     */
    const $options = {
        postMessageUrl: "http://localhost:5000/#/public/jsonFormat/jsonFormat",
        intervaTime: 500
    }

    // 创建iframe添加到页面
    let $iframe = document.createElement('iframe');
    $iframe.src = $options.postMessageUrl;
    $iframe.style = "display: none;"
    $iframe.id = "iframePostMsg"
    document.body.appendChild($iframe);

    let imeoutObj = "";
    let load = false; //子页面是否加载完成

    // console上添加jsonmonospace方法
    window.console.json = function(text) {
        if (load) {
            $iframe.contentWindow.postMessage(text, $options.postMessageUrl);
        } else {
            $iframe.onload = function() {
                load = true;
                clearTimeout(imeoutObj)
                $iframe.contentWindow.postMessage(text, $options.postMessageUrl);
            }
        }

        imeoutObj = setTimeout(() => {
            if (!load) {
                console.error(`PostMassage通信页面: [JSON Format] ${$options.postMessageUrl} 长时间未响应!`);
            }
        }, 5000);

    }

}