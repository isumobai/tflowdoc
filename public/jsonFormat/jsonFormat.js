$(function() {

    /**
     * 消息提示组件
     */
    function messageFn() {
        var timeOutClose;

        function closeLayer() {
            $(".m-message-layer").addClass("m-message-layer-hide");
            setTimeout(() => {
                $(".m-message-layer").remove();
                clearTimeout(timeOutClose);
            }, 100);
        }

        function setLayer(type, content) {
            $(".m-message-layer").remove();
            clearTimeout(timeOutClose);

            $("body").append("<div class='m-message-layer'></div>");
            $(".m-message-layer").attr("data-type", type).show().html(content);
            timeOutClose = setTimeout(() => {
                closeLayer();
            }, 3600);
        }

        this.success = function(content) {
            setLayer("success", content);
        }

        this.warning = function(content) {
            setLayer("warning", content);
        }
        this.error = function(content) {
            setLayer("error", content);
        }

        this.close = closeLayer;
    }

    var messageLayer = new messageFn();

    /**
     * loading组件
     */
    function loadingFN() {
        var hasEl = $(".m-loading-layer").length;
        if (!hasEl) $("body").append("<div class='m-loading-layer'></div>");

        var loadEl = $(".m-loading-layer");
        this.show = function() {
            loadEl.show();
        }
        this.hide = function() {
            loadEl.hide();
        }
    }

    var loadingLayer = new loadingFN();


    /**
     * ajax get方法Promise封装
     */
    const ajaxGetPromise = (url) => {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                success: function(response, status, xhr) {
                    resolve(xhr.responseText);
                },
                error: function(e, xhr) {
                    reject(e);
                }
            });
        })
    }



    function jsonRenderView(el, data) {

        var opts = {
            deep: 1
        }
        var $opts = Object.assign(opts, {});

        function json2html(json) {
            var html = "";

            function handArrayHtml(json) {
                var cont = 0;

                html += '[<div class="json-array-content json-content" style="display:none">';

                json.forEach((item, ind) => {
                    var index = '<span class="json-index">' + ind + ':</span>';

                    if (item instanceof Object && Object.keys(item).length > 0) {
                        html += '<div class="json-array-body json-body">';
                        html += '<span class="json-toggle collapsed">' + index + '</span>';
                    } else {
                        html += '<div class="json-array-li json-view-li">';
                        html += index;
                    }

                    cont++;
                    html += json2html(item);

                    if (ind < json.length - 1) {
                        html += ',';
                    }
                    html += '</div>';
                })
                html += '</div>' + '<span class="json-cont">' + cont + ' items</span>' + ']';

            }

            function handJsonHtml(json) {
                var length = Object.keys(json).length;
                var cont = 0;

                html += '{<div class="json-object-content json-content" style="display:none">';
                for (key in json) {
                    var item = json[key];
                    var key = '<span class="json-key json-serval">"' + key + '":</span>';

                    if (item instanceof Object && Object.keys(item).length > 0) {
                        html += '<div class="json-object-body json-body">';
                        html += '<span class="json-toggle collapsed">' + key + '</span>';

                    } else {
                        html += '<div class="json-object-li json-view-li">';
                        html += key;
                    }

                    cont = Object.keys(json).length;

                    html += json2html(item);

                    if (--length > 0) {
                        html += ',';
                    }
                    html += '</div>';
                }
                html += '</div>' + '<span class="json-cont">' + cont + ' items</span>' + '}';
            }

            if (typeof json === 'string') {
                json = json
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/'/g, '&apos;')
                    .replace(/"/g, '&quot;');
                html += '<span class="json-string json-serval">"' + json + '"</span>';

            } else if (typeof json === 'number') {
                html += '<span class="json-number json-serval">' + json + '</span>';

            } else if (typeof json === 'boolean') {
                html += '<span class="json-boolean json-serval">' + json + '</span>';

            } else if (typeof json === null) {
                html += '<span class="json-null json-serval">null</span>';

            } else if (json instanceof Array) {
                if (json.length > 0) {
                    handArrayHtml(json)
                } else {
                    html += '[]';
                }
            } else if (typeof json === 'object') {
                var length = json && Object.keys(json).length;

                if (length > 0) {
                    handJsonHtml(json)
                } else {
                    html += '{}';
                }
            }
            return html;
        }


        // 给Dom树添加层级
        function addDeepInd(el) {
            var deepInd = 0;

            function deep(el) {
                el.children(".json-content").attr("data-ind", deepInd);
                deepInd++;
                var ell = el.children(".json-content").children(".json-body") || el.children(".json-content");
                if (ell.length) deep(ell)
            }
            deep($(el));
        }


        // 展开指定层级
        function spreadDeep(ind) {
            var index = ind - 1;

            function spread(num) {
                var deepEl = $(el).find(".json-content[data-ind='" + num + "']");
                var jsonBody = deepEl.children('.json-body');
                jsonBody.children('.json-toggle').removeClass(".collapsed")
                    .siblings(".json-content").show()
                    .siblings(".json-cont").hide();
            }
            for (var i = 0; i <= index && i < 6; i++) {
                spread(i)
            }

            var deepElNext = $(el).find(".json-content[data-ind='" + ind + "']");
            deepElNext.find(".json-toggle").addClass("collapsed");
            deepElNext.find(".json-content").hide();
            deepElNext.find(".json-cont").show();
        }
        // 展开所有层级
        function spreadAll() {
            $(el).find(".json-toggle").removeClass("collapsed");
            $(el).find(".json-content").show();
            $(el).find(".json-cont").hide();
        }
        // 折叠所有层级
        function collapsedAll() {
            var deepEl = $(el).find(".json-content[data-ind='" + $opts.deep + "']");
            $(deepEl).find(".json-toggle").addClass("collapsed");
            $(deepEl).find(".json-content").hide();
            $(deepEl).find(".json-cont").show();
        }

        var $el = $(el);
        if ($el.length && data) {
            // 渲染到页面
            var jsonHtml = '<div class="json-content" data-deep="0">' +
                '<div class="json-body">' +
                '<span class="json-toggle" style="display:none"></span>' +
                json2html(data) +
                "</div></div>";
            $el.html(jsonHtml);
            addDeepInd($el);

            // 折叠当前层级
            $el.find(".json-toggle").click(function() {
                var $this = $(this);

                $this.toggleClass('collapsed')
                    .siblings(".json-content").toggle()
                    .siblings(".json-cont").toggle();

                $this.siblings(".json-content")
                    .find(".json-toggle").addClass("collapsed")
                    .siblings(".json-content").hide()
                    .siblings(".json-cont").show();
            });

            // 展开/折叠...内容
            $el.find(".json-view-li").click(function() {
                $(this).toggleClass("json-view-li-open");
            });

            // 点击cont展开当前层级
            $el.find(".json-cont").click(function() {
                $(this).siblings(".json-toggle").click();
            });

            // 默认展开根级别
            spreadDeep($opts.deep);
        }

        return {
            spreadAll,
            collapsedAll,
            spreadDeep
        }

    }
    var jsonRenderObj = jsonRenderView("#json-render");


    /**
     * 展开/折叠层级操作
     */

    // 展开指定层级
    $(".json-btn-deep").click(function() {
        var deep = $(this).attr("data-deep");
        jsonRenderObj.spreadDeep(deep);
    });
    // 展开所有
    $(".json-btn-spreadall").click(function() {
        jsonRenderObj.spreadAll();
    });
    // 折叠
    $(".json-btn-collapsed").click(function() {
        jsonRenderObj.collapsedAll();
    });


    /**
     * 搜索相关逻辑
     */
    var searchElSelectInd = 0;
    var searchElLength = 0;

    // 点击搜索
    $(".json-search-but").click(function() {
        var searchInputVal = $(".json-search-input").val().trim();
        var searchEl = $(".json-body span.json-serval:contains(" + searchInputVal + ")");
        var searchNotEl = $(".json-body span.json-serval:not(:contains(" + searchInputVal + "))").removeClass("json-search-val")
        searchElLength = searchEl.length;

        if (!searchInputVal || searchElLength == 0) {
            messageLayer.warning("没有匹配结果!");
            clrarSearchStyle();
            return false
        }

        $(".json-search-prev").show();
        $(".json-search-next").show();
        $(".json-search-num").show();
        searchEl.addClass("json-search-val");

        jsonRenderObj.spreadAll();

        toSearchVal(searchElSelectInd);
    });

    // 回车搜索
    $(".json-search-input").keydown(function(e) {
        var curKey = e.which;
        if (curKey == 13) {
            $(".json-search-but").click();
            $(".json-search-input").blur();
            return false;
        }
    });

    // 搜索结果上一个
    $(".json-search-prev").click(function() {
        searchElSelectInd--;
        toSearchVal(searchElSelectInd);
    });
    // 搜索结果下一个
    $(".json-search-next").click(function() {
        searchElSelectInd++;
        toSearchVal(searchElSelectInd);

    });

    // 滚动到指定节点
    function toSearchVal(ind) {

        // 当前选定节点计数
        if (!(ind < searchElLength)) {
            searchElSelectInd = 0;
        } else if (ind < 0) {
            searchElSelectInd = searchElLength + ind;
        }

        // 高亮当前跳转节点
        $(".json-search-val").removeClass("json-search-valsel");
        $(".json-search-val:eq(" + searchElSelectInd + ")").addClass("json-search-valsel");

        var jsonView = $(".json-view");
        var toELTop = $(".json-search-val:eq(" + searchElSelectInd + ")").offset().top;
        var boxOffsetTop = jsonView.offset().top;
        var boxScrollTop = jsonView.scrollTop();

        var topNum = toELTop - boxOffsetTop + boxScrollTop;


        showSelInd();

        jsonView.stop();
        jsonView.animate({
            scrollTop: topNum - 30
        }, 200);
    };

    // 清除所有搜索样式
    function clrarSearchStyle() {
        $(".json-search-val").removeClass("json-search-val");
        $(".json-search-valsel").removeClass("json-search-valsel");
        $(".json-view").animate({
            scrollTop: 0
        }, 360);
        $(".json-search-prev").hide();
        $(".json-search-next").hide();
        $(".json-search-num").hide();
    }

    // 跳转节点动态计数
    function showSelInd() {
        $(".json-search-num").text((searchElSelectInd + 1) + "/" + searchElLength)
    }

    // 搜索框由内容则显示清除按钮
    $(".json-search-input").keyup(function() {
        var val = $(this).val().trim();
        var clearEl = $(".json-search-clear");
        (val) ? clearEl.show(): clearEl.hide();
    });

    // 清除搜索
    $(".json-search-clear").click(function() {
        $(".json-search-input").val("");
        $(this).hide();
        clrarSearchStyle();
    });


    /**
     * 解析数据到视图
     * 并检测JSON格式
     */

    async function jsonRenderLint(data, callback) {

        // 引入jsonlint文件
        if (typeof jsonlint == 'undefined') await $.getScript("/public/jsonFormat/jsonlint.js");

        var lintJsonRes = "";
        try {
            lintJsonRes = jsonlint.parse(data);
            data = eval('(' + data + ')');
        } catch (err) {
            var errHtml = '<div class="json-error-info">' + err + '</div>';
            $('#json-render').html('<div class="json-error-wrap">' + errHtml + '</div>');
            return false;
        }

        // JSON可视化
        jsonRenderView("#json-render", data);

        if (callback) callback();
    }


    /**
     * 录入数据弹窗
     */

    // 点击右上角按钮
    $(".json-input-torbarbtn").click(function() {
        $(".json-input-layer").toggle();
        $(".json-input-cboxtear").val("");
        $(".json-input-inpbtn").val("");
    });

    // 录入数据弹窗关闭事件
    $(".json-input-closebtn").click(function() {
        $(this).parents(".json-input-layer").hide();
    });

    // 录入数据弹 > 手动输入数据
    var textareaInput = $(".json-input-cboxtear");
    var textareaBtn = $(".json-input-inpbtn");
    textareaBtn.click(function() {
        var val = textareaInput.val();
        if (!val) return false;
        handDataType(val);
        restUrlStyle();
    });

    // 根据数据类型做不同渲染操作
    function handDataType(val) {
        var urlRegexp = /^[(http)|(https)]+:\/\/[^\s]*/i;
        var isurl = urlRegexp.test(val);
        isurl ? urlIptResolv(val) : jsonRenderLint(val);
    }

    // 录入数据弹窗 > URL输入数据
    var dataInputInp = $(".json-input-dataipt");
    var urlInputBtn = $(".json-input-urlbtn");

    urlInputBtn.click(function() {
        var val = dataInputInp.val();
        handDataType(val)
    });

    // 重置样式
    function restUrlStyle() {
        $(".json-input-layer").hide();
        dataInputInp.val("");
    }

    // 通过url加载解析数据
    function urlIptResolv(url) {
        loadingLayer.show();
        ajaxGetPromise(url).then((res) => {
            loadingLayer.hide();

            jsonRenderLint(res, function() {
                restUrlStyle();
            });
        }).catch(() => {
            loadingLayer.hide();
            messageLayer.error("数据请求失败!")
        })
    }


    /**
     * 实现Ctrl+Shift+V 粘贴数据
     */
    const ctrlShiftV = $("#ctrlShiftV");
    if (ctrlShiftV.length == 0) {
        $('body').append("<input id='ctrlShiftV' value='' style='position: fixed;left:-5000px;top:0' />");
    };

    // 要用原始JS书写
    // input粘贴事件
    document.querySelector("#ctrlShiftV").addEventListener('paste', e => {
        var clipboardData = e.clipboardData || window.clipboardData
        var pastedData = clipboardData.getData('Text').trim();
        handDataType(pastedData);
    });

    // Ctrl+Shift+V粘贴功能
    document.onkeydown = (e) => {
        if (e.shiftKey && e.ctrlKey) {
            $("#ctrlShiftV").val("").focus();
        }
    }


    /**
     * PostMessage
     */

    window.addEventListener('message',
        function(e) {
            var origin = $docsify && $docsify.jsonFormat && $docsify.jsonFormat.postMessageAllowUrl;
            origin = origin ? origin.replace(/\/$/g, "") : true;

            if (e.origin == origin || origin === true) {
                localStorage.setItem("postMessageJsonFormatData", JSON.stringify(e.data));
            } else {
                console.error(`postMessage只允许 ${origin} 通信!`)
            }
        },
        false);

    var postMessageJsonDataFormatVal = "";
    var localStoragePostMsgJt = null;

    // 刷新PostMssages数据
    $(".json-topbar-postmsgresbtn").click(function() {
        restPostMsg()
    });

    // 刷新PostMssages传过来的数据
    // @force {boolean} true强制刷新 false不强制刷新
    function restPostMsg(force) {
        var val = localStorage.getItem("postMessageJsonFormatData");
        if (val !== postMessageJsonDataFormatVal && val != undefined) {
            postMessageJsonDataFormatVal = val;
            jsonRenderLint(val);
        } else if (val && force) {
            jsonRenderLint(val);
        }
    }

    // 双击Shift触发 刷新PostMssages数据
    let shiftDowntiem = 0;
    document.onkeyup = (e) => {
        if (e.key == 'Shift') {
            let nt = new Date().getTime();
            let ct = nt - shiftDowntiem;
            if (ct > 0 && ct < 600) {
                restPostMsg(true);
            }
            shiftDowntiem = nt;
        }
    };

    // 监听PostMasage消息按钮
    localStorage.clear("postMessageJsonFormatData");

    $(".json-jtmsg-inp").change(function() {
        if ($(this).is(":checked")) {
            localStoragePostMsgJt = setInterval(function() {
                restPostMsg();
            }, 1000);
        } else {
            window.clearInterval(localStoragePostMsgJt);
            localStorage.clear("postMessageJsonFormatData");
        }
    });




    /******************************
     **      HTTPLE相关代码       **
     ******************************/

    // 展开
    $(".json-btn-httple").click(function() {
        $(".json-format-wrap").toggleClass("json-format-httple")
        $(".httple-wrap").show();
        $(".json-input-layer").hide();
    });

    // 关闭
    $(".httple-close").click(function() {
        $(".json-format-wrap").removeClass("json-format-httple")
        $(".httple-wrap").hide();
        restHttple();
    });

    // 重置
    function restHttple() {
        var $el = $(".httple-wrap");

        $el.find(".httple-select-requesttype").val("get");
        $el.find(".httple-area").val("");
        $el.find(".httple-input").val("");
        $el.find(".httple-txtval").html("");
        $el.find(".httple-select-contype").val("application/x-www-form-urlencoded");
        $el.find(".httple-select-codetype").val("utf-8");
        $el.find(".httple-chkbox-ipt[name='parm']").prop("checked", true);
        $el.find(".httple-chkbox-ipt[name='sethead']").prop("checked", true);
        $el.find(".httple-chkbox-ipt[name='setcookie']").prop("checked", false);
        $el.find(".httple-chkbox-ipt").trigger("change");
        $el.find(".httple-parm-addli").remove();
        $el.find(".httple-btn-parmaddli").click().click();
    }


    /**
     * TAB选项卡
     */
    $(".httple-tab-opt").click(function() {
        var _this = $(this);
        var parentEl = _this.parents(".httple-tabbox");
        var ind = _this.attr("data-ind");
        var hasAdd = _this.attr("data-type");
        var contentElActive = parentEl.find(".httple-tab-content[data-ind='" + ind + "']");
        contentElActive.addClass("active").siblings(".httple-tab-content").removeClass('active')
        _this.addClass("active").siblings(".httple-tab-opt").removeClass("active");

        hasAdd == "add" ? parentEl.find(".httple-btn-parmaddli").show() : parentEl.find(".httple-btn-parmaddli").hide();
    });


    /**
     * 参数添加/删除
     */

    // 添加
    $(".httple-btn-parmaddli").click(function() {
        var parentContent = $(this).parents(".httple-tab-content")
        var copyLiEl = parentContent.find(".httple-parm-addli-copy");
        var copyLiCloneEl = copyLiEl.clone().removeClass("httple-parm-addli-copy").addClass("httple-parm-addli")
        $(this).before(copyLiCloneEl);
        copyLiCloneEl.find(".httple-parm-key").focus()
    });

    // 删除
    $(".httple-parm-addbox").on("click", ".httple-btn-delparm", function() {
        $(this).parent(".httple-parm-addli").remove();
    });


    /**
     * 设置选项卡开启/关闭
     */
    $(".httple-chkbox-ipt").change(function() {
        var _this = $(this);
        var name = _this.attr("name");
        var ischecked = _this.is(":checked");
        var setparmEl = $(".httple-box-setparm");
        var setheadEl = $(".httple-box-sethead");
        var setcookieEl = $(".httple-box-setcookie");
        if (name == "parm") {
            ischecked ? setparmEl.show() : setparmEl.hide();
        } else if (name == "sethead") {
            ischecked ? setheadEl.show() : setheadEl.hide();
        } else if (name == "setcookie") {
            ischecked ? setcookieEl.show() : setcookieEl.hide();
        }
    });


    /**
     * 动态更改参数设置方式
     */
    $(".httple-select-contype").change(function() {
        var val = $(this).val();
        var codedEl = $(".httple-tab-opt[data-type='coded']");
        var jsonEl = $(".httple-tab-opt[data-type='json']");
        var activeInLi = $(".httple-tab-opt-parmli").hasClass("active");

        if (val == "application/x-www-form-urlencoded") {
            codedEl.show()
            jsonEl.hide();
            if (!activeInLi) codedEl.click();

            data2data('list2coded');

        } else if (val == "application/json") {
            codedEl.hide();
            jsonEl.show();
            if (!activeInLi) jsonEl.click();

            data2data('list2json');
        }
    });


    /**
     * 参数设置同步效果
     */

    //  更新参数设置列表视图
    function restParmsLiDom(dataObj) {
        var el = $(".httple-box-setparm");
        var copyLiEl = el.find(".httple-parm-addli-copy");
        var addLiEl = el.find(".httple-btn-parmaddli");

        el.find(".httple-parm-addli").remove();

        for (item in dataObj) {
            var copyLiCloneEl = copyLiEl.clone().removeClass("httple-parm-addli-copy").addClass("httple-parm-addli");
            copyLiCloneEl.find(".httple-parm-key").val(item);
            copyLiCloneEl.find(".httple-parm-val").val(dataObj[item]);
            addLiEl.before(copyLiCloneEl)
        }
    }

    // 获取设置列表参数JSON对象
    function getParmListJsonObj() {
        var addLiObj = {};
        var addLiEl = $(".httple-box-setparm .httple-parm-addli");
        addLiEl.each(function(el) {
            var key = $(this).find(".httple-parm-key").val().trim();
            var val = $(this).find(".httple-parm-val").val().trim();

            if (val == "undefined" || val == "null" || val == "true" || val == "false") {
                val = JSON.parse(val)
            } else if (/^[0-9]+.?[0-9]*/.test(val)) {
                val = Number(val)
            }

            if (key) addLiObj[key] = val;
        });

        return Object.keys(addLiObj).length > 0 ? addLiObj : undefined;
    }

    // coded值解析成JSON
    // coded格式错误返回undefined
    function codedVal2JsonObj(val) {
        var jsonObj = {};
        var arr = val.split("&");
        arr.forEach(item => {
            var ob = item.split("=");
            var key = ob[0];
            var val = ob[1];

            if (key === undefined) {
                jsonObj = undefined;
                return false;
            } else if (key && val != 'undefined') {
                jsonObj[key] = val;
            }
        });
        return jsonObj;
    }


    // JSON解析成coded值
    function jsonObj2CodedVal(jsonObj) {
        var codedArr = [];
        for (item in jsonObj) {
            var val = jsonObj[item];
            var codedVal = val ? item + '=' + val : item + '=';
            codedArr.push(codedVal)
        }
        var coded = codedArr.join("&").replace(/=$/g, "")
        return coded;
    }


    // 判断字符串是否是JSON格式字符串
    function isJSON(str) {
        try {
            var obj = eval('(' + str + ')');
            if (typeof obj == 'object' && obj) {
                return obj;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    }

    // 数据同步更新
    function data2data(type) {

        if (type == "coded2list") {

            var codedVal = $(".httple-area-parmcoded").val();
            var codedValObj = codedVal ? codedVal2JsonObj(codedVal) : undefined;

            if (codedValObj) {
                restParmsLiDom(codedValObj);
                messageLayer.close();
            } else {
                messageLayer.warning("application/x-www-form-urlencoded值设置错误!");
            }

        } else if (type == "list2coded") {

            var listObj = getParmListJsonObj();
            var coded = jsonObj2CodedVal(listObj);
            $(".httple-area-parmcoded").val(coded);

        } else if (type == "json2list") {

            var jsonVal = $(".httple-area-parmjson").val();
            var isJson = isJSON(jsonVal);
            if (isJson) {
                restParmsLiDom(isJson);
                messageLayer.close();
            } else {
                messageLayer.warning("application/json值设置错误!");
            }

        } else if (type == "list2json") {
            var listObj = getParmListJsonObj();
            var jsonStr = JSON.stringify(listObj);
            $(".httple-area-parmjson").val(jsonStr);
        }

    }

    $(".httple-area-parmcoded").blur(function() {
        var val = $(this).val().trim();
        $(this).val(val)
        if (val) data2data("coded2list");
    });

    $(".httple-area-parmjson").blur(function() {
        var val = $(this).val().trim();
        $(this).val(val)
        if (val) data2data("json2list");
    });


    $(".httple-box-setparm").on("blur", ".httple-input", function() {
        var val = $(this).val().trim();
        $(this).val(val)
        contypeD2D();
    });

    $(".httple-parm-addbox").on("click", ".httple-btn-delparm", function() {
        contypeD2D();
    });

    function contypeD2D() {
        var contype = $(".httple-select-contype").val();
        if (contype == "application/x-www-form-urlencoded") {
            data2data("list2coded");
        } else if (contype == "application/json") {
            data2data("list2json");
        }
    }

    // 获取header参数设置
    function getHeaderParm() {
        var headParm = {};
        $(".httple-box-sethead .httple-parm-addli").each(function(el) {
            var el = $(el);
            var key = el.find(".httple-parm-key").val();
            var val = el.find(".httple-parm-val").val();
            if (key && val) headParm[key] = val;
        });
        return (Object.keys(headParm).length) ? headParm : undefined;
    }

    // 获取Cookie设置值
    function getCookie() {
        return $(".httple-area-cookie").val();
    }


    function requestFn(callback) {

        var url = $(".httple-area-url").val();
        var contentType = $(".httple-select-contype").val();
        var codeType = $(".httple-select-codetype").val();
        var requestType = $(".httple-select-requesttype").val();
        var headParms = getHeaderParm();
        var listObj = getParmListJsonObj();
        var cookie = getCookie();
        var parms = function() {
            if (contentType == "application/x-www-form-urlencoded") {
                return jsonObj2CodedVal(listObj);
            } else if (contentType == "application/json") {
                return JSON.stringify(listObj);
            }
        }();


        // 未完成
        function createAjaxFn() {
            var fnStr;
            var parmsStr = listObj ? JSON.stringify(listObj, null, "  ") : "";

            fnStr += 'const url = ' + url + '\n\n';
            fnStr = parmsStr ? fnStr + 'const parms = ' + parmsStr + '\n\n' : fnStr + "";

            fnStr += 'axios.' + requestType + "(\n";
            fnStr += '"url",';
            fnStr += 'qs.stringify(parms)\n';
            fnStr += ').then(res => {\n\n';
            fnStr += '});'
            return fnStr;
        }



        console.log(createAjaxFn());

        function createHeadLi(XHR) {
            var list = XHR.getAllResponseHeaders();
            list = list.replace(/(^\S*):(.*)/mg, "<div class='httple-head-returnli clearfix'><span>$1 :</span>$2</div>");
            $(".httple-txtval-headreturn").html(list);
        }


        $.ajax({
            url: url,
            type: requestType,
            data: parms,
            headers: {
                'Content-Type': contentType + ";charset=" + codeType,
                // 'Cookie': cookie,
                ...headParms
            },
            success: function(data, textStatus, request) {
                jsonRenderLint(JSON.stringify(data));
            },
            error: function(err) {
                messageLayer.error("URL请求失败!");
                console.error(`${url} 请求失败: `, err);
            },
            complete: function(XHR) {
                createHeadLi(XHR);
                if (callback) callback();
            }
        });
    }


    $(".httple-btn-request").click(function() {
        var url = $(".httple-area-url").val().trim();
        if (!url) {
            messageLayer.warning("请输入URl!");
            return false;
        };

        var isUrl = /[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\./g.test(url);
        if (!isUrl) {
            messageLayer.warning("URL格式错误!");
            return false;
        }


        var _this = $(this);
        var isLock = _this.attr("data-lock");
        if (isLock == true) return false;
        _this.attr("data-lock", true);

        var timeout = setTimeout(() => {
            _this.addClass("httple-btn-loading");
        }, 200);

        requestFn(function() {
            clearTimeout(timeout);
            _this.removeClass("httple-btn-loading").attr("data-lock", false);
            $(".httple-tabbox-return").show();
        })
    });



});