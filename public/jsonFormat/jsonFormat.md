<div class="json-format-wrap" style="display:none">
    <div class="json-topbar clearfix">
        <div class="json-topbar-logo">JSON Format</div>
        <div class="json-topbar-serach clearfix">
            <input class="json-search-input" type="text" value="">
            <div class="json-search-clear">×</div>
            <div class="json-search-but">搜索</div>
            <div class="json-search-prev">&lt;</div>
            <div class="json-search-next">&gt;</div>
            <div class="json-search-num"></div>
        </div>
        <div class="json-input-torbarbtn">录入数据</div>
        <div class="json-topbar-box json-topbar-httple">
            <div class="json-btn-httple">HTTP.LE</div>
        </div>
        <div class="json-topbar-jtmsg json-topbar-box  clearfix">
            <div class="json-topbar-title">PostMessage:</div>
            <label class="json-jtmsg-label json-toolge-switch">
              <input class="json-jtmsg-inp json-topbar-postmsg" type="checkbox">
              <span class="json-jtmsg-text json-toolge-text" data-yes="监听中" data-no="未监听"></span> 
            </label>
            <div class="json-topbar-item json-topbar-postmsgresbtn">
                刷新
            </div>
        </div>
        <div class="json-topbar-box  json-topbar-level clearfix">
            <div class="json-topbar-title">显示:</div>
            <div class="json-topbar-item json-btn-spreadall">展开所有</div>
            <div class="json-topbar-item json-btn-collapsed">折叠</div>
            <div class="json-topbar-item json-btn-deep" data-deep="1">1级</div>
            <div class="json-topbar-item json-btn-deep" data-deep="2">2级</div>
            <div class="json-topbar-item json-btn-deep" data-deep="3">3级</div>
            <div class="json-topbar-item json-btn-deep" data-deep="4">4级</div>
        </div>
    </div>
    <div class="json-view u-scrollbar">
        <div id="json-render">
            <div class="json-render-tip">请点击右上角按钮录入数据,<br>或者[Ctrl+Shift+V]粘贴数据</div>
        </div>
    </div>
    <div class="json-input-layer">
        <div class="json-input-cbox clearfix">
            <div class="json-input-cboxtear-wrap">
                <textarea class="json-input-cboxtear json-input-dataipt" placeholder="手动输入JSON数据 或者 Ctrl+Shift+V粘贴JSON数据"></textarea>
            </div>
            <div class="json-input-closebtn">关闭</div>
            <div class="json-input-inpbtn">确定</div>
        </div>
    </div>
</div>

<div class="httple-wrap" style="display: none;">
    <div class="httple-topbar clearfix">
        <div class="httple-logo">HTTP.LE</div>
        <div class="httple-close">关闭</div>
    </div>
    <div class="httple-content u-scrollbar">
        <div class="httple-box clearfix">
            <div class="httple-textbox httple-downline httple-textbox-url">网址</div>
            <div class="httple-btn-deft httple-btn-request ">模拟请求</div>
            <select class="httple-select httple-select-requesttype">
                <option selected value="get">GET</option>
                <option value="post">POST</option>
            </select>
            <div class="httple-area-box httple-area-urlbox">
                <textarea class="httple-area httple-area-url" placeholder="请输入模拟请求网址"></textarea>
            </div>
            <div class="httple-contype clearfix">
                <div class="httple-textbox httple-textbox-contype">Content-Type</div>
                <select class="httple-select httple-select-contype">
                <option selected value="application/x-www-form-urlencoded">application/x-www-form-urlencoded</option>
                <option value="application/json">application/json</option>
            </select>
            </div>
            <select class="httple-select httple-select-codetype">
                <option value="utf-8">UTF-8</option>
                <option value="gbk">GBK</option>
                <option value="gb2312">GB2312</option>
            </select>
        </div>
        <div class="httple-box-setting clearfix">
            <label class="httple-chkbox httple-chkbox-parm">
                参数设置
                <input class="httple-chkbox-ipt" checked type="checkbox" name="parm" />
                <span class="httple-chkbox-sta"></span>
            </label>
            <label class="httple-chkbox httple-chkbox-headset">
                Header设置
                <input class="httple-chkbox-ipt" checked type="checkbox" name="sethead" />
                <span class="httple-chkbox-sta"></span>
            </label>
            <label class="httple-chkbox httple-chkbox-cookie">
                Cookie设置
                <input class="httple-chkbox-ipt" type="checkbox" name="setcookie" />
                <span class="httple-chkbox-sta"></span>
            </label>
        </div>
        <div class="httple-box clearfix httple-tabbox httple-tabbox-return">
            <div class="httple-textbox httple-text-restxt">ResponseText返回</div>
            <div class="httple-tab-topbar clearfix">
                <div class="httple-tab-opt httple-tab-opt-ajaxfn" data-ind="1">生成代码</div>
                <div class="httple-tab-opt httple-tab-opt-headreturn active" data-ind="0">Header返回值</div>
            </div>
            <div class="httple-tab-content active" data-ind="0">
                <div class="httple-txtval-box">
                    <div class="httple-txtval httple-txtval-headreturn"></div>
                </div>
            </div>
            <div class="httple-tab-content" data-ind="1">
                <div class="httple-txtval-box">
                    <div class="httple-txtval httple-txtval-ajaxfn"></div>
                </div>
            </div>
        </div>
        <div class="httple-box httple-box-setparm clearfix httple-tabbox">
            <div class="httple-tab-topbar clearfix">
                <div class="httple-tab-tit">参数设置:</div>
                <div class="httple-tab-opt httple-tab-opt-parmcoder" data-ind="1" data-type="coded">批量添加</div>
                <div class="httple-tab-opt httple-tab-opt-parmjson" data-ind="2" data-type="json">批量添加</div>
                <div class="httple-tab-opt httple-tab-opt-parmli active" data-ind="0" data-type="add">参数设置</div>
            </div>
            <div class="httple-tab-content httple-parm-addbox active" data-ind="0">
                <div class="httple-parm-addli-copy clearfix">
                    <input type="text" placeholder="参数key" class="httple-input httple-parm-key">
                    <input type="text" placeholder="参数value,可为空" class="httple-input httple-parm-val">
                    <div class="httple-btn-line httple-btn-delparm">删除</div>
                </div>
                <div class="httple-parm-addli clearfix">
                    <input type="text" placeholder="参数key" value="" class="httple-input httple-parm-key">
                    <input type="text" placeholder="参数value,可为空" value="" class="httple-input httple-parm-val">
                    <div class="httple-btn-line httple-btn-delparm">删除</div>
                </div>
                <div class="httple-parm-addli clearfix">
                    <input type="text" placeholder="参数key" class="httple-input httple-parm-key">
                    <input type="text" placeholder="参数value,可为空" class="httple-input httple-parm-val">
                    <div class="httple-btn-line httple-btn-delparm">删除</div>
                </div>
                <div class="httple-btn-parmaddli">
                    <span class="httple-parm-adda">[ 添加一行 ]</span>
                    <span class="httple-parm-addnull">[ 请点击添加参数 ]</span>
                </div>
            </div>
            <div class="httple-tab-content" data-ind="1">
                <div class="httple-area-box">
                    <textarea class="httple-area httple-area-parmcoded" placeholder="Content-Type:application/x-www-form-urlencoded,如:name=name&value=value"></textarea>
                </div>
            </div>
            <div class="httple-tab-content" data-ind="2">
                <div class="httple-area-box">
                    <textarea class="httple-area httple-area-parmjson" placeholder='Content-Type:application/json,如:{"name":"name","value":"value"}'></textarea>
                </div>
            </div>
        </div>
        <div class="httple-box httple-box-sethead httple-box-untop  clearfix">
            <div class="httple-tab-topbar clearfix">
                <div class="httple-tab-tit">Header设置:</div>
            </div>
            <div class="httple-tab-content httple-parm-addbox active">
                <div class="httple-parm-addli-copy clearfix">
                    <input type="text" placeholder="参数key" class="httple-input httple-parm-key">
                    <input type="text" placeholder="参数value,可为空" class="httple-input httple-parm-val">
                    <div class="httple-btn-line httple-btn-delparm">删除</div>
                </div>
                <div class="httple-parm-addli clearfix">
                    <input type="text" placeholder="参数key" class="httple-input httple-parm-key">
                    <input type="text" value="" placeholder="参数value,可为空" class="httple-input httple-parm-val">
                    <div class="httple-btn-line httple-btn-delparm">删除</div>
                </div>
                <div class="httple-parm-addli clearfix">
                    <input type="text" placeholder="参数key" class="httple-input httple-parm-key">
                    <input type="text" placeholder="参数value,可为空" class="httple-input httple-parm-val">
                    <div class="httple-btn-line httple-btn-delparm">删除</div>
                </div>
                <div class="httple-btn-parmaddli">
                    <span class="httple-parm-adda">[ 添加一行 ]</span>
                    <span class="httple-parm-addnull">[ 请点击添加参数 ]</span>
                </div>
            </div>
        </div>
        <div class="httple-box httple-box-untop httple-box-setcookie clearfix httple-tabbox">
            <div class="httple-tab-topbar clearfix">
                <div class="httple-tab-tit">Cookie设置:</div>
            </div>
            <div class="httple-tab-content active">
                <div class="httple-area-box">
                    <textarea class="httple-area httple-area-cookie" placeholder="设置Cookie信息"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- 引入 css -->
<link href="/public/jsonFormat/jsonFormat.css" type="text/css" rel="stylesheet">
<script src="/public/jsonFormat/jsonFormat.js"></script>