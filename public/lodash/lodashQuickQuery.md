[^.^ Fixed-nav ]: 开启页面二级导航浮动



# lodash工具函数速查


## 1、简介

[Lodash](https://github.com/lodash/lodash)是一款优秀的 JavaScript 工具库，里面包含了大量的工具函数。在 2015 年就成为[被依赖最多的 JavaScript 库](https://www.npmjs.com/browse/depended)，此文档整理于版本 4.17(感谢[飞鹰走码](https://lichangwei.github.io/2016/09/06/lodash-functions/)整理),本地文档内容来自于[Lodash 中文文档](https://www.lodashjs.com),版本:4.17,采集时间:2021.3.16。


## 2、模块

Lodash 的工具函数很多，可以分为以下几类：数组（Array），集合（Collection），函数（Function），Lang（Lang），数学（Math），数字（Number），对象（Object），字符串（String），未分类工具函数（Util）。下面将会按类别介绍常见工具函数。

## 3、数组

### 获取子数组

| 函数名                                                    | 简介                                             |
| --------------------------------------------------------- | ------------------------------------------------ |
| [slice](https://www.lodashjs.com/docs/lodash.slice)                   | 获取元素第 m-n\(不包含\)个元素                   |
| [tail](https://www.lodashjs.com/docs/lodash.tail)                     | 获取出第一个元素之外的其他元素                   |
| [initial](https://www.lodashjs.com/docs/lodash.initial)               | 获取出最后一个元素之外的其他元素                 |
| [take](https://www.lodashjs.com/docs/lodash.take)                     | 从左侧开始获取任意数量的元素                     |
| [takeRight](https://www.lodashjs.com/docs/lodash.takeRight)           | 从右侧开始获取任意数量的元素                     |
| [takeWhile](https://www.lodashjs.com/docs/lodash.takeWhile)           | 从左侧开始获取任意数量的元素，直到断言返回假值   |
| [takeRightWhile](https://www.lodashjs.com/docs/lodash.takeRightWhile) | 从右侧开始获取任意数量的元素，直到断言返回假值   |
| [drop](https://www.lodashjs.com/docs/lodash.drop)                     | 丢掉前面几个元素，得到剩余元素                   |
| [dropWhile](https://www.lodashjs.com/docs/lodash.dropWhile)           | 丢掉前面几个元素知道迭代器返回假值，得到剩余元素 |
| [dropRight](https://www.lodashjs.com/docs/lodash.dropRight)           | 丢掉后面几个元素，得到剩余元素                   |
| [dropRightWhile](https://www.lodashjs.com/docs/lodash.dropRightWhile) | 丢掉后面几个元素知道迭代器返回假值，得到剩余元素 |

### 数组常见操作

| 操作 | 不修改原数组                                              | 修改原数组                                          |
| ---- | --------------------------------------------------------- | --------------------------------------------------- |
| 移除 | [without](https://www.lodashjs.com/docs/lodash.without)               | [pull](https://www.lodashjs.com/docs/lodash.pull)               |
| 相减 | [difference](https://www.lodashjs.com/docs/lodash.difference)         | [pullAll](https://www.lodashjs.com/docs/lodash.pullAll)         |
| 相减 | [differenceBy](https://www.lodashjs.com/docs/lodash.differenceBy)     | [pullAllBy](https://www.lodashjs.com/docs/lodash.pullAllBy)     |
| 相减 | [differenceWith](https://www.lodashjs.com/docs/lodash.differenceWith) | [pullAllWith](https://www.lodashjs.com/docs/lodash.pullAllWith) |
| 反转 |                                                           | [reverse](https://www.lodashjs.com/docs/lodash.reverse)         |
| 裁剪 | [at](https://www.lodashjs.com/docs/lodash.at)                         | [pullAt](https://www.lodashjs.com/docs/lodash.pullAt)           |
| 过滤 | [filter](https://www.lodashjs.com/docs/lodash.filter)                 | [remove](https://www.lodashjs.com/docs/lodash.remove)           |

### 数组常见操作变种函数 by, with

有些函数还可以稍微变化一下，接受不同的参数，提供更多灵活性。

| 作用 | 函数名                                                | by                                                        | with                                                                                                                                                                    |
| ---- | ----------------------------------------------------- | --------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 相减 | [difference](https://www.lodashjs.com/docs/lodash.difference)     | [differenceBy](https://www.lodashjs.com/docs/lodash.differenceBy)     | [differenceWith](https://www.lodashjs.com/docs/lodash.differenceWith)                                                                                                               |
| 交集 | [intersection](https://www.lodashjs.com/docs/lodash.intersection) | [intersectionBy](https://www.lodashjs.com/docs/lodash.intersectionBy) | [intersectionWith](https://www.lodashjs.com/docs/lodash.intersectionWith)                                                                                                           |
| 并集 | [union](https://www.lodashjs.com/docs/lodash.union)               | [unionBy](https://www.lodashjs.com/docs/lodash.unionBy)               | [unionWith](https://www.lodashjs.com/docs/lodash.unionWith)                                                                                                                         |
| 异或 | [xor](https://www.lodashjs.com/docs/lodash.xor)                   | [xorBy](https://www.lodashjs.com/docs/lodash.xorBy)                   | [xorWith](https://www.lodashjs.com/docs/lodash.xorWith)                                                                                                                             |
| 相减 | [pullAll](https://www.lodashjs.com/docs/lodash.pullAll)           | [pullAllBy](https://www.lodashjs.com/docs/lodash.pullAllBy)           | [pullAllWith](https://www.lodashjs.com/docs/lodash.pullAllWith)，跟[difference](https://www.lodashjs.com/docs/lodash.difference)不同的是，[pullAll](https://www.lodashjs.com/docs/lodash.pullAll)修改原数组 |
| 去重 | [uniq](https://www.lodashjs.com/docs/lodash.uniq)                 | [uniqBy](https://www.lodashjs.com/docs/lodash.uniqBy)                 | [uniqWith](https://www.lodashjs.com/docs/lodash.uniqWith)                                                                                                                           |
| 去重 | [sortedUniq](https://www.lodashjs.com/docs/lodash.sortedUniq)     | [sortedUniqBy](https://www.lodashjs.com/docs/lodash.sortedUniqBy)     |                                                                                                                                                                         |

### 获取数组某个位置上的元素

| 函数名                                | 主要参数-返回值 | 简介                                                                |
| ------------------------------------- | --------------- | ------------------------------------------------------------------- |
| [head](https://www.lodashjs.com/docs/lodash.head) | 数组=>元素      | 返回数组的第一个元素，和[first](https://www.lodashjs.com/docs/lodash.head)相同 |
| [last](https://www.lodashjs.com/docs/lodash.last) | 数组=>元素      | 返回数组的最后一个元素，和[head](https://www.lodashjs.com/docs/lodash.head)相反 |
| [nth](https://www.lodashjs.com/docs/lodash.nth)   | 数组=>元素      | 返回数组中某个位置上的元素                                          |

### 检测元素在数组中的索引

| 函数名                                                          | 简介                                                                                |
| --------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| [indexOf](https://www.lodashjs.com/docs/lodash.indexOf)                     | 获取元素在数组中的索引                                                              |
| [sortedIndexOf](https://www.lodashjs.com/docs/lodash.sortedIndexOf)         | 和[indexOf](https://www.lodashjs.com/docs/lodash.indexOf)功能一致，只是通过二分搜索方法         |
| [lastIndexOf](https://www.lodashjs.com/docs/lodash.lastIndexOf)             | 获取元素在数组中的索引，最后一次出现                                                |
| [sortedLastIndexOf](https://www.lodashjs.com/docs/lodash.sortedLastIndexOf) | 和[lastIndexOf](https://www.lodashjs.com/docs/lodash.lastIndexOf)功能一致，只是通过二分搜索方法 |
| [findIndex](https://www.lodashjs.com/docs/lodash.findIndex)                 | 寻找元素位置                                                                        |
| [findLastIndex](https://www.lodashjs.com/docs/lodash.findLastIndex)         | 寻找元素位置，从后往前                                                              |

### 检测元素在插在有序数组的什么位置

| 函数名                                                          | 简介                                                                      |
| --------------------------------------------------------------- | ------------------------------------------------------------------------- |
| [sortedIndex](https://www.lodashjs.com/docs/lodash.sortedIndex)             | 通过二分搜索判断元素应该插在数组的哪个位置                                |
| [sortedIndexBy](https://www.lodashjs.com/docs/lodash.sortedIndexBy)         | 同上，可以额外提供一个迭代器函数                                          |
| [sortedLastIndex](https://www.lodashjs.com/docs/lodash.sortedLastIndex)     | 和[sortedIndex](https://www.lodashjs.com/docs/lodash.sortedIndex)类似，但是从右边开始 |
| [sortedLastIndexBy](https://www.lodashjs.com/docs/lodash.sortedLastIndexBy) | 同上，可以额外提供一个迭代器函数                                          |

### 将数组拍平

| 函数名                                                | 主要参数-返回值    | 简介       |
| ----------------------------------------------------- | ------------------ | ---------- |
| [flatten](https://www.lodashjs.com/docs/lodash.flatten)                  | 高维数组=>低维数组 | 将数组拍平 |
| [flattenDeep](https://www.lodashjs.com/docs/lodash.flattenDeep)   | 高维数组=>数组     | 将数组拍平 |
| [flattenDepth](https://www.lodashjs.com/docs/lodash.flattenDepth) | 高维数组=>低维数组 | 将数组拍平 |

### Zip

| 函数名                                                  | 主要参数-返回值    | 简介                                                   |
| ------------------------------------------------------- | ------------------ | ------------------------------------------------------ |
| [zip](https://www.lodashjs.com/docs/lodash.zip)                     | 多个数组=>二维数组 | 可以理解为二维数组的行列互换                           |
| [zipWith](https://www.lodashjs.com/docs/lodash.zipWith)             | 多个数组=>数组     | 同上，但是可以自由处理行列互换后的数组中的每个数组元素 |
| [zipObject](https://www.lodashjs.com/docs/lodash.zipObject)         | 两个数组=>对象     | 把 keys 和 values 数组组成一个新对象                   |
| [zipObjectDeep](https://www.lodashjs.com/docs/lodash.zipObjectDeep) | 两个数组=>对象     | 同上，递归地处理属性名                                 |

### 未分类函数

| 函数名                                          | 主要参数-返回值 | 简介                                                                  |
| ----------------------------------------------- | --------------- | --------------------------------------------------------------------- |
| [chunk](https://www.lodashjs.com/docs/lodash.chunk)         | 数组=>二维数组  | 分段形成二维数组                                                      |
| [compact](https://www.lodashjs.com/docs/lodash.compact)     | 数组=>数组      | 移除假值                                                              |
| [concat](https://www.lodashjs.com/docs/lodash.concat)       | 多个数组=>数组  | 连接多个数组形成一个数组                                              |
| [fill](https://www.lodashjs.com/docs/lodash.fill)           | 数组=>数组      | 填充数组                                                              |
| [fromPairs](https://www.lodashjs.com/docs/lodash.fromPairs) | 二维数组=>对象  | 将键值数组变成对象。和[toPairs](https://www.lodashjs.com/docs/lodash.toPairs)相反 |
| [join](https://www.lodashjs.com/docs/lodash.join)           | 数组=>字符串    | 拼接数组元素成一个字符串                                              |

## 3、集合

为什么区分集合函数和数组函数？  
集合函数不单单适用于数组，还适用于字符串，对象，类数组对象（比如 Arguments，NodeList 等）。字符串是字符的集合，对象是属性值的集合。类数组对象是通过“[鸭子类型](https://zh.wikipedia.org/wiki/%E9%B8%AD%E5%AD%90%E7%B1%BB%E5%9E%8B)”工作的，所以如果你传入一个拥有`length`字段并且值为数字的对象，这个对象会被当做一个数组处理。具体请参考[Underscore.js](http://underscorejs.org/#collections)文档。

范例：

```js
function printKeyVal(val, key) {
  console.log(key, val);
}

//普通对象
_.each({ a: 1 }, printKeyVal);
//打印结果
// a 1

//拥有值为数字的length字段
_.each({ a: 1, length: 2 }, printKeyVal);
//打印结果
// 0 undefined
// 1 undefined
```

下面将分类介绍集合相关函数。

### 遍历

| 函数名                                          | 简介                                                    |
| ----------------------------------------------- | ------------------------------------------------------- |
| [each](https://www.lodashjs.com/docs/lodash.forEach)           | 同[forEach](https://www.lodashjs.com/docs/lodash.forEach)           |
| [eachRight](https://www.lodashjs.com/docs/lodash.forEachRight) | 同[forEachRight](https://www.lodashjs.com/docs/lodash.forEachRight) |

### 排序

| 函数名                                      | 简介                                                      |
| ------------------------------------------- | --------------------------------------------------------- |
| [sortBy](https://www.lodashjs.com/docs/lodash.sortBy)         | 排序                                                      |
| [orderBy](https://www.lodashjs.com/docs/lodash.orderBy) | 同[sortBy](https://www.lodashjs.com/docs/lodash.sortBy)，还可以指定正序倒序 |
| [shuffle](https://www.lodashjs.com/docs/lodash.shuffle) | 返回一个打乱顺序的新数组                                  |

### 过滤

| 函数名                                          | 简介                                       |
| ----------------------------------------------- | ------------------------------------------ |
| [filter](https://www.lodashjs.com/docs/lodash.filter)       | 创建一个新数组，包含了所有让断言为真的元素 |
| [reject](https://www.lodashjs.com/docs/lodash.reject)       | 创建一个新数组，包含了所有让断言为假的元素 |
| [partition](https://www.lodashjs.com/docs/lodash.partition) | 根据断言真假将一个集合分成两个集合         |

### Map 之后再 flatten

| 函数名                                                | 简介                                                                                           |
| ----------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| [flatMap](https://www.lodashjs.com/docs/lodash.flatMap)           | [map](https://www.lodashjs.com/docs/lodash.map)之后再[flatten](https://www.lodashjs.com/docs/lodash.flatten)           |
| [flatMapDeep](https://www.lodashjs.com/docs/lodash.flatMapDeep)   | [map](https://www.lodashjs.com/docs/lodash.map)之后再[flattenDeep](https://www.lodashjs.com/docs/lodash.flattenDeep)   |
| [flatMapDepth](https://www.lodashjs.com/docs/lodash.flatMapDepth) | [map](https://www.lodashjs.com/docs/lodash.map)之后再[flattenDepth](https://www.lodashjs.com/docs/lodash.flattenDepth) |

### 寻找元素

| 函数名                                        | 简介                       |
| --------------------------------------------- | -------------------------- |
| [find](https://www.lodashjs.com/docs/lodash.find)         | 找到第一个让断言为真的元素 |
| [findLast](https://www.lodashjs.com/docs/lodash.findLast) | 同上，逆序                 |

### 随机取值

| 函数名                                            | 简介                      |
| ------------------------------------------------- | ------------------------- |
| [sample](https://www.lodashjs.com/docs/lodash.sample)         | 从集合中随机选出一个元素  |
| [sampleSize](https://www.lodashjs.com/docs/lodash.sampleSize) | 从集合中随机选出 n 个元素 |

### 迭代

| 函数名                                              | 简介 |
| --------------------------------------------------- | ---- |
| [reduce](https://www.lodashjs.com/docs/lodash.reduce)           |      |
| [reduceRight](https://www.lodashjs.com/docs/lodash.reduceRight) | \-   |

### 分组计数

| 函数名                                      | 简介                                                                   |
| ------------------------------------------- | ---------------------------------------------------------------------- |
| [countBy](https://www.lodashjs.com/docs/lodash.countBy) | 返回一个对象，属性名是迭代器的返回值，属性值该返回值出现的次数         |
| [groupBy](https://www.lodashjs.com/docs/lodash.groupBy) | 返回一个对象，属性名是迭代器的返回值，属性值是一个包含了相应元素的数组 |

### 未分类

| 函数名                                          | 简介                                                                                               |
| ----------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| [keyBy](https://www.lodashjs.com/docs/lodash.keyBy)         | 返回一个对象，属性名是迭代器的返回值，属性值是元素本身                                             |
| [some](https://www.lodashjs.com/docs/lodash.)               | 对于集合中的每个元素，是否至少其一返回真值                                                         |
| [every](https://www.lodashjs.com/docs/lodash.every)         | 对于集合中的每个元素，是否都返回真值                                                               |
| [includes](https://www.lodashjs.com/docs/lodash.includes)   | 判断元素是不是在数组中，判断某个值是不是某个对象的属性值，判断一个字符串是不是包含在另一个字符串中 |
| [map](https://www.lodashjs.com/docs/lodash.map)             | 对集合的数组                                                                                       |
| [invokeMap](https://www.lodashjs.com/docs/lodash.invokeMap) | \-                                                                                                 |

## 4、对象

### 仅需要部分字段

| 函数名                                    | 简介                           |
| ----------------------------------------- | ------------------------------ |
| [omit](https://www.lodashjs.com/docs/lodash.omit)     | 通过忽略某些字段创建一个新对象 |
| [omitBy](https://www.lodashjs.com/docs/lodash.omitBy) |                                |
| [pick](https://www.lodashjs.com/docs/lodash.pick)     | 通过指定某些字段创建一个新对象 |
| [pickBy](https://www.lodashjs.com/docs/lodash.pickBy) | \-                             |

### 合并对象

| 函数名                                                | 简介                                                                           |
| ----------------------------------------------------- | ------------------------------------------------------------------------------ |
| [assign](https://www.lodashjs.com/docs/lodash.assign)             | 合并对象                                                                       |
| [assignWith](https://www.lodashjs.com/docs/lodash.assignWith)     | 有条件地合并对象                                                               |
| [extend](https://www.lodashjs.com/docs/lodash.extend)             | 合并对象，包括原型链上的属性                                                   |
| [extendWith](https://www.lodashjs.com/docs/lodash.extendWith)     | 有条件地合并对象，包括原型链上的属性                                           |
| [assignIn](https://www.lodashjs.com/docs/lodash.assignIn)         | 别名`extend`                                                                   |
| [assignInWith](https://www.lodashjs.com/docs/lodash.assignInWith) | 别名`extendWith`                                                               |
| [defaults](https://www.lodashjs.com/docs/lodash.defaults)         | 合并对象，将后面参数的属性付给第一个参数，如果第一个参数没有相应属性的话       |
| [defaultsDeep](https://www.lodashjs.com/docs/lodash.defaultsDeep) | 递归地合并对象，将后面参数的属性付给第一个参数，如果第一个参数没有相应属性的话 |
| [merge](https://www.lodashjs.com/docs/lodash.merge)               | 递归地合并对象，将后面参数的属性付给第一个参数                                 |
| [mergeWith](https://www.lodashjs.com/docs/lodash.mergeWith)       | 同[merge](https://www.lodashjs.com/docs/lodash.merge)，额外接受一个 customizer 参数        |

### 键值列表

| 函数名                                              | 简介                                                         |
| --------------------------------------------------- | ------------------------------------------------------------ |
| [keys](https://www.lodashjs.com/docs/lodash.keys)               | 创建一个数组，包含对象中所有的属性                           |
| [keysIn](https://www.lodashjs.com/docs/lodash.keysIn)           | 创建一个数组，包含对象中所有的属性（包含原型链上的）         |
| [functions](https://www.lodashjs.com/docs/lodash.functions)     | 创建一个数组，包含对象中所有值为函数的属性                   |
| [functionsIn](https://www.lodashjs.com/docs/lodash.functionsIn) | 创建一个数组，包含对象中所有值为函数的属性（包含原型链上的） |
| [values](https://www.lodashjs.com/docs/lodash.values)           | 创建一个数组，包含对象中所有的属性值                         |
| [valuesIn](https://www.lodashjs.com/docs/lodash.valuesIn)       | 创建一个数组，包含对象中所有的属性值（包含原型链上的）       |

### 赋值取值

| 函数名                                            | 简介                                                                               |
| ------------------------------------------------- | ---------------------------------------------------------------------------------- |
| [at](https://www.lodashjs.com/docs/lodash.at)                 | 获取对象的一组属性路径的值，肯定不会报错                                           |
| [get](https://www.lodashjs.com/docs/lodash.get)               | 获取对象的某个属性路径的值，肯定不会报错                                           |
| [result](https://www.lodashjs.com/docs/lodash.result)         | 同[get](https://www.lodashjs.com/docs/lodash.get)，但是如果属性值是函数的话，自动执行该函数    |
| [set](https://www.lodashjs.com/docs/lodash.set)               | 设置对象的某个属性路径的值                                                         |
| [setWith](https://www.lodashjs.com/docs/lodash.setWith)       | 设置对象的某个属性路径的值，遇到不存在的中间对象，使用数组呢？还是使用对象呢？等等 |
| [update](https://www.lodashjs.com/docs/lodash.update)         | 同[set](https://www.lodashjs.com/docs/lodash.set)，只是接受一个函数作为参数                    |
| [updateWith](https://www.lodashjs.com/docs/lodash.updateWith) | 同[setWith](https://www.lodashjs.com/docs/lodash.setWith)，只是接受一个函数作为参数            |
| [unset](https://www.lodashjs.com/docs/lodash.unset)           | 删除对象的某个属性路径                                                             |
| [invoke](https://www.lodashjs.com/docs/lodash.invoke)         | 调用对象中某个属性路径上的函数，肯定不会报错                                       |

### 键值数组

| 函数名                                          | 简介                                          |
| ----------------------------------------------- | --------------------------------------------- |
| [entries](https://www.lodashjs.com/docs/lodash.entries)     | \{‘a’:1\}=>\[\[‘a’,1\]\]。别名`toPairs`       |
| [entriesIn](https://www.lodashjs.com/docs/lodash.entriesIn) | 同上，但是包含原型链上的属性。别名`toPairsIn` |

### 键值变换

| 函数名                                          | 简介                                                                                  |
| ----------------------------------------------- | ------------------------------------------------------------------------------------- |
| [mapKeys](https://www.lodashjs.com/docs/lodash.mapKeys)     | 对对象中所有属性名做某种处理之后形成一个新对象                                        |
| [mapValues](https://www.lodashjs.com/docs/lodash.mapValues) | 对对象中所有属性值做某种处理之后形成一个新对象                                        |
| [invert](https://www.lodashjs.com/docs/lodash.invert)       | 将对象中的属性名和属性值互换转成一个新对象                                            |
| [invertBy](https://www.lodashjs.com/docs/lodash.invertBy)   | 同[invert](https://www.lodashjs.com/docs/lodash.invert)，但是转换以后的属性值是原属性值组成的数组 |

### 键值遍历

| 函数名                                              | 简介                                     |
| --------------------------------------------------- | ---------------------------------------- |
| [forIn](https://www.lodashjs.com/docs/lodash.forIn)             | 遍历对象上的所有属性，包含原型链上的。   |
| [forInRight](https://www.lodashjs.com/docs/lodash.forInRight)   | 遍历对象上的所有属性，包含原型链上的。   |
| [forOwn](https://www.lodashjs.com/docs/lodash.forOwn)           | 遍历对象上的所有属性，不包含原型链上的。 |
| [forOwnRight](https://www.lodashjs.com/docs/lodash.forOwnRight) | 遍历对象上的所有属性，不包含原型链上的。 |

### 寻找属性

| 函数名                                              | 简介                                                                                              |
| --------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| [findKey](https://www.lodashjs.com/docs/lodash.findKey)         | 同[find](https://www.lodashjs.com/docs/lodash.find)类似，但是匹配的是对象的属性值，返回的是对象的属性名       |
| [findLastKey](https://www.lodashjs.com/docs/lodash.findLastKey) | 同[findKey](https://www.lodashjs.com/docs/lodash.findKey)类似，但是匹配的是对象的属性值，返回的是对象的属性名 |

### 判断属性是否存在

| 函数名                                  | 简介                                         |
| --------------------------------------- | -------------------------------------------- |
| [has](https://www.lodashjs.com/docs/lodash.has)     | 判断对象上是否拥有某个属性，不包含原型链上的 |
| [hasIn](https://www.lodashjs.com/docs/lodash.hasIn) | 判断对象上是否拥有某个属性，包含原型链上的   |

### 转换对象或数组

| 函数名                                          | 简介                                                                                                    |
| ----------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
| [transform](https://www.lodashjs.com/docs/lodash.transform) | 同[reduce](https://www.lodashjs.com/docs/lodash.reduce)，但是其迭代器函数返回的是布尔值，如果返回 false，则停止迭代 |

### 创建新对象

| 函数名                                    | 简介                             |
| ----------------------------------------- | -------------------------------- |
| [create](https://www.lodashjs.com/docs/lodash.create) | 创建一个对象，并指定其原型和属性 |

## 5、函数

### 修改参数

| 函数名                                        | 简介                                                                                        |
| --------------------------------------------- | ------------------------------------------------------------------------------------------- |
| [ary](https://www.lodashjs.com/docs/lodash.ary)           | 创建一个包裹函数，只将前 n 个参数传递给原函数。                                             |
| [unary](https://www.lodashjs.com/docs/lodash.unary)       | 创建一个包裹函数，只将第一个参数传递给原函数。                                              |
| [flip](https://www.lodashjs.com/docs/lodash.flip)         | 创建一个包裹函数，将参数逆序之后传递给原函数。                                              |
| [rearg](https://www.lodashjs.com/docs/lodash.rearg)       | 创建一个包裹函数，调整参数顺序之后在传递给原函数                                            |
| [rest](https://www.lodashjs.com/docs/lodash.rest)         | 创建一个包裹函数，将参数合成数组之后传递给原函数                                            |
| [spread](https://www.lodashjs.com/docs/lodash.spread)     | 创建一个包裹函数，将数组参数展开之后传给原函数，跟[rest](https://www.lodashjs.com/docs/lodash.rest)相反 |
| [overArgs](https://www.lodashjs.com/docs/lodash.overArgs) | 创建一个包裹函数，将参数做处理之后再传递给原函数。                                          |

### 修改结果

| 函数名                                    | 简介                                   |
| ----------------------------------------- | -------------------------------------- |
| [negate](https://www.lodashjs.com/docs/lodash.negate) | 创建一个包裹函数，返回原函数结果的非。 |

### 缓存结果

| 函数名                                      | 简介                             |
| ------------------------------------------- | -------------------------------- |
| [memoize](https://www.lodashjs.com/docs/lodash.memoize) | 创建一个包裹函数，会缓存计算结果 |

### 降频调用

| 函数名                                        | 简介 |
| --------------------------------------------- | ---- |
| [debounce](https://www.lodashjs.com/docs/lodash.debounce) |      |
| [throttle](https://www.lodashjs.com/docs/lodash.throttle) |      |

### 延迟调用

| 函数名                                  | 简介                                 |
| --------------------------------------- | ------------------------------------ |
| [defer](https://www.lodashjs.com/docs/lodash.defer) | 类似`setTimeout(fn,0)`，可以指定参数 |
| [delay](https://www.lodashjs.com/docs/lodash.delay) | 类似`setTimeout(fn,x)`，可以指定参数 |

### 延迟调用

| 函数名                                    | 简介                                                          |
| ----------------------------------------- | ------------------------------------------------------------- |
| [once](https://www.lodashjs.com/docs/lodash.once)     | 创建一个包裹函数，确保原函数只被执行一次。                    |
| [before](https://www.lodashjs.com/docs/lodash.before) | 创建一个包裹函数，确保原函数只被执行 n 次。                   |
| [after](https://www.lodashjs.com/docs/lodash.after)   | 创建一个包裹函数，调用包裹函数时只有 n 次之后才会调用目标函数 |

### 固定参数

| 函数名                                                | 简介                                                                                                                     |
| ----------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| [wrap](https://www.lodashjs.com/docs/lodash.wrap)                 | 创建一个包裹函数，固定原函数的第一个参数                                                                                 |
| [partial](https://www.lodashjs.com/docs/lodash.partial)           | 创建一个包裹函数，固定原函数若干个参数                                                                                   |
| [partialRight](https://www.lodashjs.com/docs/lodash.partialRight) | 创建一个包裹函数，固定原函数若干个参数                                                                                   |
| [bind](https://www.lodashjs.com/docs/lodash.bind)                 | 创建一个包裹函数，固定原函数若干个参数，并指定 this 对象                                                                 |
| [bindKey](https://www.lodashjs.com/docs/lodash.bindKey)           | 和[bind](https://www.lodashjs.com/docs/lodash.bind)功能类似，但是能够处理尚未创建或被重写的函数，有点事件代理的感觉。                |
| [curry](https://www.lodashjs.com/docs/lodash.curry)               | 创建一个包裹函数，可以传入任意数量的参数，如果参数不完整，则返回一个接受余下参数的新函数，否则，调用原函数获得计算结果。 |
| [curryRight](https://www.lodashjs.com/docs/lodash.curryRight)     | 同上，逆序                                                                                                               |

## 6、字符串

### 书写格式

| 函数名                                          | 简介                           |
| ----------------------------------------------- | ------------------------------ |
| [startCase](https://www.lodashjs.com/docs/lodash.startCase) | 每个单词首字母大写，多用于标题 |
| [camelCase](https://www.lodashjs.com/docs/lodash.camelCase) | 小驼峰                         |
| [kebabCase](https://www.lodashjs.com/docs/lodash.kebabCase) | 小写连字符                     |
| [snakeCase](https://www.lodashjs.com/docs/lodash.snakeCase) | 小写下划线                     |
| [upperCase](https://www.lodashjs.com/docs/lodash.upperCase) | 大写加空格                     |
| [lowerCase](https://www.lodashjs.com/docs/lodash.lowerCase) | 小写加空格                     |

### 大写小写

| 函数名                                            | 简介                 |
| ------------------------------------------------- | -------------------- |
| [capitalize](https://www.lodashjs.com/docs/lodash.capitalize) | 首字母大写，其余小写 |
| [upperFirst](https://www.lodashjs.com/docs/lodash.upperFirst) | 首字母大写，其余不变 |
| [lowerFirst](https://www.lodashjs.com/docs/lodash.lowerFirst) | 首字母小写，其余不变 |
| [toUpper](https://www.lodashjs.com/docs/lodash.lowerFirst)    | 大写                 |
| [toLower](https://www.lodashjs.com/docs/lodash.toLower)       | 小写                 |

### 打头结尾

| 函数名                                            | 简介                   |
| ------------------------------------------------- | ---------------------- |
| [endsWith](https://www.lodashjs.com/docs/lodash.endsWith)     | 是不是以特定字符串结尾 |
| [startsWith](https://www.lodashjs.com/docs/lodash.startsWith) | 是不是以特定字符串打头 |

### 转义

| 函数名                                                | 简介                                                              |
| ----------------------------------------------------- | ----------------------------------------------------------------- |
| [escape](https://www.lodashjs.com/docs/lodash.escape)             | 转义 \&\<>”‘，与[unescape](https://www.lodashjs.com/docs/lodash.unescape)相反 |
| [escapeRegExp](https://www.lodashjs.com/docs/lodash.escapeRegExp) | 转义正则表达式中的特殊字符：\^\\\$.\*+\?\(\)\[\]\{\}\\            |

### 补全抹掉

| 函数名                                | 简介                                                                                                                                           |
| ------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| [pad](https://www.lodashjs.com/docs/lodash.pad)   | 使用某个字符串将特定字符串扩充至指定长度，类似地还有[padStart](https://www.lodashjs.com/docs/lodash.padStart)，[padEnd](https://www.lodashjs.com/docs/lodash.padEnd)   |
| [trim](https://www.lodashjs.com/docs/lodash.trim) | 去除字符串两边的特殊字符（默认为空格），类似地还有[trimStart](https://www.lodashjs.com/docs/lodash.trimStart)，[trimEnd](https://www.lodashjs.com/docs/lodash.trimEnd) |

### 未分类

| 函数名                                        | 简介                                 |
| --------------------------------------------- | ------------------------------------ |
| [parseInt](https://www.lodashjs.com/docs/lodash.parseInt) | 转成整型                             |
| [repeat](https://www.lodashjs.com/docs/lodash.repeat)     | 将某个字符串重复 n 遍                |
| [replace](https://www.lodashjs.com/docs/lodash.replace)   | 替换字符串                           |
| [split](https://www.lodashjs.com/docs/lodash.split)       | 拆分字符串                           |
| [template](https://www.lodashjs.com/docs/lodash.template) | 简单模板引擎                         |
| [truncate](https://www.lodashjs.com/docs/lodash.truncate) | 截断字符串                           |
| [words](https://www.lodashjs.com/docs/lodash.words)       | 将字符串拆分成单词，可以指定拆分模式 |
| [deburr](https://www.lodashjs.com/docs/lodash.deburr)     | 基本拉丁字母                         |

## 7、 数字

| 函数名                                      | 主要参数-返回值 | 简介                                                 |
| ------------------------------------------- | --------------- | ---------------------------------------------------- |
| [clamp](https://www.lodashjs.com/docs/lodash.clamp)     | 数字=>数字      | 将数字限定在一个范围内                               |
| [inRange](https://www.lodashjs.com/docs/lodash.inRange) | 数字=>布尔      | 判断数字是否在某个区间里                             |
| [random](https://www.lodashjs.com/docs/lodash.random)   | 区间=>数字      | 随机获取一个值，可以通过第三个参数指定是不是返回小数 |

## 8、 数学

### 加减乘除

| 函数名                                        | 主要参数-返回值  | 简介             |
| --------------------------------------------- | ---------------- | ---------------- |
| [add](https://www.lodashjs.com/docs/lodash.add)           | 两个数字 => 数字 | 返回两个数字的和 |
| [subtract](https://www.lodashjs.com/docs/lodash.subtract) | 两个数字 => 数字 | 返回两个数字的差 |
| [multiply](https://www.lodashjs.com/docs/lodash.multiply) | 两个数字 => 数字 | 返回两个数字的积 |
| [divide](https://www.lodashjs.com/docs/lodash.divide)     | 两个数字 => 数字 | 返回两个数字的商 |

### 和，最大值，最小值，平均值

| 函数名                                | 主要参数-返回值 | 简介                   |
| ------------------------------------- | --------------- | ---------------------- |
| [sum](https://www.lodashjs.com/docs/lodash.sum)   | 数组 => 数字    | 返回数组中的各数字之和 |
| [max](https://www.lodashjs.com/docs/lodash.max)   | 数组 => 数字    | 返回数组中的最大值     |
| [min](https://www.lodashjs.com/docs/lodash.min)   | 数组 => 数字    | 返回数组中的最小值     |
| [mean](https://www.lodashjs.com/docs/lodash.mean) | 数组 => 数字    | 返回数组中的平均值     |

### 数字精度

| 函数名                                  | 主要参数-返回值 | 简介                       |
| --------------------------------------- | --------------- | -------------------------- |
| [ceil](https://www.lodashjs.com/docs/lodash.ceil)   | 数字 => 数字    | 向上取整，可以指定精度     |
| [floor](https://www.lodashjs.com/docs/lodash.floor) | 数字 => 数字    | 向下取整，可以指定精度     |
| [round](https://www.lodashjs.com/docs/lodash.round) | 数字 => 数字    | 四舍五入取整，可以指定精度 |

## 9、语言

### 数值比较

| 函数名                                              | 简介                                       |
| --------------------------------------------------- | ------------------------------------------ |
| [eq](https://www.lodashjs.com/docs/lodash.eq)                   | 等价于`===`                                |
| [isEqual](https://www.lodashjs.com/docs/lodash.isEqual)         | 深度比较对象是否相等                       |
| [isEqualWith](https://www.lodashjs.com/docs/lodash.isEqualWith) | 深度比较对象是否相等，可以定义相等比较函数 |
| [gt](https://www.lodashjs.com/docs/lodash.gt)                   | 大于                                       |
| [lt](https://www.lodashjs.com/docs/lodash.lt)                   | 小于                                       |
| [gte](https://www.lodashjs.com/docs/lodash.gte)                 | 大于等于                                   |
| [lte](https://www.lodashjs.com/docs/lodash.lte)                 | 小于等于                                   |

### 类型判断

| 函数名                                                          | 简介                                   |
| --------------------------------------------------------------- | -------------------------------------- |
| [isArguments](https://www.lodashjs.com/docs/lodash.isArguments)             |                                        |
| [isArray](https://www.lodashjs.com/docs/lodash.isArray)                     |                                        |
| [isArrayBuffer](https://www.lodashjs.com/docs/lodash.isArrayBuffer)         |                                        |
| [isArrayLike](https://www.lodashjs.com/docs/lodash.isArrayLike)             |                                        |
| [isArrayLikeObject](https://www.lodashjs.com/docs/lodash.isArrayLikeObject) |                                        |
| [isBoolean](https://www.lodashjs.com/docs/lodash.isBoolean)                 |                                        |
| [isBuffer](https://www.lodashjs.com/docs/lodash.isBuffer)                   |                                        |
| [isDate](https://www.lodashjs.com/docs/lodash.isDate)                       |                                        |
| [isElement](https://www.lodashjs.com/docs/lodash.isElement)                 |                                        |
| [isEmpty](https://www.lodashjs.com/docs/lodash.isEmpty)                     | 判断是否有可遍历的属性                 |
| [isError](https://www.lodashjs.com/docs/lodash.isError)                     | 错误                                   |
| [isFinite](https://www.lodashjs.com/docs/lodash.isFinite)                   | 是否是有限的数字，基于 Number.isFinite |
| [isFunction](https://www.lodashjs.com/docs/lodash.isFunction)               |                                        |
| [isInteger](https://www.lodashjs.com/docs/lodash.isInteger)                 |                                        |
| [isLength](https://www.lodashjs.com/docs/lodash.isLength)                   |                                        |
| [isMap](https://www.lodashjs.com/docs/lodash.isMap)                         |                                        |
| [isMatch](https://www.lodashjs.com/docs/lodash.isMatch)                     |                                        |
| [isMatchWith](https://www.lodashjs.com/docs/lodash.isMatchWith)             |                                        |
| [isNaN](https://www.lodashjs.com/docs/lodash.isNaN)                         |                                        |
| [isNative](https://www.lodashjs.com/docs/lodash.isNative)                   | 原生函数                               |
| [isNil](https://www.lodashjs.com/docs/lodash.isNil)                         | 等价于 `_.isNull(val)`                 |
| [isNull](https://www.lodashjs.com/docs/lodash.isNull)                       |                                        |
| [isNumber](https://www.lodashjs.com/docs/lodash.isNumber)                   |                                        |
| [isObject](https://www.lodashjs.com/docs/lodash.isObject)                   |                                        |
| [isObjectLike](https://www.lodashjs.com/docs/lodash.isObjectLike)           |                                        |
| [isPlainObject](https://www.lodashjs.com/docs/lodash.isPlainObject)         |                                        |
| [isRegExp](https://www.lodashjs.com/docs/lodash.isRegExp)                   |                                        |
| [isSafeInteger](https://www.lodashjs.com/docs/lodash.isSafeInteger)         |                                        |
| [isSet](https://www.lodashjs.com/docs/lodash.)                              | isSet                                  |
| [isString](https://www.lodashjs.com/docs/lodash.isString)                   |                                        |
| [isSymbol](https://www.lodashjs.com/docs/lodash.isSymbol)                   |                                        |
| [isTypedArray](https://www.lodashjs.com/docs/lodash.isTypedArray)           |                                        |
| [isUndefined](https://www.lodashjs.com/docs/lodash.isUndefined)             |                                        |
| [isWeakMap](https://www.lodashjs.com/docs/lodash.isWeakMap)                 |                                        |
| [isWeakSet](https://www.lodashjs.com/docs/lodash.isWeakSet)                 |                                        |

### 类型转换

| 函数名                                                  | 简介                                                 |
| ------------------------------------------------------- | ---------------------------------------------------- |
| [castArray](https://www.lodashjs.com/docs/lodash.castArray)         | 强制转给数组                                         |
| [toArray](https://www.lodashjs.com/docs/lodash.toArray)             | 转成数组，对象调用 Object.values，字符串转成字符数组 |
| [toFinite](https://www.lodashjs.com/docs/lodash.toFinite)           |                                                      |
| [toInteger](https://www.lodashjs.com/docs/lodash.toInteger)         |                                                      |
| [toLength](https://www.lodashjs.com/docs/lodash.toLength)           |                                                      |
| [toNumber](https://www.lodashjs.com/docs/lodash.toNumber)           |                                                      |
| [toPlainObject](https://www.lodashjs.com/docs/lodash.toPlainObject) |                                                      |
| [toSafeInteger](https://www.lodashjs.com/docs/lodash.toSafeInteger) |                                                      |
| [toString](https://www.lodashjs.com/docs/lodash.toString)           | 转成字符串，                                         |

### 复制对象

| 函数名                                                  | 简介 |
| ------------------------------------------------------- | ---- |
| [clone](https://www.lodashjs.com/docs/lodash.clone)                 |      |
| [cloneDeep](https://www.lodashjs.com/docs/lodash.cloneDeep)         |      |
| [cloneDeepWith](https://www.lodashjs.com/docs/lodash.cloneDeepWith) |      |
| [cloneWith](https://www.lodashjs.com/docs/lodash.cloneWith)         |      |

### 检测对象

| 函数名                                            | 简介                               |
| ------------------------------------------------- | ---------------------------------- |
| [conformsTo](https://www.lodashjs.com/docs/lodash.conformsTo) | 判断一个对象的字段是否满足一些条件 |

## 10、工具

### 总是返回某个参数的函数

| 函数名                                        | 简介                                  |
| --------------------------------------------- | ------------------------------------- |
| [constant](https://www.lodashjs.com/docs/lodash.constant) | 创建一个包裹函数，总是返回第一个参数  |
| [nthArg](https://www.lodashjs.com/docs/lodash.nthArg)     | 创建一个包裹函数，总是返回第 n 个参数 |

### 总是返回某个特定值的函数

| 函数名                                            | 简介                      |
| ------------------------------------------------- | ------------------------- |
| [noop](https://www.lodashjs.com/docs/lodash.noop)             | 总是返回`undefined`的函数 |
| [stubArray](https://www.lodashjs.com/docs/lodash.stubArray)   | 总是返回空数组的函数      |
| [stubObject](https://www.lodashjs.com/docs/lodash.stubObject) | 总是返回空对象的函数      |
| [stubString](https://www.lodashjs.com/docs/lodash.stubString) | 总是返回空字符串的函数    |
| [stubTrue](https://www.lodashjs.com/docs/lodash.stubTrue)     | 总是返回`true`的函数      |
| [stubFalse](https://www.lodashjs.com/docs/lodash.stubFalse)   | 总是返回`false`的函数     |
| [identity](https://www.lodashjs.com/docs/lodash.identity)     | 总是返回第一个参数        |

### 获取对象的属性值或者调用对象的函数

| 函数名                                            | 简介                                                                                                      |
| ------------------------------------------------- | --------------------------------------------------------------------------------------------------------- |
| [method](https://www.lodashjs.com/docs/lodash.method)         | [\_.invoke\(object, path, \[args\]\)](https://www.lodashjs.com/docs/lodash.invoke)预设`path`和`args`两个参数          |
| [methodOf](https://www.lodashjs.com/docs/lodash.methodOf)     | [\_.invoke\(object, path, \[args\]\)](https://www.lodashjs.com/docs/lodash.invoke)预设`object`和`args`两个参数        |
| [property](https://www.lodashjs.com/docs/lodash.property)     | [\_.get\(object, path\)](https://www.lodashjs.com/docs/lodash.invoke)预设`path参数，不同的是缺少`defaultValue\`参数   |
| [propertyOf](https://www.lodashjs.com/docs/lodash.propertyOf) | [\_.get\(object, path\)](https://www.lodashjs.com/docs/lodash.invoke)预设`object`参数，不同的是缺少`defaultValue`参数 |

### 判断对象是否满足某些条件

| 函数名                                                      | 简介                                                                                                                                                               |
| ----------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [conforms](https://www.lodashjs.com/docs/lodash.conforms)               | 创建一个包裹函数，判断一个对象的字段是否满足某个函数。`conforms`意思是遵守。                                                                                       |
| [matches](https://www.lodashjs.com/docs/lodash.matches)                 | 创建一个包裹函数，判断一个对象的字段是否等于某个值，使用[isEqual](https://www.lodashjs.com/docs/lodash.isEqual)判断是否相等。跟[isMatch](https://www.lodashjs.com/docs/lodash.isMatch)类似 |
| [matchesProperty](https://www.lodashjs.com/docs/lodash.matchesProperty) | 创建一个包裹函数，判断一个对象特定字段是否等于某个值，使用[isEqual](https://www.lodashjs.com/docs/lodash.isEqual)判断是否相等。                                                |

### 把多个操作合成一个操作

| 函数名                                          | 简介                           |
| ----------------------------------------------- | ------------------------------ |
| [flow](https://www.lodashjs.com/docs/lodash.flow)           | 把一组函数串起来形成一个新函数 |
| [flowRight](https://www.lodashjs.com/docs/lodash.flowRight) | 同上，倒序                     |

### 批量进行多个操作

| 函数名                                          | 简介                                                           |
| ----------------------------------------------- | -------------------------------------------------------------- |
| [over](https://www.lodashjs.com/docs/lodash.over)           | 创建一个新函数，并将参数传递给预先指定的一组函数，并返回其结果 |
| [overEvery](https://www.lodashjs.com/docs/lodash.overEvery) | 跟`over`类似，判断是不是所有函数都返回真值                     |
| [overSome](https://www.lodashjs.com/docs/lodash.overSome)   | 跟`over`类似，判断是不是至少一个函数返回真值                   |

### 等差数列

| 函数名                                            | 简介                                                                          |
| ------------------------------------------------- | ----------------------------------------------------------------------------- |
| [range](https://www.lodashjs.com/docs/lodash.range)           | 生成等差数列，可以指定步长，步长可以是小数，也可以是负数                      |
| [rangeRight](https://www.lodashjs.com/docs/lodash.rangeRight) | 这个基本可以忽略，功能完成可以由[range](https://www.lodashjs.com/docs/lodash.range)代替。 |

### 其他未分类

| 函数名                                                | 简介                                                                                                        |
| ----------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| [attempt](https://www.lodashjs.com/docs/lodash.attempt)           | 使用 try-catch 包裹函数，如果出错返回错误对象                                                               |
| [bindAll](https://www.lodashjs.com/docs/lodash.bindAll)           | 将一个对象的多个函数中的 this 固定为该对象                                                                  |
| [cond](https://www.lodashjs.com/docs/lodash.cond)                 | 创建一个拥有复杂 if-else 的函数                                                                             |
| [defaultTo](https://www.lodashjs.com/docs/lodash.defaultTo)       | 如果第一个参数为 NaN,null,undefined，则返回第二个参数，否则返回第一个参数                                   |
| [iteratee](https://www.lodashjs.com/docs/lodash.iteratee)         | 创建一个迭代函数                                                                                            |
| [noConflict](https://www.lodashjs.com/docs/lodash.noConflict)     | 如果`_`被占用，可以使用该方法                                                                               |
| [runInContext](https://www.lodashjs.com/docs/lodash.runInContext) | 创建一个`lodash`镜像对象，可以扩展修改该对象                                                                |
| [mixin](https://www.lodashjs.com/docs/lodash.mixin)               | 给一个对象的原型添加属性或方法，一般配合[runInContext](https://www.lodashjs.com/docs/lodash.runInContext)扩展`lodash`。 |
| [times](https://www.lodashjs.com/docs/lodash.times)               | 执行函数 n 次，传入参数为 index                                                                             |
| [toPath](https://www.lodashjs.com/docs/lodash.toPath)             | ‘a\[0\].b.c’=>\[‘a’,’0’,’b’,’c’\]                                                                           |
| [uniqueId](https://www.lodashjs.com/docs/lodash.uniqueId)         | 生成唯一 ID，可以指定前缀                                                                                   |

## 11、 链式

### 链式调用的好处

省略了中间变量，让代码更加简洁，更加安全。  
链式调用可以优化成惰性求值（延迟计算），让代码更加高效。

### \_\(value\)

创建一个经过 lodash 包装过后的对象会启用隐式链，直到调用了不支持链接调用的函数或者主动调用`value`方法解除链式调用。  
作用类似于[chain](https://www.lodashjs.com/docs/lodash.chain)

### lodash 包装对象上的特殊函数

| 函数名                                              | 简介                                                                             |
| --------------------------------------------------- | -------------------------------------------------------------------------------- |
| [tap](https://www.lodashjs.com/docs/lodash.prototype-tap)       | 可以在链式调用中插入普通方法，直接修改中间结果，也可以仅仅是用于调试打印中间结果 |
| [thru](https://www.lodashjs.com/docs/lodash.prototype-thru)     | 同[tap](https://www.lodashjs.com/docs/lodash.tap)，但是使用函数的返回值作为中间结果          |
| [commit](https://www.lodashjs.com/docs/lodash.prototype-commit) | 立即执行链式调用中尚未进行的操作                                                 |
| [next](https://www.lodashjs.com/docs/lodash.prototype-next)     | 获得包装对象的下一个值                                                           |
| [plant](https://www.lodashjs.com/docs/lodash.prototype-plant)   | 复制一个链式调用，并传入初始值                                                   |
| [value](https://www.lodashjs.com/docs/lodash.prototype-value)   | 结束链式调用，并计算结果。别名`valueOf`，`toJSON`                                |


<!-- Lodash函数文档弹窗 -->
<div class="lo-layer">
  <div class="lo-layer-contentwrap">
    <div class="lo-layer-close">
        <svg  fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M6 18L18 6M6 6l12 12" /></svg>
    </div>
    <div class="lo-layer-content"></div>
  </div>
  <div class="lo-layer-mask"></div>
</div>

<!-- 引入页面JS -->
<script src="/public/lodash/lodashQuickQuery.js"></script>
<style>
.lo-layer {
    display: none;
}
.lo-layer-mask {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 300px;
    background-color: rgba(0, 0, 0, .3);
    z-index: 996;
}
.lo-layer-contentwrap {
    display: flex;
    position: fixed;
    top: 50%;
    left: calc(50% + 150px);
    transform: translate(-50%, -50%);
    width: 800px;
    max-height: 80%;
    z-index: 998;
}
.lo-layer-content {
    flex-grow: 1;
    padding: 0 50px 30px;
    border: 1px solid #aaa;
    border-radius: 3px;
    background-color: #fff;
    overflow-y: auto;
    box-shadow: 1px 1px 2px rgba(0, 0, 0, .1);
}
.lo-layer-content::-webkit-scrollbar {
    width: 6px;
    border-radius: 0 3px 3px 0;
    background: #f9f9f9;
}
.lo-layer-content::-webkit-scrollbar-thumb {
    background: #ddd;
}
.lo-layer-content h1,
.lo-layer-content h2 {
    margin-bottom: 30px;
    font-size: 30px;
}
.lo-layer-close {
    position: absolute;
    top: 20px;
    right: -31px;
    padding: 6px;
    background-color: #f9f9f9;
    border-radius: 0 3px 3px 0;
    z-index: 998;
}
.lo-layer-close svg {
    display: block;
    width: 20px;
    height: 20px;
    color: #999;
}
.lo-layer-close:hover svg {
    color: #555;
}
</style>