## stubObject


```js
_.stubObject()
```

这个方法返回一个空对象.

### 例子:

```js
var objects = _.times(2, _.stubObject);
 
console.log(objects);
// => [{}, {}]
 
console.log(objects[0] === objects[1]);
// => false
```
