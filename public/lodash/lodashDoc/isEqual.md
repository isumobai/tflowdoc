## isEqual


```js
_.isEqual(value, other)
```

执行深比较来确定两者的值是否相等。

 **注意: **这个方法支持比较 arrays, array buffers, booleans, date objects, error objects, maps, numbers, `Object` objects, regexes, sets, strings, symbols, 以及 typed arrays. `Object` 对象值比较自身的属性，不包括继承的和可枚举的属性。 **不**支持函数和DOM节点比较。

### 参数:

+ `value` (*) : 用来比较的值。  

+ `other` (*) : 另一个用来比较的值。  

### 例子:

```js
var object = { 'a': 1 };
var other = { 'a': 1 };
 
_.isEqual(object, other);
// => true
 
object === other;
// => false
```
