## sortedUniq


```js
_.sortedUniq(array)
```

这个方法类似`_.uniq`，除了它会优化排序数组。

### 参数:

+ `array` (Array) : 要检查的数组。  

### 例子:

```js
_.sortedUniq([1, 1, 2]);
// => [1, 2]
```
