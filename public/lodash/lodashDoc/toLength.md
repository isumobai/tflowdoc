## toLength


```js
_.toLength(value)
```

转换 `value` 为用作类数组对象的长度整数。

**注意:** 这个方法基于`ToLength`.

### 参数:

+ `value` (*) : 要转换的值。  

### 例子:

```js
_.toLength(3.2);
// => 3
 
_.toLength(Number.MIN_VALUE);
// => 0
 
_.toLength(Infinity);
// => 4294967295
 
_.toLength('3.2');
// => 3
```
