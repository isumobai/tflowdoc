## pad


```js
_.pad([string=''], [length=0], [chars=' '])
```

如果`string`字符串长度小于 `length` 则从左侧和右侧填充字符。 如果没法平均分配，则截断超出的长度。

### 参数:

+ `[string='']` (string) : 要填充的字符串。  

+ `[length=0]` (number) : 填充的长度。  

+ `[chars=' ']` (string) : 填充字符。  

### 例子:

```js
_.pad('abc', 8);
// => '  abc   '
 
_.pad('abc', 8, '_-');
// => '_-abc_-_'
 
_.pad('abc', 3);
// => 'abc'
```
