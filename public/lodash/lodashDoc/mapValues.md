## mapValues


```js
_.mapValues(object, [iteratee=_.identity])
```

创建一个对象，这个对象的key与`object`对象相同，值是通过 `iteratee` 运行 `object` 中每个自身可枚举属性名字符串产生的。 `iteratee`调用三个参数： <em>(value, key, object)</em>。

### 参数:

+ `object` (Object) : 要遍历的对象。  

+ `[iteratee=_.identity]` (Function) : 每次迭代时调用的函数。  

### 例子:

```js
var users = {
  'fred':    { 'user': 'fred',    'age': 40 },
  'pebbles': { 'user': 'pebbles', 'age': 1 }
};
 
_.mapValues(users, function(o) { return o.age; });
// => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
 
// The `_.property` iteratee shorthand.
_.mapValues(users, 'age');
// => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
```
