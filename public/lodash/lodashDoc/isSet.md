## isSet


```js
_.isSet(value)
```

检查 `value` 是否是一个`Set`对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isSet(new Set);
// => true
 
_.isSet(new WeakSet);
// => false
```
