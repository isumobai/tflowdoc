## omitBy


```js
_.omitBy(object, [predicate=_.identity])
```

反向版`_.pickBy`；这个方法一个对象，这个对象忽略 `predicate`（断言函数）判断不是真值的属性后，`object`自身和继承的可枚举属性组成。`predicate`调用与2个参数：<em>(value, key)</em>。

### 参数:

+ `object` (Object) : 来源对象。  

+ `[predicate=_.identity]` (Function) : 调用每一个属性的函数。  

### 例子:

```js
var object = { 'a': 1, 'b': '2', 'c': 3 };
 
_.omitBy(object, _.isNumber);
// => { 'b': '2' }
```
