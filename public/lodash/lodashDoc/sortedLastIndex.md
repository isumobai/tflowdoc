## sortedLastIndex


```js
_.sortedLastIndex(array, value)
```

此方法类似于`_.sortedIndex`，除了 它返回 `value`值 在 `array` 中尽可能大的索引位置（index）。

### 参数:

+ `array` (Array) : 要检查的排序数组。  

+ `value` (*) : 要评估的值。  

### 例子:

```js
_.sortedLastIndex([4, 5, 5, 5, 6], 5);
// => 4
```
