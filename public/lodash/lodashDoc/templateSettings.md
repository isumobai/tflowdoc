## templateSettings


```js
_.templateSettings
```

(Object): 默认情况下，lodash使用的模板分隔符就像那他们嵌入到Ruby（ERB）一样。更改以下模板设置使用替代分隔符。

