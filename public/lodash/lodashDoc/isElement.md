## isElement


```js
_.isElement(value)
```

检查 `value` 是否是可能是 DOM 元素。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isElement(document.body);
// => true
 
_.isElement('<body>');
// => false
```
