## repeat


```js
_.repeat([string=''], [n=1])
```

重复 N 次给定字符串。

### 参数:

+ `[string='']` (string) : 要重复的字符串。  

+ `[n=1]` (number) : 重复的次数。  

### 例子:

```js
_.repeat('*', 3);
// => '***'
 
_.repeat('abc', 2);
// => 'abcabc'
 
_.repeat('abc', 0);
// => ''
```
