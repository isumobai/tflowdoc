## stubArray


```js
_.stubArray()
```

这个方法返回一个新的空数组。

### 例子:

```js
var arrays = _.times(2, _.stubArray);
 
console.log(arrays);
// => [[], []]
 
console.log(arrays[0] === arrays[1]);
// => false
```
