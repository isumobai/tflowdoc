## isInteger


```js
_.isInteger(value)
```

检查 `value` 是否为一个整数。

**注意:** 这个方法基于`Number.isInteger`.

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isInteger(3);
// => true
 
_.isInteger(Number.MIN_VALUE);
// => false
 
_.isInteger(Infinity);
// => false
 
_.isInteger('3');
// => false
```
