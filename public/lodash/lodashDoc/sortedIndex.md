## sortedIndex


```js
_.sortedIndex(array, value)
```

使用二进制的方式检索来决定 `value`值 应该插入到数组中 尽可能小的索引位置，以保证`array`的排序。

### 参数:

+ `array` (Array) : 要检查的排序数组。  

+ `value` (*) : 要评估的值。  

### 例子:

```js
_.sortedIndex([30, 50], 40);
// => 1
```
