## capitalize


```js
_.capitalize([string=''])
```

转换字符串`string`首字母为大写，剩下为小写。

### 参数:

+ `[string='']` (string) : 要大写开头的字符串。  

### 例子:

```js
_.capitalize('FRED');
// => 'Fred'
```
