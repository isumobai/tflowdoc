## invoke


```js
_.invoke(object, path, [args])
```

调用`object`对象`path`上的方法。

### 参数:

+ `object` (Object) : 要检索的对象。  

+ `path` (Array|string) : 用来调用的方法路径。  

+ `[args]` (...*) : 调用的方法的参数。  

### 例子:

```js
var object = { 'a': [{ 'b': { 'c': [1, 2, 3, 4] } }] };
 
_.invoke(object, 'a[0].b.c.slice', 1, 3);
// => [2, 3]
```
