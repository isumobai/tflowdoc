## defer


```js
_.defer(func, [args])
```

推迟调用`func`，直到当前堆栈清理完毕。 调用时，任何附加的参数会传给`func`。

### 参数:

+ `func` (Function) : 要延迟的函数。  

+ `[args]` (...*) : 会在调用时传给 `func` 的参数。  

### 例子:

```js
_.defer(function(text) {
  console.log(text);
}, 'deferred');
// => 一毫秒或更久一些输出 'deferred'。
```
