## isNaN


```js
_.isNaN(value)
```

检查 `value` 是否是 `NaN`。

**注意:** 这个方法基于`Number.isNaN`，和全局的`isNaN` 不同之处在于，全局的`isNaN`对 于 `undefined` 和其他非数字的值返回 `true`。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isNaN(NaN);
// => true
 
_.isNaN(new Number(NaN));
// => true
 
isNaN(undefined);
// => true
 
_.isNaN(undefined);
// => false
```
