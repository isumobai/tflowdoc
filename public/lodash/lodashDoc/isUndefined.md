## isUndefined


```js
_.isUndefined(value)
```

检查 `value` 是否是 `undefined`.

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isUndefined(void 0);
// => true
 
_.isUndefined(null);
// => false
```
