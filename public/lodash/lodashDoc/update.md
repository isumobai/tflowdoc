## update


```js
_.update(object, path, updater)
```

该方法类似`_.set`，除了接受`updater`以生成要设置的值。使用`_.updateWith`来自定义生成的新`path`。`updater`调用1个参数：<em>(value)</em>。

**Note:** 这个方法会改变 `object`。

### 参数:

+ `object` (Object) : 要修改的对象。  

+ `path` (Array|string) : 要设置属性的路径。  

+ `updater` (Function) : 用来生成设置值的函数。  

### 例子:

```js
var object = { 'a': [{ 'b': { 'c': 3 } }] };
 
_.update(object, 'a[0].b.c', function(n) { return n * n; });
console.log(object.a[0].b.c);
// => 9
 
_.update(object, 'x[0].y.z', function(n) { return n ? n + 1 : 0; });
console.log(object.x[0].y.z);
// => 0
```
