## camelCase


```js
_.camelCase([string=''])
```

转换字符串`string`为驼峰写法。

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.camelCase('Foo Bar');
// => 'fooBar'
 
_.camelCase('--foo-bar--');
// => 'fooBar'
 
_.camelCase('__FOO_BAR__');
// => 'fooBar'
```
