## toLower


```js
_.toLower([string=''])
```

转换整个`string`字符串的字符为小写，类似String#toLowerCase。

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.toLower('--Foo-Bar--');
// => '--foo-bar--'
 
_.toLower('fooBar');
// => 'foobar'
 
_.toLower('__FOO_BAR__');
// => '__foo_bar__'
```
