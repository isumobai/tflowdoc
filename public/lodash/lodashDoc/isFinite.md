## isFinite


```js
_.isFinite(value)
```

检查 `value` 是否是原始有限数值。

 ** 注意:** 这个方法基于`Number.isFinite`.

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isFinite(3);
// => true
 
_.isFinite(Number.MIN_VALUE);
// => true
 
_.isFinite(Infinity);
// => false
 
_.isFinite('3');
// => false
```
