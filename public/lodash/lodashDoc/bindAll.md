## bindAll


```js
_.bindAll(object, methodNames)
```

绑定一个对象的方法到对象本身，覆盖现有的方法。

**注意:** 这个方法不会设置绑定函数的 "length" 属性。

### 参数:

+ `object` (Object) : 用来绑定和分配绑定方法的对象。  

+ `methodNames` (...(string|string[])) : 对象绑定方法的名称。  

### 例子:

```js
var view = {
  'label': 'docs',
  'click': function() {
    console.log('clicked ' + this.label);
  }
};
 
_.bindAll(view, ['click']);
jQuery(element).on('click', view.click);
// => Logs 'clicked docs' when clicked.
```
