## omit


```js
_.omit(object, [props])
```

反向版`_.pick`; 这个方法一个对象，这个对象由忽略属性之外的`object`自身和继承的可枚举属性组成。（注：可以理解为删除`object`对象的属性）。

### 参数:

+ `object` (Object) : 来源对象。  

+ `[props]` (...(string|string[])) : 要被忽略的属性。（注：单独指定或指定在数组中。）  

### 例子:

```js
var object = { 'a': 1, 'b': '2', 'c': 3 };
 
_.omit(object, ['a', 'c']);
// => { 'b': '2' }
```
