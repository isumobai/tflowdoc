## has


```js
_.has(object, path)
```

检查 `path` 是否是`object`对象的直接属性。

### 参数:

+ `object` (Object) : 要检索的对象。  

+ `path` (Array|string) : 要检查的路径`path`。  

### 例子:

```js
var object = { 'a': { 'b': 2 } };
var other = _.create({ 'a': _.create({ 'b': 2 }) });
 
_.has(object, 'a');
// => true
 
_.has(object, 'a.b');
// => true
 
_.has(object, ['a', 'b']);
// => true
 
_.has(other, 'a');
// => false
```
