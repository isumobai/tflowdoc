## add


```js
_.add(augend, addend)
```

两个数相加。

### 参数:

+ `augend` (number) : 相加的第一个数。  

+ `addend` (number) : 相加的第二个数。  

### 例子:

```js
_.add(6, 4);
// => 10
```
