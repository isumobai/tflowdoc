## bind


```js
_.bind(func, thisArg, [partials])
```

创建一个调用`func`的函数，`thisArg`绑定`func`函数中的 `this` (注：`this`的上下文为`thisArg`) ，并且`func`函数会接收`partials`附加参数。

`_.bind.placeholder`值，默认是以 `_` 作为附加部分参数的占位符。

**注意:** 不同于原生的 `Function#bind`，这个方法不会设置绑定函数的 "length" 属性。

### 参数:

+ `func` (Function) : 绑定的函数。  

+ `thisArg` (*) : `func` 绑定的`this`对象。  

+ `[partials]` (...*) : 附加的部分参数。  

### 例子:

```js
var greet = function(greeting, punctuation) {
  return greeting + ' ' + this.user + punctuation;
};
 
var object = { 'user': 'fred' };
 
var bound = _.bind(greet, object, 'hi');
bound('!');
// => 'hi fred!'
 
// Bound with placeholders.
var bound = _.bind(greet, object, _, '!');
bound('hi');
// => 'hi fred!'
```
