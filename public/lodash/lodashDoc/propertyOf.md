## propertyOf


```js
_.propertyOf(object)
```

`_.property`的反相版本。 这个方法创建的函数返回给定 path 在`object`上的值。

### 参数:

+ `object` (Object) : 要检索的对象。  

### 例子:

```js
var array = [0, 1, 2],
    object = { 'a': array, 'b': array, 'c': array };
 
_.map(['a[2]', 'c[0]'], _.propertyOf(object));
// => [2, 0]
 
_.map([['a', '2'], ['c', '0']], _.propertyOf(object));
// => [2, 0]
```
