## unescape


```js
_.unescape([string=''])
```

`_.escape`的反向版。 这个方法转换`string`字符串中的 HTML 实体 `&amp;amp;`, `&amp;lt;`, `&amp;gt;`, `&amp;quot;`, `&amp;#39;`, 和 `&amp;#96;` 为对应的字符。

 注意: 不会转换其他的 HTML 实体，需要转换可以使用类似 he 的第三方库。

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.unescape('fred, barney, & pebbles');
// => 'fred, barney, & pebbles'
```
