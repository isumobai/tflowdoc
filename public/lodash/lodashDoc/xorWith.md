## xorWith


```js
_.xorWith([arrays], [comparator])
```

该方法是像`_.xor`，除了它接受一个 `comparator` ，以调用比较数组的元素。 comparator 调用2个参数：<em>(arrVal, othVal)</em>.

### 参数:

+ `[arrays]` (...Array) : 要检查的数组。  

+ `[comparator]` (Function) : 调用每一个元素的比较函数。  

### 例子:

```js
var objects = [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }];
var others = [{ 'x': 1, 'y': 1 }, { 'x': 1, 'y': 2 }];
 
_.xorWith(objects, others, _.isEqual);
// => [{ 'x': 2, 'y': 1 }, { 'x': 1, 'y': 1 }]
```
