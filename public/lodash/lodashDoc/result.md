## result


```js
_.result(object, path, [defaultValue])
```

这个方法类似`_.get`， 除了如果解析到的值是一个函数的话，就绑定 `this` 到这个函数并返回执行后的结果。

### 参数:

+ `object` (Object) : 要检索的对象。  

+ `path` (Array|string) : 要解析的属性路径。  

+ `[defaultValue]` (*) : 如果值解析为 `undefined`，返回这个值。  

### 例子:

```js
var object = { 'a': [{ 'b': { 'c1': 3, 'c2': _.constant(4) } }] };
 
_.result(object, 'a[0].b.c1');
// => 3
 
_.result(object, 'a[0].b.c2');
// => 4
 
_.result(object, 'a[0].b.c3', 'default');
// => 'default'
 
_.result(object, 'a[0].b.c3', _.constant('default'));
// => 'default'
```
