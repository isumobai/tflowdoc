## rangeRight


```js
_.rangeRight([start=0], end, [step=1])
```

这个方法类似`_.range` ， 除了它是降序生成值的。

### 参数:

+ `[start=0]` (number) : 开始的范围。  

+ `end` (number) : 结束的范围。  

+ `[step=1]` (number) :范围的增量 或者 减量。  

### 例子:

```js
_.rangeRight(4);
// => [3, 2, 1, 0]
 
_.rangeRight(-4);
// => [-3, -2, -1, 0]
 
_.rangeRight(1, 5);
// => [4, 3, 2, 1]
 
_.rangeRight(0, 20, 5);
// => [15, 10, 5, 0]
 
_.rangeRight(0, -4, -1);
// => [-3, -2, -1, 0]
 
_.rangeRight(1, 4, 0);
// => [1, 1, 1]
 
_.rangeRight(0);
// => []
```
