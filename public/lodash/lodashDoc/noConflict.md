## noConflict


```js
_.noConflict()
```

释放 `_` 变量为原来的值，并返回一个 `lodash` 的引用。

### 例子:

```js
var lodash = _.noConflict();
```
