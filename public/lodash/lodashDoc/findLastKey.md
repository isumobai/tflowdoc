## findLastKey


```js
_.findLastKey(object, [predicate=_.identity])
```

这个方法类似`_.findKey`。 不过它是反方向开始遍历的。

### 参数:

+ `object` (Object) : 需要检索的对象。  

+ `[predicate=_.identity]` (Function) : 每次迭代时调用的函数。  

### 例子:

```js
var users = {
  'barney':  { 'age': 36, 'active': true },
  'fred':    { 'age': 40, 'active': false },
  'pebbles': { 'age': 1,  'active': true }
};
 
_.findLastKey(users, function(o) { return o.age < 40; });
// => returns 'pebbles' assuming `_.findKey` returns 'barney'
 
// The `_.matches` iteratee shorthand.
_.findLastKey(users, { 'age': 36, 'active': true });
// => 'barney'
 
// The `_.matchesProperty` iteratee shorthand.
_.findLastKey(users, ['active', false]);
// => 'fred'
 
// The `_.property` iteratee shorthand.
_.findLastKey(users, 'active');
// => 'pebbles'
```
