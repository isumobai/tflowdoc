## matchesProperty


```js
_.matchesProperty(path, srcValue)
```

创建一个深比较的方法来比较给定对象的 `path` 的值是否是 `srcValue` 。 如果是返回 `true` ，否则返回 `false` 。

**注意:** 这个方法支持以`_.isEqual` 的方式比较相同的值。

### 参数:

+ `path` (Array|string) : 给定对象的属性路径名。  

+ `srcValue` (*) : 要匹配的值。  

### 例子:

```js
var objects = [
  { 'a': 1, 'b': 2, 'c': 3 },
  { 'a': 4, 'b': 5, 'c': 6 }
];
 
_.find(objects, _.matchesProperty('a', 4));
// => { 'a': 4, 'b': 5, 'c': 6 }
```
