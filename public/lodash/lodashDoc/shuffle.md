## shuffle


```js
_.shuffle(collection)
```

创建一个被打乱值的集合。 使用Fisher-Yates shuffle 版本。

### 参数:

+ `collection` (Array|Object) : 要打乱的集合。  

### 例子:

```js
_.shuffle([1, 2, 3, 4]);
// => [4, 1, 3, 2]
```
