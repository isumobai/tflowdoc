## lowerCase


```js
_.lowerCase([string=''])
```

转换字符串`string`以空格分开单词，并转换为小写。

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.lowerCase('--Foo-Bar--');
// => 'foo bar'
 
_.lowerCase('fooBar');
// => 'foo bar'
 
_.lowerCase('__FOO_BAR__');
// => 'foo bar'
```
