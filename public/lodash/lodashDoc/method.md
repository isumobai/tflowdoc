## method


```js
_.method(path, [args])
```

创建一个调用给定对象 `path` 上的函数。 任何附加的参数都会传入这个调用函数中。

### 参数:

+ `path` (Array|string) : 调用函数所在对象的路径。  

+ `[args]` (...*) : 传递给调用函数的参数。  

### 例子:

```js
var objects = [
  { 'a': { 'b': _.constant(2) } },
  { 'a': { 'b': _.constant(1) } }
];
 
_.map(objects, _.method('a.b'));
// => [2, 1]
 
_.map(objects, _.method(['a', 'b']));
// => [2, 1]
```
