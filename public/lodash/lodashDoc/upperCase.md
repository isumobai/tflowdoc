## upperCase


```js
_.upperCase([string=''])
```

转换字符串`string`为 空格 分隔的大写单词。

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.upperCase('--foo-bar');
// => 'FOO BAR'
 
_.upperCase('fooBar');
// => 'FOO BAR'
 
_.upperCase('__foo_bar__');
// => 'FOO BAR'
```
