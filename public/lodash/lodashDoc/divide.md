## divide


```js
_.divide(dividend, divisor)
```

两个数相除。

### 参数:

+ `dividend` (number) : 相除的第一个数。  

+ `divisor` (number) : 相除的第二个数。  

### 例子:

```js
_.divide(6, 4);
// => 1.5
```
