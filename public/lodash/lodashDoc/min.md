## min


```js
_.min(array)
```

计算 `array` 中的最小值。 如果 `array` 是 空的或者假值将会返回 `undefined`。

### 参数:

+ `array` (Array) : 要迭代的数组。  

### 例子:

```js
_.min([4, 2, 8, 6]);
// => 2
 
_.min([]);
// => undefined
```
