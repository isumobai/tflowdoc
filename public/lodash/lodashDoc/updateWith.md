## updateWith


```js
_.updateWith(object, path, updater, [customizer])
```

该方法类似`_.update`，不同之处在于它接受`customizer`，调用来生成新的对象的`path`。如果`customizer`返回`undefined`，路径创建由该方法代替。`customizer`调用有三个参数：<em>(nsValue, key, nsObject)</em> 。

**Note:** 这个方法会改变 `object`.

### 参数:

+ `object` (Object) : 要修改的对象。  

+ `path` (Array|string) : 要设置属性的路径。  

+ `updater` (Function) : 用来生成设置值的函数。  

+ `[customizer]` (Function) : 用来自定义分配值的函数。  

### 例子:

```js
var object = {};
 
_.updateWith(object, '[0][1]', _.constant('a'), Object);
// => { '0': { '1': 'a' } }
```
