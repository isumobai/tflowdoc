## uniq


```js
_.uniq(array)
```

创建一个去重后的`array`数组副本。使用了`SameValueZero` 做等值比较。只有第一次出现的元素才会被保留。

### 参数:

+ `array` (Array) : 要检查的数组。  

### 例子:

```js
_.uniq([2, 1, 2]);
// => [2, 1]
```
