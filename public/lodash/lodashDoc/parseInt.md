## parseInt


```js
_.parseInt(string, [radix=10])
```

转换`string`字符串为指定基数的整数。 如果基数是 `undefined` 或者 `0`，则`radix`基数默认是`10`，如果`string`字符串是16进制，则`radix`基数为 `16`。

**注意:** 这个方法与ES5 implementation 的 `parseInt`是一样的。

### 参数:

+ `string` (string) : 要转换的字符串。  

+ `[radix=10]` (number) :转换基数。  

### 例子:

```js
_.parseInt('08');
// => 8
 
_.map(['6', '08', '10'], _.parseInt);
// => [6, 8, 10]
```
