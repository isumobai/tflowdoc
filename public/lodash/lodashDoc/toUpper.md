## toUpper


```js
_.toUpper([string=''])
```

转换整个`string`字符串的字符为大写，类似String#toUpperCase.

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.toUpper('--foo-bar--');
// => '--FOO-BAR--'
 
_.toUpper('fooBar');
// => 'FOOBAR'
 
_.toUpper('__foo_bar__');
// => '__FOO_BAR__'
```
