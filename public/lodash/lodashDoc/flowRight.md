## flowRight


```js
_.flowRight([funcs])
```

这个方法类似`_.flow`，除了它调用函数的顺序是从右往左的。

### 参数:

+ `[funcs]` (...(Function|Function[])) : 要调用的函数。  

### 例子:

```js
function square(n) {
  return n * n;
}
 
var addSquare = _.flowRight([square, _.add]);
addSquare(1, 2);
// => 9
```
