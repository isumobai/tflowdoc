## times


```js
_.times(n, [iteratee=_.identity])
```

调用 iteratee `n` 次，每次调用返回的结果存入到数组中。 iteratee 调用入1个参数： <em>(index)</em>。

### 参数:

+ `n` (number) : 调用 `iteratee` 的次数。  

+ `[iteratee=_.identity]` (Function) : 每次迭代调用的函数。  

### 例子:

```js
_.times(3, String);
// => ['0', '1', '2']
 
 _.times(4, _.constant(0));
// => [0, 0, 0, 0]
```
