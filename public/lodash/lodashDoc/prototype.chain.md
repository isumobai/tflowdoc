## prototype.chain


```js
_.prototype.chain()
```

创建一个`lodash`包装实例，启用显式链模式。

### 例子:

```js
var users = [
  { 'user': 'barney', 'age': 36 },
  { 'user': 'fred',   'age': 40 }
];
 
// 不启用显式链
_(users).head();
// => { 'user': 'barney', 'age': 36 }
 
// 启用显式链
_(users)
  .chain()
  .head()
  .pick('user')
  .value();
// => { 'user': 'barney' }
```
