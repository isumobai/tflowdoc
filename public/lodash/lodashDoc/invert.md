## invert


```js
_.invert(object)
```

创建一个`object`键值倒置后的对象。 如果 `object` 有重复的值，后面的值会覆盖前面的值。

### 参数:

+ `object` (Object) : 要键值倒置对象。  

### 例子:

```js
var object = { 'a': 1, 'b': 2, 'c': 1 };
 
_.invert(object);
// => { '1': 'c', '2': 'b' }
```
