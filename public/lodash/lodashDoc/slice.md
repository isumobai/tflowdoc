## slice


```js
_.slice(array, [start=0], [end=array.length])
```

裁剪数组`array`，从 `start` 位置开始到`end`结束，但不包括 `end` 本身的位置。

**Note:** 这个方法用于代替`Array#slice` 来确保数组正确返回。

### 参数:

+ `array` (Array) : 要裁剪数组。  

+ `[start=0]` (number) : 开始位置。  

+ `[end=array.length]` (number) : 结束位置。  

