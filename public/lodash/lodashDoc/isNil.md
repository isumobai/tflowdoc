## isNil


```js
_.isNil(value)
```

检查 `value` 是否是 `null` 或者 `undefined`。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isNil(null);
// => true
 
_.isNil(void 0);
// => true
 
_.isNil(NaN);
// => false
```
