## ceil


```js
_.ceil(number, [precision=0])
```

根据 `precision`（精度） 向上舍入 `number`。（注： `precision`（精度）可以理解为保留几位小数。）

### 参数:

+ `number` (number) : 要向上舍入的值。  

+ `[precision=0]` (number) : 向上舍入的的精度。  

### 例子:

```js
_.ceil(4.006);
// => 5
 
_.ceil(6.004, 2);
// => 6.01
 
_.ceil(6040, -2);
// => 6100
```
