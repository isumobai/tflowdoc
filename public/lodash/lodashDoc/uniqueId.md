## uniqueId


```js
_.uniqueId([prefix=''])
```

生成唯一ID。 如果提供了 `prefix` ，会被添加到ID前缀上。

### 参数:

+ `[prefix='']` (string) : 要添加到ID前缀的值。  

### 例子:

```js
_.uniqueId('contact_');
// => 'contact_104'
 
_.uniqueId();
// => '105'
```
