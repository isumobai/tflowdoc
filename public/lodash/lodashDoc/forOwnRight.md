## forOwnRight


```js
_.forOwnRight(object, [iteratee=_.identity])
```

这个方法类似`_.forOwn`。 除了它是反方向开始遍历`object`的。

### 参数:

+ `object` (Object) : 要遍历的对象。  

+ `[iteratee=_.identity]` (Function) : 每次迭代时调用的函数。  

### 例子:

```js
function Foo() {
  this.a = 1;
  this.b = 2;
}
 
Foo.prototype.c = 3;
 
_.forOwnRight(new Foo, function(value, key) {
  console.log(key);
});
// =>  输出 'b' 然后 'a'， `_.forOwn` 会输出 'a' 然后 'b'
```
