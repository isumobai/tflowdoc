## toPlainObject


```js
_.toPlainObject(value)
```

转换 `value` 为普通对象。 包括继承的可枚举属性。

### 参数:

+ `value` (*) : 要转换的值。  

### 例子:

```js
function Foo() {
  this.b = 2;
}
 
Foo.prototype.c = 3;
 
_.assign({ 'a': 1 }, new Foo);
// => { 'a': 1, 'b': 2 }
 
_.assign({ 'a': 1 }, _.toPlainObject(new Foo));
// => { 'a': 1, 'b': 2, 'c': 3 }
```
