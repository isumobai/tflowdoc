## escape


```js
_.escape([string=''])
```

转义`string`中的 "&amp;", "&lt;", "&gt;", '"', "'", 和 "`" 字符为HTML实体字符。

**注意:** 不会转义其他字符。如果需要，可以使用第三方库，例如<em>he</em>。

 虽然 "&gt;" 是对称转义的，字符如 "&gt;" 和 "/" 没有特殊的意义，所以不需要在 HTML 转义。 除非它们是标签的一部分，或者是不带引号的属性值。 查看Mathias Bynens's article 的文章 <em>(under "semi-related fun fact")</em> 了解详情 。

 在 IE &lt; `9` 中转义引号，因为会中断属性值或 HTML 注释，查看HTML5 Security Cheatsheet 的#59,#102,#108, 和#133, 以及#133 了解详情。

 当解析 HTML 时，总应该在属性值上使用引号 以减少 XSS 的可能性。

### 参数:

+ `[string='']` (string) : 要转义的字符串。  

### 例子:

```js
_.escape('fred, barney, & pebbles');
// => 'fred, barney, & pebbles'
```
