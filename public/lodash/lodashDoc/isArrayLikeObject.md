## isArrayLikeObject


```js
_.isArrayLikeObject(value)
```

这个方法类似`_.isArrayLike`。除了它还检查`value`是否是个对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isArrayLikeObject([1, 2, 3]);
// => true
 
_.isArrayLikeObject(document.body.children);
// => true
 
_.isArrayLikeObject('abc');
// => false
 
_.isArrayLikeObject(_.noop);
// => false
```
