## transform


```js
_.transform(object, [iteratee=_.identity], [accumulator])
```

`_.reduce`的替代方法;此方法将转换`object`对象为一个新的`accumulator`对象，结果来自`iteratee`处理自身可枚举的属性。 每次调用可能会改变 `accumulator` 对象。如果不提供`accumulator`，将使用与`[[Prototype]]`相同的新对象。`iteratee`调用4个参数：<em>(accumulator, value, key, object)</em>。如果返回 `false`，`iteratee` 会提前退出。

### 参数:

+ `object` (Object) : 要遍历的对象  

+ `[iteratee=_.identity]` (Function) : 每次迭代时调用的函数。  

+ `[accumulator]` (*) : 定制叠加的值。  

### 例子:

```js
_.transform([2, 3, 4], function(result, n) {
  result.push(n *= n);
  return n % 2 == 0;
}, []);
// => [4, 9]
 
_.transform({ 'a': 1, 'b': 2, 'c': 1 }, function(result, value, key) {
  (result[value] || (result[value] = [])).push(key);
}, {});
// => { '1': ['a', 'c'], '2': ['b'] }
```
