## nthArg


```js
_.nthArg([n=0])
```

创建一个函数，这个函数返回第 `n` 个参数。如果 `n`为负数，则返回从结尾开始的第n个参数。

### 参数:

+ `[n=0]` (number) : 要返回参数的索引值。  

### 例子:

```js
var func = _.nthArg(1);
func('a', 'b', 'c', 'd');
// => 'b'
 
var func = _.nthArg(-2);
func('a', 'b', 'c', 'd');
// => 'c'
```
