## trim


```js
_.trim([string=''], [chars=whitespace])
```

从`string`字符串中移除前面和后面的 空格 或 指定的字符。

### 参数:

+ `[string='']` (string) : 要处理的字符串。  

+ `[chars=whitespace]` (string) : 要移除的字符。  

### 例子:

```js
_.trim('  abc  ');
// => 'abc'
 
_.trim('-_-abc-_-', '_-');
// => 'abc'
 
_.map(['  foo  ', '  bar  '], _.trim);
// => ['foo', 'bar']
```
