## isArguments


```js
_.isArguments(value)
```

检查 `value` 是否是一个类 `arguments` 对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isArguments(function() { return arguments; }());
// => true
 
_.isArguments([1, 2, 3]);
// => false
```
