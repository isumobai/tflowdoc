## gte


```js
_.gte(value, other)
```

检查 `value`是否大于或者等于 `other`。

### 参数:

+ `value` (*) : 要比较的值。  

+ `other` (*) : 另一个要比较的值。  

### 例子:

```js
_.gte(3, 1);
// => true
 
_.gte(3, 3);
// => true
 
_.gte(1, 3);
// => false
```
