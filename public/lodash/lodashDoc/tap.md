## tap


```js
_.tap(value, interceptor)
```

这个方法调用一个 `interceptor` 并返回 `value`。`interceptor`调用1个参数： <em>(value)</em>。 该方法的目的是 进入 方法链序列以便修改中间结果。

### 参数:

+ `value` (*) : 提供给 `interceptor` 的值。  

+ `interceptor` (Function) : 用来调用的函数。  

### 例子:

```js
_([1, 2, 3])
 .tap(function(array) {
// 改变传入的数组
   array.pop();
 })
 .reverse()
 .value();
// => [2, 1]
```
