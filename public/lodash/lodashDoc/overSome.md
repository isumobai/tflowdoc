## overSome


```js
_.overSome([predicates=[_.identity]])
```

创建一个函数，传入提供的参数的函数并调用 `predicates` 判断是否 **存在** 有真值。

### 参数:

+ `[predicates=[_.identity]]` (...(Function|Function[])) : 要调用的 predicates。  

### 例子:

```js
var func = _.overSome([Boolean, isFinite]);
 
func('1');
// => true
 
func(null);
// => true
 
func(NaN);
// => false
```
