## lte


```js
_.lte(value, other)
```

检查 `value` 是否小于等于 `other`。

### 参数:

+ `value` (*) : 用来比较的值。  

+ `other` (*) : 另一个用来比较的值。  

### 例子:

```js
_.lte(1, 3);
// => true
 
_.lte(3, 3);
// => true
 
_.lte(3, 1);
// => false
```
