## mapKeys


```js
_.mapKeys(object, [iteratee=_.identity])
```

反向版`_.mapValues`。 这个方法创建一个对象，对象的值与`object`相同，并且 key 是通过 `iteratee` 运行 `object` 中每个自身可枚举属性名字符串 产生的。`iteratee`调用三个参数： <em>(value, key, object)</em>。

### 参数:

+ `object` (Object) : 要遍历的对象。  

+ `[iteratee=_.identity]` (Function) : 每次迭代时调用的函数。  

### 例子:

```js
_.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
  return key + value;
});
// => { 'a1': 1, 'b2': 2 }
```
