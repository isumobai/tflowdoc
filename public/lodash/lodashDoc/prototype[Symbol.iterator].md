## prototype[Symbol.iterator]


```js
_.prototype[Symbol.iterator]
```

启用包装对象为 iterable。

### 例子:

```js
var wrapped = _([1, 2]);
 
wrapped[Symbol.iterator]() === wrapped;
// => true
 
Array.from(wrapped);
// => [1, 2]
```
