## sampleSize


```js
_.sampleSize(collection, [n=1])
```

从`collection`（集合）中获得 `n` 个随机元素。

### 参数:

+ `collection` (Array|Object) : 要取样的集合。  

+ `[n=1]` (number) : 取样的元素个数。  

### 例子:

```js
_.sampleSize([1, 2, 3], 2);
// => [3, 1]
 
_.sampleSize([1, 2, 3], 4);
// => [2, 3, 1]
```
