## over


```js
_.over([iteratees=[_.identity]])
```

创建一个函数，传入提供的参数的函数并调用 `iteratees` 返回结果。

### 参数:

+ `[iteratees=[_.identity]]` (...(Function|Function[])) : 要调用的 iteratees。  

### 例子:

```js
var func = _.over([Math.max, Math.min]);
 
func(1, 2, 3, 4);
// => [4, 1]
```
