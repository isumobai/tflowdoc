## unary


```js
_.unary(func)
```

创建一个最多接受一个参数的函数，忽略多余的参数。

### 参数:

+ `func` (Function) : 要处理的函数。  

### 例子:

```js
_.map(['6', '8', '10'], _.unary(parseInt));
// => [6, 8, 10]
```
