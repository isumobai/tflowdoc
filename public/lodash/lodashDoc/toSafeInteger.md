## toSafeInteger


```js
_.toSafeInteger(value)
```

转换 `value` 为安全整数。 安全整数可以用于比较和准确的表示。

### 参数:

+ `value` (*) : 要转换的值。  

### 例子:

```js
_.toSafeInteger(3.2);
// => 3
 
_.toSafeInteger(Number.MIN_VALUE);
// => 0
 
_.toSafeInteger(Infinity);
// => 9007199254740991
 
_.toSafeInteger('3.2');
// => 3
```
