## pickBy


```js
_.pickBy(object, [predicate=_.identity])
```

创建一个对象，这个对象组成为从 `object` 中经 `predicate` 判断为真值的属性。 `predicate`调用2个参数：<em>(value, key)</em>。

### 参数:

+ `object` (Object) : 来源对象。  

+ `[predicate=_.identity]` (Function) : 调用每一个属性的函数。  

### 例子:

```js
var object = { 'a': 1, 'b': '2', 'c': 3 };
 
_.pickBy(object, _.isNumber);
// => { 'a': 1, 'c': 3 }
```
