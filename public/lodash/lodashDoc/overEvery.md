## overEvery


```js
_.overEvery([predicates=[_.identity]])
```

建一个函数，传入提供的参数的函数并调用 `predicates` 判断是否 **全部** 都为真值。

### 参数:

+ `[predicates=[_.identity]]` (...(Function|Function[])) : 要调用的 predicates。  

### 例子:

```js
var func = _.overEvery([Boolean, isFinite]);
 
func('1');
// => true
 
func(null);
// => false
 
func(NaN);
// => false
```
