## some


```js
_.some(collection, [predicate=_.identity])
```

通过 `predicate`（断言函数） 检查`collection`（集合）中的元素是否存在 **任意** truthy（真值）的元素，一旦 `predicate`（断言函数） 返回 truthy（真值），遍历就停止。 predicate 调用3个参数：<em>(value, index|key, collection)</em>。

### 参数:

+ `collection` (Array|Object) : 用来迭代的集合。  

+ `[predicate=_.identity]` (Array|Function|Object|string) : 每次迭代调用的函数。  

### 例子:

```js
_.some([null, 0, 'yes', false], Boolean);
// => true
 
var users = [
  { 'user': 'barney', 'active': true },
  { 'user': 'fred',   'active': false }
];
 
// The `_.matches` iteratee shorthand.
_.some(users, { 'user': 'barney', 'active': false });
// => false
 
// The `_.matchesProperty` iteratee shorthand.
_.some(users, ['active', false]);
// => true
 
// The `_.property` iteratee shorthand.
_.some(users, 'active');
// => true
```
