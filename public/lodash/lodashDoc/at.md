## at


```js
_.at(object, [paths])
```

创建一个数组，值来自 `object` 的`paths`路径相应的值。

### 参数:

+ `object` (Object) : 要迭代的对象。  

+ `[paths]` (...(string|string[])) : 要获取的对象的元素路径，单独指定或者指定在数组中。  

### 例子:

```js
var object = { 'a': [{ 'b': { 'c': 3 } }, 4] };
 
_.at(object, ['a[0].b.c', 'a[1]']);
// => [3, 4]
```
