## noop


```js
_.noop()
```

这个方法返回 `undefined`。

### 例子:

```js
_.times(2, _.noop);
// => [undefined, undefined]
```
