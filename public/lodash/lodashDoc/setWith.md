## setWith


```js
_.setWith(object, path, value, [customizer])
```

这个方法类似`_.set`，除了它接受一个 `customizer`，调用生成对象的 `path`。 如果 `customizer` 返回 `undefined` 将会有它的处理方法代替。 `customizer` 调用3个参数： <em>(nsValue, key, nsObject)</em>。

**注意:** 这个方法会改变 `object`.

### 参数:

+ `object` (Object) : 要修改的对象。  

+ `path` (Array|string) : 要设置的对象路径。  

+ `value` (*) : 要设置的值。  

+ `[customizer]` (Function) : 这个函数用来定制分配的值。  

### 例子:

```js
var object = {};
 
_.setWith(object, '[0][1]', 'a', Object);
// => { '0': { '1': 'a' } }
```
