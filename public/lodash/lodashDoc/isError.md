## isError


```js
_.isError(value)
```

检查 `value` 是否是 `Error`, `EvalError`, `RangeError`, `ReferenceError`,`SyntaxError`, `TypeError`, 或者 `URIError`对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isError(new Error);
// => true
 
_.isError(Error);
// => false
```
