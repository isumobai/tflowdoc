## pick


```js
_.pick(object, [props])
```

创建一个从 `object` 中选中的属性的对象。

### 参数:

+ `object` (Object) : 来源对象。  

+ `[props]` (...(string|string[])) : 要被忽略的属性。（注：单独指定或指定在数组中。）  

### 例子:

```js
var object = { 'a': 1, 'b': '2', 'c': 3 };
 
_.pick(object, ['a', 'c']);
// => { 'a': 1, 'c': 3 }
```
