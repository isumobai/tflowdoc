## isWeakSet


```js
_.isWeakSet(value)
```

检查 `value` 是否是 `WeakSet` 对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isWeakSet(new WeakSet);
// => true
 
_.isWeakSet(new Set);
// => false
```
