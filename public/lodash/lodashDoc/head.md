## head


```js
_.head(array)
```

获取数组 `array` 的第一个元素。

### 引入版本:

0.1.0

### 参数:

+ `array` (Array) : 要查询的数组。  

### 返回值:

*(*)*: 返回数组 `array`的第一个元素。

### 例子:

```js
_.head([1, 2, 3]);
// => 1
 
_.head([]);
// => undefined
```
