## toString


```js
_.toString(value)
```

转换 `value` 为字符串。 `null` 和 `undefined` 将返回空字符串。`-0` 将被转换为字符串`"-0"`。

### 参数:

+ `value` (*) : 要处理的值。  

### 例子:

```js
_.toString(null);
// => ''
 
_.toString(-0);
// => '-0'
 
_.toString([1, 2, 3]);
// => '1,2,3'
```
