## property


```js
_.property(path)
```

创建一个返回给定对象的 `path` 的值的函数。

### 参数:

+ `path` (Array|string) : 要得到值的属性路径。  

### 例子:

```js
var objects = [
  { 'a': { 'b': 2 } },
  { 'a': { 'b': 1 } }
];
 
_.map(objects, _.property('a.b'));
// => [2, 1]
 
_.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
// => [1, 2]
```
