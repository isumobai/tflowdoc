## padEnd


```js
_.padEnd([string=''], [length=0], [chars=' '])
```

如果`string`字符串长度小于 `length` 则在右侧填充字符。 如果超出`length`长度则截断超出的部分。

### 参数:

+ `[string='']` (string) : 要填充的字符串。  

+ `[length=0]` (number) : 填充的长度。  

+ `[chars=' ']` (string) : 填充字符。  

### 例子:

```js
_.padEnd('abc', 6);
// => 'abc   '
 
_.padEnd('abc', 6, '_-');
// => 'abc_-_'
 
_.padEnd('abc', 3);
// => 'abc'
```
