## isMap


```js
_.isMap(value)
```

检查 `value` 是否为一个 `Map` 对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isMap(new Map);
// => true
 
_.isMap(new WeakMap);
// => false
```
