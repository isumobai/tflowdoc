## lt


```js
_.lt(value, other)
```

检查 `value` 是否小于 `other`。

### 参数:

+ `value` (*) : 用来比较的值。  

+ `other` (*) : 另一个用来比较的值。  

### 例子:

```js
_.lt(1, 3);
// => true
 
_.lt(3, 3);
// => false
 
_.lt(3, 1);
// => false
```
