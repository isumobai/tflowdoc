## isNumber


```js
_.isNumber(value)
```

检查 `value` 是否是原始`Number`数值型 或者 对象。

**注意:** 要排除 `Infinity`, `-Infinity`, 以及 `NaN` 数值类型，用`_.isFinite` 方法。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isNumber(3);
// => true
 
_.isNumber(Number.MIN_VALUE);
// => true
 
_.isNumber(Infinity);
// => true
 
_.isNumber('3');
// => false
```
