## words


```js
_.words([string=''], [pattern])
```

拆分字符串`string`中的词为数组 。

### 参数:

+ `[string='']` (string) : 要拆分的字符串。  

+ `[pattern]` (RegExp|string) : 匹配模式。  

### 例子:

```js
_.words('fred, barney, & pebbles');
// => ['fred', 'barney', 'pebbles']
 
_.words('fred, barney, & pebbles', /[^, ]+/g);
// => ['fred', 'barney', '&', 'pebbles']
```
