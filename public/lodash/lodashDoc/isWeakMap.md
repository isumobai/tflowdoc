## isWeakMap


```js
_.isWeakMap(value)
```

检查 `value` 是否是 `WeakMap` 对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isWeakMap(new WeakMap);
// => true
 
_.isWeakMap(new Map);
// => false
```
