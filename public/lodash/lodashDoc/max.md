## max


```js
_.max(array)
```

计算 `array` 中的最大值。 如果 `array` 是 空的或者假值将会返回 `undefined`。

### 参数:

+ `array` (Array) : 要迭代的数组。  

### 例子:

```js
_.max([4, 2, 8, 6]);
// => 8
 
_.max([]);
// => undefined
```
