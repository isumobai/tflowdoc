## ary


```js
_.ary(func, [n=func.length])
```

创建一个调用`func`的函数。调用`func`时最多接受 `n`个参数，忽略多出的参数。

### 参数:

+ `func` (Function) : 需要被限制参数个数的函数。  

+ `[n=func.length]` (number) : 限制的参数数量。  

### 例子:

```js
_.map(['6', '8', '10'], _.ary(parseInt, 1));
// => [6, 8, 10]
```
