## defaultsDeep


```js
_.defaultsDeep(object, [sources])
```

这个方法类似`_.defaults`，除了它会递归分配默认属性。

**注意:** 这方法会改变 `object`.

### 参数:

+ `object` (Object) : 目标对象。  

+ `[sources]` (...Object) : 来源对象。  

### 例子:

```js
_.defaultsDeep({ 'a': { 'b': 2 } }, { 'a': { 'b': 1, 'c': 3 } });
// => { 'a': { 'b': 2, 'c': 3 } }
```
