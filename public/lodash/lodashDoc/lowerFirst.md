## lowerFirst


```js
_.lowerFirst([string=''])
```

转换字符串`string`的首字母为小写。

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.lowerFirst('Fred');
// => 'fred'
 
_.lowerFirst('FRED');
// => 'fRED'
```
