## mergeWith


```js
_.mergeWith(object, sources, customizer)
```

该方法类似`_.merge`，除了它接受一个 `customizer`，调用以产生目标对象和来源对象属性的合并值。如果`customizer` 返回 `undefined`，将会由合并处理方法代替。`customizer`调用与7个参数：<em>(objValue, srcValue, key, object, source, stack)</em>。

**Note:** 这方法会改变对象 `object`.

### 参数:

+ `object` (Object) : 目标对象。  

+ `[sources]` (...Object) : 来源对象。  

+ `customizer` (Function) : 这个函数定制合并值。  

### 例子:

```js
function customizer(objValue, srcValue) {
  if (_.isArray(objValue)) {
    return objValue.concat(srcValue);
  }
}
 
var object = { 'a': [1], 'b': [2] };
var other = { 'a': [3], 'b': [4] };
 
_.mergeWith(object, other, customizer);
// => { 'a': [1, 3], 'b': [2, 4] }
```
