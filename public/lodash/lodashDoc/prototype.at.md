## prototype.at


```js
_.prototype.at([paths])
```

这个方法是`_.at` 的包装版本 。

### 参数:

+ `[paths]` (...(string|string[])) : 要选择元素的属性路径（注： 单独指定或者数组）。  

### 例子:

```js
var object = { 'a': [{ 'b': { 'c': 3 } }, 4] };
 
_(object).at(['a[0].b.c', 'a[1]']).value();
// => [3, 4]
```
