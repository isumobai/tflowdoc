## isObject


```js
_.isObject(value)
```

检查 `value` 是否为 `Object` 的language type。 <em>(例如： arrays, functions, objects, regexes,`new Number(0)`, 以及 `new String('')`)</em>

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isObject({});
// => true
 
_.isObject([1, 2, 3]);
// => true
 
_.isObject(_.noop);
// => true
 
_.isObject(null);
// => false
```
