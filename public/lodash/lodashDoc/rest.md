## rest


```js
_.rest(func, [start=func.length-1])
```

创建一个函数，调用`func`时，`this`绑定到创建的新函数，并且`start`之后的参数作为数组传入。

**Note:** 这个方法基于rest parameter。

### 参数:

+ `func` (Function) : 要应用的函数。  

+ `[start=func.length-1]` (number) : rest 参数的开始位置。  

### 例子:

```js
var say = _.rest(function(what, names) {
  return what + ' ' + _.initial(names).join(', ') +
    (_.size(names) > 1 ? ', & ' : '') + _.last(names);
});
 
say('hello', 'fred', 'barney', 'pebbles');
// => 'hello fred, barney, & pebbles'
```
