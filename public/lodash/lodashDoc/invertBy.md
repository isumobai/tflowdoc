## invertBy


```js
_.invertBy(object, [iteratee=_.identity])
```

这个方法类似`_.invert`，除了倒置对象 是 `collection`（集合）中的每个元素经过 `iteratee`（迭代函数） 处理后返回的结果。每个反转键相应反转的值是一个负责生成反转值key的数组。`iteratee` 会传入3个参数：<em>(value)</em> 。

### 参数:

+ `object` (Object) : 要键值倒置对象。  

+ `[iteratee=_.identity]` (Function) : 每次迭代时调用的函数。  

### 例子:

```js
var object = { 'a': 1, 'b': 2, 'c': 1 };
 
_.invertBy(object);
// => { '1': ['a', 'c'], '2': ['b'] }
 
_.invertBy(object, function(value) {
  return 'group' + value;
});
// => { 'group1': ['a', 'c'], 'group2': ['b'] }
```
