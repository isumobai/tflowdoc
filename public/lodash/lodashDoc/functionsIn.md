## functionsIn


```js
_.functionsIn(object)
```

创建一个函数属性名称的数组，函数属性名称来自`object`对象自身和继承的可枚举属性。

### 参数:

+ `object` (Object) : 要检查的对象。  

### 例子:

```js
function Foo() {
  this.a = _.constant('a');
  this.b = _.constant('b');
}
 
Foo.prototype.c = _.constant('c');
 
_.functionsIn(new Foo);
// => ['a', 'b', 'c']
```
