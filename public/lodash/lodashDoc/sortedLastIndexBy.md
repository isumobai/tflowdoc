## sortedLastIndexBy


```js
_.sortedLastIndexBy(array, value, [iteratee=_.identity])
```

这个方法类似`_.sortedLastIndex` ，除了它接受一个 `iteratee` （迭代函数），调用每一个数组（`array`）元素，返回结果和`value` 值比较来计算排序。iteratee 会传入一个参数：<em>(value)</em>。

### 参数:

+ `array` (Array) : 要检查的排序数组。  

+ `value` (*) : 要评估的值。  

+ `[iteratee=_.identity]` (Array|Function|Object|string) : 迭代函数，调用每个元素。  

### 例子:

```js
var objects = [{ 'x': 4 }, { 'x': 5 }];
 
_.sortedLastIndexBy(objects, { 'x': 4 }, function(o) { return o.x; });
// => 1
 
// The `_.property` iteratee shorthand.
_.sortedLastIndexBy(objects, { 'x': 4 }, 'x');
// => 1
```
