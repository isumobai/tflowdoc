## without


```js
_.without(array, [values])
```

创建一个剔除所有给定值的新数组，剔除值的时候，使用`SameValueZero`做相等比较。

**注意:** 不像`_.pull`, 这个方法会返回一个新数组。

### 参数:

+ `array` (Array) : 要检查的数组。  

+ `[values]` (...*) : 要剔除的值。  

### 例子:

```js
_.without([2, 1, 2, 3], 1, 2);
// => [3]
```
