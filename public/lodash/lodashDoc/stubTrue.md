## stubTrue


```js
_.stubTrue()
```

这个方法返回 `true`。

### 例子:

```js
_.times(2, _.stubTrue);
// => [true, true]
```
