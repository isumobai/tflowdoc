## kebabCase


```js
_.kebabCase([string=''])
```

转换字符串`string`为kebab case.

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.kebabCase('Foo Bar');
// => 'foo-bar'
 
_.kebabCase('fooBar');
// => 'foo-bar'
 
_.kebabCase('__FOO_BAR__');
// => 'foo-bar'
```
