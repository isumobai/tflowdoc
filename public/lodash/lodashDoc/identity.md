## identity


```js
_.identity(value)
```

这个方法返回首个提供的参数。

### 参数:

+ `value` (*) : 任何值。  

### 例子:

```js
var object = { 'a': 1 };
 
console.log(_.identity(object) === object);
// => true
```
