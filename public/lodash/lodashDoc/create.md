## create


```js
_.create(prototype, [properties])
```

创建一个继承 `prototype` 的对象。 如果提供了 `prototype`，它的可枚举属性会被分配到创建的对象上。

### 参数:

+ `prototype` (Object) : 要继承的对象。  

+ `[properties]` (Object) : 待分配的属性。  

### 例子:

```js
function Shape() {
  this.x = 0;
  this.y = 0;
}
 
function Circle() {
  Shape.call(this);
}
 
Circle.prototype = _.create(Shape.prototype, {
  'constructor': Circle
});
 
var circle = new Circle;
circle instanceof Circle;
// => true
 
circle instanceof Shape;
// => true
```
