## isSafeInteger


```js
_.isSafeInteger(value)
```

检查 `value` 是否是一个安全整数。 一个安全整数应该是符合 IEEE-754 标准的非双精度浮点数。

**注意:** 这个方法基于`Number.isSafeInteger`.

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isSafeInteger(3);
// => true
 
_.isSafeInteger(Number.MIN_VALUE);
// => false
 
_.isSafeInteger(Infinity);
// => false
 
_.isSafeInteger('3');
// => false
```
