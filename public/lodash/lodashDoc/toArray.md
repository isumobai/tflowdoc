## toArray


```js
_.toArray(value)
```

转换 `value` 为一个数组。

### 参数:

+ `value` (*) : 要转换的值。  

### 例子:

```js
_.toArray({ 'a': 1, 'b': 2 });
// => [1, 2]
 
_.toArray('abc');
// => ['a', 'b', 'c']
 
_.toArray(1);
// => []
 
_.toArray(null);
// => []
```
