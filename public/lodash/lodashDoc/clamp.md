## clamp


```js
_.clamp(number, [lower], upper)
```

返回限制在 `lower` 和 `upper` 之间的值。

### 参数:

+ `number` (number) : 被限制的值。  

+ `[lower]` (number) : 下限。  

+ `upper` (number) : 上限。  

### 例子:

```js
_.clamp(-10, -5, 5);
// => -5
 
_.clamp(10, -5, 5);
// => 5
```
