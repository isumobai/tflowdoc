## sample


```js
_.sample(collection)
```

从`collection`（集合）中获得一个随机元素。

### 参数:

+ `collection` (Array|Object) : 要取样的集合。  

### 例子:

```js
_.sample([1, 2, 3, 4]);
// => 2
```
