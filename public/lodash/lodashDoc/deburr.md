## deburr


```js
_.deburr([string=''])
```

转换字符串`string`中拉丁语-1补充字母 和拉丁语扩展字母-A 为基本的拉丁字母，并且去除组合变音标记。

### 参数:

+ `[string='']` (string) : 要处理的字符串。  

### 例子:

```js
_.deburr('déjà vu');
// => 'deja vu'
```
