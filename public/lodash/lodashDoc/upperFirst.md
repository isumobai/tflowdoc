## upperFirst


```js
_.upperFirst([string=''])
```

转换字符串`string`的首字母为大写。

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.upperFirst('fred');
// => 'Fred'
 
_.upperFirst('FRED');
// => 'FRED'
```
