## sum


```js
_.sum(array)
```

计算 `array` 中值的总和

### 参数:

+ `array` (Array) : 要迭代的数组。  

### 例子:

```js
_.sum([4, 2, 8, 6]);
// => 20
```
