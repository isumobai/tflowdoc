## mean


```js
_.mean(array)
```

计算 `array` 的平均值。

### 参数:

+ `array` (Array) : 要迭代的数组。  

### 例子:

```js
_.mean([4, 2, 8, 6]);
// => 5
```
