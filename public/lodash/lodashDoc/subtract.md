## subtract


```js
_.subtract(minuend, subtrahend)
```

亮数相减。

### 参数:

+ `minuend` (number) : 相减的第一个数。  

+ `subtrahend` (number) : 相减的第二个数。  

### 例子:

```js
_.subtract(6, 4);
// => 2
```
