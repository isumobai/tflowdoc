## hasIn


```js
_.hasIn(object, path)
```

检查 `path` 是否是`object`对象的直接或继承属性。

### 参数:

+ `object` (Object) : 要检索的对象。  

+ `path` (Array|string) : 要检查的路径`path`。  

### 例子:

```js
var object = _.create({ 'a': _.create({ 'b': 2 }) });
 
_.hasIn(object, 'a');
// => true
 
_.hasIn(object, 'a.b');
// => true
 
_.hasIn(object, ['a', 'b']);
// => true
 
_.hasIn(object, 'b');
// => false
```
