## unset


```js
_.unset(object, path)
```

移除`object`对象 `path` 路径上的属性。

**注意:** 这个方法会改变源对象 `object`。

### 参数:

+ `object` (Object) : 要修改的对象。  

+ `path` (Array|string) : 要移除的对象路径。  

### 例子:

```js
var object = { 'a': [{ 'b': { 'c': 7 } }] };
_.unset(object, 'a[0].b.c');
// => true
 
console.log(object);
// => { 'a': [{ 'b': {} }] };
 
_.unset(object, ['a', '0', 'b', 'c']);
// => true
 
console.log(object);
// => { 'a': [{ 'b': {} }] };
```
