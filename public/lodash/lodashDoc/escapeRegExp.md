## escapeRegExp


```js
_.escapeRegExp([string=''])
```

转义 `RegExp` 字符串中特殊的字符 "^", "$", "", ".", "*", "+", "?", "(", ")", "[", "]", ", ", 和 "|" in .

### 参数:

+ `[string='']` (string) : 要转义的字符串。  

### 例子:

```js
_.escapeRegExp('[lodash](https://lodash.com/)');
// => '\[lodash\]\(https://lodash\.com/\)'
```
