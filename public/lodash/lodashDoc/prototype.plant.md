## prototype.plant


```js
_.prototype.plant(value)
```

创建一个链式队列的拷贝，传入的 `value` 作为链式队列的值。

### 参数:

+ `value` (*) : 替换原值的值。  

### 例子:

```js
function square(n) {
  return n * n;
}
 
var wrapped = _([1, 2]).map(square);
var other = wrapped.plant([3, 4]);
 
other.value();
// => [9, 16]
 
wrapped.value();
// => [1, 4]
```
