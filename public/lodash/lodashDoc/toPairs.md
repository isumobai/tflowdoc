## toPairs


```js
_.toPairs(object)
```

创建一个`object`对象自身可枚举属性的键值对数组。这个数组可以通过`_.fromPairs`撤回。如果`object` 是 map 或 set，返回其条目。

### 参数:

+ `object` (Object) : 要检索的对象。  

### 例子:

```js
function Foo() {
  this.a = 1;
  this.b = 2;
}
 
Foo.prototype.c = 3;
 
_.toPairs(new Foo);
// => [['a', 1], ['b', 2]] (iteration order is not guaranteed)
```
