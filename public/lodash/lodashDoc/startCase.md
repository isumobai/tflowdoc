## startCase


```js
_.startCase([string=''])
```

转换 `string` 字符串为start case.

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.startCase('--foo-bar--');
// => 'Foo Bar'
 
_.startCase('fooBar');
// => 'Foo Bar'
 
_.startCase('__FOO_BAR__');
// => 'FOO BAR'
```
