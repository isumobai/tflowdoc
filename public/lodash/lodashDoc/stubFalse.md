## stubFalse


```js
_.stubFalse()
```

这个方法返回 `false`.

### 例子:

```js
_.times(2, _.stubFalse);
// => [false, false]
```
