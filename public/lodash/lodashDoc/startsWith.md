## startsWith


```js
_.startsWith([string=''], [target], [position=0])
```

检查字符串`string`是否以 `target` 开头。

### 参数:

+ `[string='']` (string) : 要检索的字符串。  

+ `[target]` (string) : 要检查的字符串。  

+ `[position=0]` (number) : 检索的位置。  

### 例子:

```js
_.startsWith('abc', 'a');
// => true
 
_.startsWith('abc', 'b');
// => false
 
_.startsWith('abc', 'b', 1);
// => true
```
