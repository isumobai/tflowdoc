## endsWith


```js
_.endsWith([string=''], [target], [position=string.length])
```

检查字符串`string`是否以给定的`target`字符串结尾。

### 参数:

+ `[string='']` (string) : 要检索的字符串。  

+ `[target]` (string) : 要检索字符。  

+ `[position=string.length]` (number) : 检索的位置。  

### 例子:

```js
_.endsWith('abc', 'c');
// => true
 
_.endsWith('abc', 'b');
// => false
 
_.endsWith('abc', 'b', 2);
// => true
```
