## isLength


```js
_.isLength(value)
```

检查 `value` 是否为有效的类数组长度。

**注意:** 这个函数基于`ToLength`.

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isLength(3);
// => true
 
_.isLength(Number.MIN_VALUE);
// => false
 
_.isLength(Infinity);
// => false
 
_.isLength('3');
// => false
```
