## conformsTo


```js
_.conformsTo(object, source)
```

通过调用断言`source`的属性与 `object` 的相应属性值，检查 `object`是否符合 `source`。当`source`偏应用时，这种方法和`_.conforms`函数是等价的。

**注意:** 当`source`为偏应用时，这种方法等价于`_.conforms`。（注：关于偏应用大家可以自己到google上搜索一下）。

### 参数:

+ `object` (Object) : 要检查的对象。  

+ `source` (Object) : 要断言属性是否符合的对象。  

### 例子:

```js
var object = { 'a': 1, 'b': 2 };
 
_.conformsTo(object, { 'b': function(n) { return n > 1; } });
// => true
 
_.conformsTo(object, { 'b': function(n) { return n > 2; } });
// => false
```
