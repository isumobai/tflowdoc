## isTypedArray


```js
_.isTypedArray(value)
```

检查 `value` 是否是TypedArray。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isTypedArray(new Uint8Array);
// => true
 
_.isTypedArray([]);
// => false
```
