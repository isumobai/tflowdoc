## prototype.commit


```js
_.prototype.commit()
```

执行链式队列并返回结果。

### 例子:

```js
var array = [1, 2];
var wrapped = _(array).push(3);
 
console.log(array);
// => [1, 2]
 
wrapped = wrapped.commit();
console.log(array);
// => [1, 2, 3]
 
wrapped.last();
// => 3
 
console.log(array);
// => [1, 2, 3]
```
