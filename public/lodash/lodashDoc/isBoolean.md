## isBoolean


```js
_.isBoolean(value)
```

检查 `value` 是否是原始 boolean 类型或者对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isBoolean(false);
// => true
 
_.isBoolean(null);
// => false
```
