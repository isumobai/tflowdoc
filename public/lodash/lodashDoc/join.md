## join


```js
_.join(array, [separator=','])
```

将 `array` 中的所有元素转换为由 `separator` 分隔的字符串。

### 引入版本:

4.0.0

### 参数:

+ `array` (Array) : 要转换的数组。  

+ `[separator=',']` (string) : 分隔元素。  

### 返回值:

*(string)*: 返回连接字符串。

### 例子:

```js
_.join(['a', 'b', 'c'], '~');
// => 'a~b~c'
```
