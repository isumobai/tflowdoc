## defaults


```js
_.defaults(object, [sources])
```

分配来源对象的可枚举属性到目标对象所有解析为 `undefined` 的属性上。 来源对象从左到右应用。 一旦设置了相同属性的值，后续的将被忽略掉。

**注意:** 这方法会改变 `object`.

### 参数:

+ `object` (Object) : 目标对象。  

+ `[sources]` (...Object) : 来源对象。  

### 例子:

```js
_.defaults({ 'a': 1 }, { 'b': 2 }, { 'a': 3 });
// => { 'a': 1, 'b': 2 }
```
