## cloneDeepWith


```js
_.cloneDeepWith(value, [customizer])
```

这个方法类似`_.cloneWith`，除了它会递归克隆 `value`。

### 参数:

+ `value` (*) : 用来递归克隆的值。  

+ `[customizer]` (Function) : 用来自定义克隆的函数。  

### 例子:

```js
function customizer(value) {
  if (_.isElement(value)) {
    return value.cloneNode(true);
  }
}
 
var el = _.cloneDeepWith(document.body, customizer);
 
console.log(el === document.body);
// => false
console.log(el.nodeName);
// => 'BODY'
console.log(el.childNodes.length);
// => 20
```
