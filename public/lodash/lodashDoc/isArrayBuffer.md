## isArrayBuffer


```js
_.isArrayBuffer(value)
```

检查 `value` 是否是 `ArrayBuffer` 对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isArrayBuffer(new ArrayBuffer(2));
// => true
 
_.isArrayBuffer(new Array(2));
// => false
```
