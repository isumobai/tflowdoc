## isRegExp


```js
_.isRegExp(value)
```

检查 `value` 是否为`RegExp`对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isRegExp(/abc/);
// => true
 
_.isRegExp('/abc/');
// => false
```
