## toInteger


```js
_.toInteger(value)
```

转换 `value` 为一个整数。

**注意:** 这个方法基于`ToInteger`.

### 参数:

+ `value` (*) : 要转换的值。  

### 例子:

```js
_.toInteger(3.2);
// => 3
 
_.toInteger(Number.MIN_VALUE);
// => 0
 
_.toInteger(Infinity);
// => 1.7976931348623157e+308
 
_.toInteger('3.2');
// => 3
```
