## mixin


```js
_.mixin([object=lodash], source, [options=])
```

添加来源对象自身的所有可枚举函数属性到目标对象。 如果 `object` 是个函数，那么函数方法将被添加到原型链上。

**注意:** 使用`_.runInContext` 来创建原始的 `lodash` 函数来避免修改造成的冲突。

### 参数:

+ `[object=lodash]` (Function|Object) : 目标对象。  

+ `source` (Object) : 来源对象。  

+ [options=] (Object) : 选项对象。  

+ `[options.chain=true]` (boolean) : 是否开启链式操作。  

### 例子:

```js
function vowels(string) {
  return _.filter(string, function(v) {
    return /[aeiou]/i.test(v);
  });
}
 
_.mixin({ 'vowels': vowels });
_.vowels('fred');
// => ['e']
 
_('fred').vowels().value();
// => ['e']
 
_.mixin({ 'vowels': vowels }, { 'chain': false });
_('fred').vowels();
// => ['e']
```
