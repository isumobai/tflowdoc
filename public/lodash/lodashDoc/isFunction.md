## isFunction


```js
_.isFunction(value)
```

检查 `value` 是否是 `Function` 对象。

### 参数:

+ `value` (*) : 要检查的值  

### 例子:

```js
_.isFunction(_);
// => true
 
_.isFunction(/abc/);
// => false
```
