## replace


```js
_.replace([string=''], pattern, replacement)
```

替换`string`字符串中匹配的`pattern`为给定的`replacement` 。

**注意:** 这个方法基于`String#replace`.

### 参数:

+ `[string='']` (string) : 待替换的字符串。  

+ `pattern` (RegExp|string) : 要匹配的内容。  

+ `replacement` (Function|string) : 替换的内容。  

### 例子:

```js
_.replace('Hi Fred', 'Fred', 'Barney');
// => 'Hi Barney'
```
