## assignIn


```js
_.assignIn(object, [sources])
```

这个方法类似`_.assign`， 除了它会遍历并继承来源对象的属性。

**Note:** 这方法会改变 `object`。

### 参数:

+ `object` (Object) : 目标对象。  

+ `[sources]` (...Object) : 来源对象。  

### 例子:

```js
function Foo() {
  this.a = 1;
}
 
function Bar() {
  this.c = 3;
}
 
Foo.prototype.b = 2;
Bar.prototype.d = 4;
 
_.assignIn({ 'a': 0 }, new Foo, new Bar);
// => { 'a': 1, 'b': 2, 'c': 3, 'd': 4 }
```
