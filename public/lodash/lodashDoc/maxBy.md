## maxBy


```js
_.maxBy(array, [iteratee=_.identity])
```

这个方法类似`_.max` 除了它接受 `iteratee` 来调用 `array`中的每一个元素，来生成其值排序的标准。 iteratee 会调用1个参数: <em>(value)</em> 。

### 参数:

+ `array` (Array) : 要迭代的数组。  

+ `[iteratee=_.identity]` (Function) : 调用每个元素的迭代函数。  

### 例子:

```js
var objects = [{ 'n': 1 }, { 'n': 2 }];
 
_.maxBy(objects, function(o) { return o.n; });
// => { 'n': 2 }
 
// The `_.property` iteratee shorthand.
_.maxBy(objects, 'n');
// => { 'n': 2 }
```
