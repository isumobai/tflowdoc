## snakeCase


```js
_.snakeCase([string=''])
```

转换字符串`string`为snake case.

### 参数:

+ `[string='']` (string) : 要转换的字符串。  

### 例子:

```js
_.snakeCase('Foo Bar');
// => 'foo_bar'
 
_.snakeCase('fooBar');
// => 'foo_bar'
 
_.snakeCase('--FOO-BAR--');
// => 'foo_bar'
```
