## toNumber


```js
_.toNumber(value)
```

转换 `value` 为一个数字。

### 参数:

+ `value` (*) : 要处理的值。  

### 例子:

```js
_.toNumber(3.2);
// => 3.2
 
_.toNumber(Number.MIN_VALUE);
// => 5e-324
 
_.toNumber(Infinity);
// => Infinity
 
_.toNumber('3.2');
// => 3.2
```
