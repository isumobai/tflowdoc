## toPath


```js
_.toPath(value)
```

转化 `value` 为属性路径的数组 。

### 参数:

+ `value` (*) : 要转换的值  

### 例子:

```js
_.toPath('a.b.c');
// => ['a', 'b', 'c']
 
_.toPath('a[0].b.c');
// => ['a', '0', 'b', 'c']
```
