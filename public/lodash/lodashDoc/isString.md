## isString


```js
_.isString(value)
```

检查 `value` 是否是原始字符串`String`或者对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isString('abc');
// => true
 
_.isString(1);
// => false
```
