## prototype.value


```js
_.prototype.value()
```

执行链式队列并提取解链后的值。

### 例子:

```js
_([1, 2, 3]).value();
// => [1, 2, 3]
```
