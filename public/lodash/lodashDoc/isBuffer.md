## isBuffer


```js
_.isBuffer(value)
```

检查 `value` 是否是个 buffer。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isBuffer(new Buffer(2));
// => true
 
_.isBuffer(new Uint8Array(2));
// => false
```
