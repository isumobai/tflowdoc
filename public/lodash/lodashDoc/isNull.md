## isNull


```js
_.isNull(value)
```

检查 `value`alue 是否是 `null`。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isNull(null);
// => true
 
_.isNull(void 0);
// => false
```
