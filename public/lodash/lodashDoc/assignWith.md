## assignWith


```js
_.assignWith(object, sources, [customizer])
```

这个方法类似`_.assign` ， 除了它接受一个 `customizer` 决定如何分配值。 如果`customizer`返回 `undefined` 将会由分配处理方法代替。`customizer` 会传入5个参数： <em>(objValue, srcValue, key, object, source)</em>。

**Note:** 这方法会改变 `object`.

### 参数:

+ `object` (Object) : 目标对象。  

+ `sources` (...Object) : 来源对象。  

+ `[customizer]` (Function) : 这个函数用来自定义分配的值。  

### 例子:

```js
function customizer(objValue, srcValue) {
  return _.isUndefined(objValue) ? srcValue : objValue;
}
 
var defaults = _.partialRight(_.assignWith, customizer);
 
defaults({ 'a': 1 }, { 'b': 2 }, { 'a': 3 });
// => { 'a': 1, 'b': 2 }
```
