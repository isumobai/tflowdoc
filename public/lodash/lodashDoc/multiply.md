## multiply


```js
_.multiply(multiplier, multiplicand)
```

两个数相乘。

### 参数:

+ `augend` (number) : 相乘的第一个数。  

+ `addend` (number) : 相乘的第二个数。  

### 例子:

```js
_.multiply(6, 4);
// => 24
```
