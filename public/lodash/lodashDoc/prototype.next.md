## prototype.next


```js
_.prototype.next()
```

获得包装对象的下一个值，遵循iterator protocol。

### 例子:

```js
var wrapped = _([1, 2]);
 
wrapped.next();
// => { 'done': false, 'value': 1 }
 
wrapped.next();
// => { 'done': false, 'value': 2 }
 
wrapped.next();
// => { 'done': true, 'value': undefined }
```
