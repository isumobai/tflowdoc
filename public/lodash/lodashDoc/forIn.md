## forIn


```js
_.forIn(object, [iteratee=_.identity])
```

使用 `iteratee` 遍历对象的自身和继承的可枚举属性。 `iteratee` 会传入3个参数：<em>(value, key, object)</em>。 如果返回 `false`，`iteratee` 会提前退出遍历。

### 参数:

+ `object` (Object) : 要遍历的对象。  

+ `[iteratee=_.identity]` (Function) : 每次迭代时调用的函数。  

### 例子:

```js
function Foo() {
  this.a = 1;
  this.b = 2;
}
 
Foo.prototype.c = 3;
 
_.forIn(new Foo, function(value, key) {
  console.log(key);
});
// => Logs 'a', 'b', then 'c' (无法保证遍历的顺序)。
```
