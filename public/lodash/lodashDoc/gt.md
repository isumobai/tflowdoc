## gt


```js
_.gt(value, other)
```

检查 `value`是否大于 `other`。

### 参数:

+ `value` (*) : 要比较的值。  

+ `other` (*) : 另一个要比较的值。  

### 例子:

```js
_.gt(3, 1);
// => true
 
_.gt(3, 3);
// => false
 
_.gt(1, 3);
// => false
```
