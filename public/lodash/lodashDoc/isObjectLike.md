## isObjectLike


```js
_.isObjectLike(value)
```

检查 `value` 是否是 类对象。 如果一个值是类对象，那么它不应该是 `null`，而且 `typeof` 后的结果是 "object"。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isObjectLike({});
// => true
 
_.isObjectLike([1, 2, 3]);
// => true
 
_.isObjectLike(_.noop);
// => false
 
_.isObjectLike(null);
// => false
```
