## defaultTo


```js
_.defaultTo(value, defaultValue)
```

检查`value`，以确定一个默认值是否应被返回。如果`value`为`NaN`, `null`, 或者 `undefined`，那么返回`defaultValue`默认值。

### 参数:

+ `value` (*) : 要检查的值。  

+ `defaultValue` (*) : 默认值。  

### 例子:

```js
_.defaultTo(1, 10);
// => 1
 
_.defaultTo(undefined, 10);
// => 10
```
