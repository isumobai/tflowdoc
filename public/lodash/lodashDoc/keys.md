## keys


```js
_.keys(object)
```

创建一个 `object` 的自身可枚举属性名为数组。

**Note:** 非对象的值会被强制转换为对象，查看ES spec 了解详情。

### 参数:

+ `object` (Object) : 要检索的对象。  

### 例子:

```js
function Foo() {
  this.a = 1;
  this.b = 2;
}
 
Foo.prototype.c = 3;
 
_.keys(new Foo);
// => ['a', 'b'] (iteration order is not guaranteed)
 
_.keys('hi');
// => ['0', '1']
```
