## isDate


```js
_.isDate(value)
```

检查 `value` 是否是 `Date` 对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isDate(new Date); 
// => true
 
_.isDate('Mon April 23 2012');
// => false
```
