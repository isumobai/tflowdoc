## flatMap


```js
_.flatMap(collection, [iteratee=_.identity])
```

创建一个扁平化（注：同阶数组）的数组，这个数组的值来自`collection`（集合）中的每一个值经过 `iteratee`（迭代函数） 处理后返回的结果，并且扁平化合并。 iteratee 调用三个参数： <em>(value, index|key, collection)</em>。

### 参数:

+ `collection` (Array|Object) : 一个用来迭代遍历的集合。  

+ `[iteratee=_.identity]` (Array|Function|Object|string) : 每次迭代调用的函数。  

### 例子:

```js
function duplicate(n) {
  return [n, n];
}
 
_.flatMap([1, 2], duplicate);
// => [1, 1, 2, 2]
```
