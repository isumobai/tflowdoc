## prototype.reverse


```js
_.prototype.reverse()
```

这个方法是`_.reverse` 的包装版本 。

**注意:** 这种方法会改变包装数组。

### 例子:

```js
var array = [1, 2, 3];
 
_(array).reverse().value()
// => [3, 2, 1]
 
console.log(array);
// => [3, 2, 1]
```
