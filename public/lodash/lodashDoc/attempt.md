## attempt


```js
_.attempt(func, [args])
```

尝试调用`func`，返回结果 或者 捕捉错误对象。任何附加的参数都会在调用时传给`func`。

### 参数:

+ `func` (Function) : 要尝试调用的函数。  

+ `[args]` (...*) : 调用`func`时，传递的参数。  

### 例子:

```js
// Avoid throwing errors for invalid selectors.
var elements = _.attempt(function(selector) {
  return document.querySelectorAll(selector);
}, '>_>');
 
if (_.isError(elements)) {
  elements = [];
}
```
