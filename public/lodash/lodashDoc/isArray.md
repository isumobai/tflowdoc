## isArray


```js
_.isArray(value)
```

检查 `value` 是否是 `Array` 类对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isArray([1, 2, 3]);
// => true
 
_.isArray(document.body.children);
// => false
 
_.isArray('abc');
// => false
 
_.isArray(_.noop);
// => false
```
