## stubString


```js
_.stubString()
```

这个方法返回一个空字符串。

### 例子:

```js
_.times(2, _.stubString);
// => ['', '']
```
