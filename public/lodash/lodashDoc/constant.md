## constant


```js
_.constant(value)
```

创建一个返回 `value` 的函数。

### 参数:

+ `value` (*) : 要新函数返回的值。  

### 例子:

```js
var objects = _.times(2, _.constant({ 'a': 1 }));
 
console.log(objects);
// => [{ 'a': 1 }, { 'a': 1 }]
 
console.log(objects[0] === objects[1]);
// => true
```
