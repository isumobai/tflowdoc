## split


```js
_.split([string=''], separator, [limit])
```

根据`separator` 拆分字符串`string`。

**注意:** 这个方法基于`String#split`.

### 参数:

+ `[string='']` (string) : 要拆分的字符串。  

+ `separator` (RegExp|string) : 拆分的分隔符。  

+ `[limit]` (number) : 限制结果的数量。  

### 例子:

```js
_.split('a-b-c', '-', 2);
// => ['a', 'b']
```
