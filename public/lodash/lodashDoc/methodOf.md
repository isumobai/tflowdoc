## methodOf


```js
_.methodOf(object, [args])
```

`_.method`的反向版。 这个创建一个函数调用给定 `object` 的 path 上的方法， 任何附加的参数都会传入这个调用函数中。

### 参数:

+ `object` (Object) : 要检索的对象。  

+ `[args]` (...*) : 传递给调用函数的参数。  

### 例子:

```js
var array = _.times(3, _.constant),
    object = { 'a': array, 'b': array, 'c': array };
 
_.map(['a[2]', 'c[0]'], _.methodOf(object));
// => [2, 0]
 
_.map([['a', '2'], ['c', '0']], _.methodOf(object));
// => [2, 0]
```
