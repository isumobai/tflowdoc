## isSymbol


```js
_.isSymbol(value)
```

检查 `value` 是否是原始 `Symbol` 或者对象。

### 参数:

+ `value` (*) : 要检查的值。  

### 例子:

```js
_.isSymbol(Symbol.iterator);
// => true
 
_.isSymbol('abc');
// => false
```
