## floor


```js
_.floor(number, [precision=0])
```

根据 `precision`（精度） 向下舍入 `number`。（注： `precision`（精度）可以理解为保留几位小数。）

### 参数:

+ `number` (number) : 要向下舍入的值。  

+ `[precision=0]` (number) : 向下舍入的精度。  

### 例子:

```js
_.floor(4.006);
// => 4
 
_.floor(0.046, 2);
// => 0.04
 
_.floor(4060, -2);
// => 4000
```
