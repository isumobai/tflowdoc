## valuesIn


```js
_.valuesIn(object)
```

创建 `object` 自身和继承的可枚举属性的值为数组

**注意:** 注意: 非对象的值会强制转换为对象。

### 参数:

+ `object` (Object) : 要检索的对象。  

### 例子:

```js
function Foo() {
  this.a = 1;
  this.b = 2;
}
 
Foo.prototype.c = 3;
 
_.valuesIn(new Foo);
// => [1, 2, 3] (无法保证遍历的顺序)
```
