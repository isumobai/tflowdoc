/**
 * docsify插件: 监测目标Vue项目网址中所使用的lodash函数模块并自动引入相应的文档
 */

(function() {

    /*
     * ajax get方法Promise封装
     */
    const ajaxGetPromise = (url) => {

        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.send();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.responseText);
                    } else {
                        reject(xhr)
                    }
                }
            }
        })
    }


    // 当[ lodash-doc ]文档加载出错时候，将其标红显示在页面
    function handloadErr(text) {
        let errTag = `<div style='color: red;padding:50px 0;'>ERROR：[ lodash-toc ] 文档加载失败,请检查相关配置!</div>`
        return text.replace(/\[\s*(lodash-doc)+.*\s*\]/g, errTag);
    }

    // 动态加载lodashDoc本地md文档
    async function getloDocMdCon(arr, lodashDocDocpath) {
        let content = "",
            resD = "";
        for (let i = 0; i < arr.length; i++) {
            const item = arr[i];
            resD += await ajaxGetPromise(lodashDocDocpath + item + ".md").then(res => {
                return res || "" + "\n\n";
            }).catch(err => {
                let errD = `<div style="color:#ff0000;font-weight:bold;padding:20px 0;">${lodashDocDocpath + item + ".md"} 请求失败，请检查文件是否存在!</div>`;
                console.error(errD);
                return errD;
            });
        }
        content += resD || "";
        return content;
    }



    // Moitor模式下
    function handMoitorContent(url, lodashDocPath) {
        return ajaxGetPromise(url).then((res) => {
            // 获取odash已使用的模块名称
            let modList = res.match(/\/node_modules\/(lodash|lodash-es)\/\S*.js/g);
            if (!modList) return false;
            modList = modList.toString().replace(/\/node_modules\/(lodash|lodash-es)\//g, "").replace(/.js/g, "").split(",");

            return modList;
        }).catch(() => {
            console.error(`监听目标网址: ${url} 请求失败，请检查相关配置！`)
        })
    }

    // report模式下
    function handReportContent(url) {
        return ajaxGetPromise(url).then((res) => {

            let resJson = JSON.parse(res);
            let resData = [],
                content = "",
                lodashModList = [];

            function getLodash(obj) {
                for (let i = 0; i < obj.length; i++) {
                    let item = obj[i];
                    if (item.label == ('lodash-es' || "lodash")) {
                        resData = item.groups;
                        return false;
                    }
                    if (item.groups) {
                        getLodash(item.groups);
                    }
                }
                return resData;
            };
            let loDa = getLodash(resJson);

            loDa.forEach(item => {
                let isUseFn = item.label.search(/^_/g)
                if (isUseFn == -1) lodashModList.push(item.label.replace(/\.js$/g, ""))
            });

            return lodashModList;

        }).catch(() => {
            console.error(`${url} 请求失败，请检查文件是否存在!`)
        })
    }



    // docsify 插件自定义函数
    function plugin(hook, vm) {

        // 每次开始解析 Markdown 内容时调用
        hook.beforeEach(function(content, next) {

            let hasLodashTag = content.match(/\[.*(lodash-doc).*\]/ig);
            // 是否开启lodashDoc
            if (!hasLodashTag) {
                next(content);
                return false;
            }

            // 相关参数
            let monitorAppUrl = window.$docsify && window.$docsify.monitorAppUrl || "http://localhost:8000/";
            monitorAppUrl = monitorAppUrl.replace(/\/$/g, "") + "/js/app.js";
            let curUrl = "http://" + window.location.host + "/";
            let reportPath = window.$docsify && window.$docsify.reportFile || "./public/report.json";
            reportPath = curUrl + reportPath;

            let lodashDoc = window.$docsify && window.$docsify.lodashDoc;
            let lodashDocModel = lodashDoc && lodashDoc.mode || "moitor";
            let lodashDocDocpath = lodashDoc && lodashDoc.docPath || "./public/lodash/lodashDoc/";
            lodashDocDocpath = curUrl + lodashDocDocpath.replace(/\/$/g, "") + "/";

            (async function Start() {
                if (lodashDocModel == "moitor") {
                    let loMd = await handMoitorContent(monitorAppUrl) || "";
                    let loMdCon = await getloDocMdCon(loMd, lodashDocDocpath);

                    // 将[ lodash-doc ]替换成markdown文本
                    content = content.replace(/\[.*(lodash-doc)+.*\].*\n?/ig, loMdCon);
                    next(content);
                } else if (lodashDocModel == "report") {
                    let loMd = await handReportContent(reportPath);
                    let loMdCon = await getloDocMdCon(loMd, lodashDocDocpath);

                    // 将[ lodash-doc ]替换成markdown文本
                    content = content.replace(/\[.*(lodash-doc)+.*\].*\n?/ig, loMdCon);
                    next(content);
                } else {
                    next(content);
                }
            })();

        });

    }


    // 注册插件
    window.$docsify.plugins = window.$docsify.plugins.concat(plugin);

})();