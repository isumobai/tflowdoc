(function() {
    /*
     * ajax get方法Promise封装
     */
    const ajaxGetPromise = (url) => {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.send();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.responseText);
                    } else {
                        reject(xhr)
                    }
                }
            }
        })
    }

    const tagAEl = document.querySelectorAll("table a");
    tagAEl.forEach(item => {
        item.onclick = function() {
            const href = item.getAttribute("href");
            const islodashjs = href.match("lodashjs.com") ? true : false;
            loDocLayer(href);
            if (islodashjs) return false;
        }
    });

    async function loDocLayer(hrefText) {
        const loFnName = hrefText.replace(/.*lodash\.(\w*)/g, "$1");
        if (typeof marked === "undefined") await ajaxGetPromise("https://cdn.bootcdn.net/ajax/libs/marked/2.0.3/marked.min.js").then(res => { eval(res) });
        const mdText = loFnName ? await ajaxGetPromise('/public/lodash/lodashDoc/' + loFnName + '.md') : null;
        let mdHtml = mdText ? marked(mdText) : null;

        document.querySelector(".lo-layer-content").innerHTML = mdHtml;
        showLoLayer()
    }

    document.querySelector(".lo-layer-close").onclick = closeLoLayer;
    document.querySelector(".lo-layer-mask").onclick = closeLoLayer;

    function showLoLayer() {
        document.querySelector(".lo-layer").style.display = "block";
    }

    function closeLoLayer() {
        document.querySelector(".lo-layer").style.display = "none";
    }

})();