# Iconfont项目图标预览


<div class="iconfont-wrap"></div>

<script src="public/iconfont/iconfontPreview.js"></script>

<style>
.iconfont-wrap{
    padding-bottom: 20px;
}
.iconfont-head {
    margin: 40px 0 30px;
    line-height: 28px;
    background: #fafafa;
    color: #666;
    padding: 10px 15px;
    border-left: solid 2px #42b983;
}
.iconfont-head .link {
    font-weight: normal;
    color: #666;
    font-size: 14px;
    text-decoration: none
}
.iconfont-head .link:hover {
    color: #42b983;
    text-decoration: underline;
}
.iconfont-content:after {
    display: block;
    clear: both;
    content: ""
}
.iconfont-content .svg-box {
    float: left;
    box-sizing: border-box;
    position: relative;
    width: 14.39%;
    height: 125px;
    padding: 20px 0;
    margin-top: -1px;
    margin-left: -1px;
    overflow: hidden;
    text-align: center;
    border: 1px solid #d0d0d0;
    background-color: #fff
}
.iconfont-content .svg-box:hover {
    background-color: #fafafa;
    cursor: pointer;
}
.iconfont-content .icon {
    width: 56px;
    height: 56px;
    margin-bottom: 10px;
    vertical-align: -.15em;
    fill: currentColor;
    overflow: hidden
}
.iconfont-content .icon-id {
    margin: 0;
    padding: 0 10px;
    height: 22px;
    line-height: 22px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    font-size: 12px
}
.iconfont-content .icon-tip {
    display: none;
    position: absolute;
    bottom: 12px;
    left: 0;
    width: 100%;
    height: 24px;
    line-height: 24px;
    color: #fff;
    margin: 0;
    background-color: rgba(0, 0, 0, .6);
    font-size: 12px
}
</style>


