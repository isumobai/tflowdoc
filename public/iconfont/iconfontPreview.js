(function() {

    /*
     * ajax get方法Promise封装
     */
    const ajaxGetPromise = (url) => {

        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.send(null);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.responseText);
                    } else {
                        console.log(xhr);
                        reject(xhr)
                    }
                }
            }
        })
    }


    /*
     * 监听网址出错时候，将其标红显示在页面
     */
    function handloadErr(url) {
        let errTag = `<div style='color: red;padding:50px 0;'>ERROR：监听目标网址:<b style="padding:0 15px"> [ ${url}  ] </b>请求失败，请检查相关配置！</div>`
        document.querySelector(".iconfont-wrap").innerHTML = errTag;
        console.error(`监听目标网址: ${url} 请求失败，请检查相关配置！`);
    }

    /* 
     * 从目标网址抓取到图标组的<svg>标签和图标ID名数组
     */
    function handSvgText(url) {
        return ajaxGetPromise(url).then((res) => {

            // 判断是否存在<svg></svg>标签
            let isSvgText = res.match(/(<svg><symbol)+[\s\S]*(symbol><\/svg>)+/g);
            let svgText = "";
            let idListArr = [];
            if (isSvgText) {
                // 拼接图标组的SVG DOM节点代码
                svgText = isSvgText[0].replace(/\\"/g, '"').replace(/<svg>/g, '<svg aria-hidden="true" style="position: absolute; width: 0px; height: 0px; overflow: hidden;">');
                // 获取所有图标的ID名,并转成数组
                idListArr = svgText.match(/id="[\S]+"/g).toString().replace(/id="|"/g, "").split(",");
            }
            return {
                svgDom: svgText,
                idArr: idListArr
            };
        }).catch(() => {
            handloadErr(url)
        });
    };
    /*
     * 向body添加SVG DOM节点
     */
    function addSvgDom(svgDom) {
        // 删除已经存在svg标签
        let SvgFirst = document.querySelector("svg");
        SvgFirst ? SvgFirst.remove() : "";

        // 向body添加SVG标签
        let divEl = document.createElement("div");
        divEl.innerHTML = svgDom;
        let svgEl = divEl.getElementsByTagName("svg")[0];
        document.body.insertBefore(svgEl, document.body.firstChild);
    };

    function montagePreviewHtml(idArr, iconfontId) {
        // 拼接单个图标预览div结构
        let svgBoxHtml = "";
        idArr.forEach((svgId) => {
            if (!svgId) return;
            svgBoxHtml += `
                        <div class="svg-box" data-id="${svgId}">
                            <svg class="icon" aria-hidden="true">
                                <use xlink:href="#${svgId}"></use>
                            </svg>
                            <p class="icon-id">${svgId}</p>
                            <p class="icon-tip">复制成功</p>
                        </div>`
        });

        // 拼接整个页面结构
        let isShowLinkIconfont = iconfontId ? `<br><b>项目地址:</b>&nbsp;<a class="link" href="https://www.iconfont.cn/manage/index?manage_type=myprojects&projectId=${iconfontId}" target="_blank">点击查看</a>` : '';
        let boxHtml = `
                    <div class="iconfont-head">
                        <b>使用方法:</b>&nbsp;点击图标即可复制对应的ID
                        ${isShowLinkIconfont}
                    </div>

                    <div class="iconfont-content" class="clearfix">${svgBoxHtml}</div>
                    `;

        boxHtml = boxHtml.replace(/^\s+/gm, "")

        return boxHtml;
    }


    // 组装逻辑执行
    (async function Start() {

        const iconfontConf = window.$docsify && window.$docsify.iconfont || "";
        const iconfontId = iconfontConf && iconfontConf.projectId || "";
        const curUrl = "http://" + window.location.host + "/";
        const iconfontFile = iconfontConf && iconfontConf.iconfontFile || "";
        const iconfontMode = iconfontConf && iconfontConf.mode || "file";
        const monitorAppUrlDef = window.$docsify && window.$docsify.monitorAppUrl || "http://localhost:8000/";
        const monitorAppUrl = monitorAppUrlDef.replace(/\/$/g, "") + "/js/app.js"
        let res = "";

        if (iconfontMode == "file") {
            res = await handSvgText(curUrl + iconfontFile);
        } else if (iconfontMode == "moitor") {
            res = await handSvgText(monitorAppUrl);
        }

        if (!res) return false;

        addSvgDom(res.svgDom);

        // 创建页面
        let svgPreviewHtml = montagePreviewHtml(res.idArr, iconfontId);
        document.querySelector(".iconfont-wrap").innerHTML = svgPreviewHtml;
    })();


    // 页面交互操作
    // 复制svg id 到剪切板
    var copySvgId = function(text, callback) {
        var inputEl = document.createElement("input");
        document.body.appendChild(inputEl);
        inputEl.setAttribute("value", text);
        inputEl.select();
        if (document.execCommand("copy")) {
            document.execCommand("copy");
            if (callback) callback()
        }
        document.body.removeChild(inputEl)
    };


    document.querySelector(".iconfont-wrap").onclick = function(e) {
        var target = e.target || e.srcElement;

        // 事件委托
        // 判断是否有svg-box class名或者祖先元素有svg-box class名
        var actEl = function hasParenDom() {
            while (target.tagName != 'BODY') {
                if (target.classList.contains('svg-box')) {
                    return target
                };
                target = target.parentNode;
            }
        }();

        if (!target.classList.contains('svg-box')) return false;

        var id = actEl && actEl.getAttribute('data-id');
        var elCnAll = document.querySelectorAll('.icon-tip');
        var elCn = target.querySelector('.icon-tip');
        elCnAll.forEach(item => {
            item.style.display = 'none';
        });

        copySvgId(id, function() {
            elCn.style.display = 'block';
        });
        setTimeout(() => {
            elCn.style.display = 'none';
        }, 1000)
    }


})()