window.$docsify = {
    // 左侧顶部LOGO
    name: "项目文档",
    // 右侧 GitHub Corner 挂件URL指向地址
    // repo: 'https://github.com/docsifyjs/docsify/',
    // 开启侧边栏 并指定侧边栏导航配置文件
    loadSidebar: "_sidebar.md",
    alias: {
        '/.*/_sidebar.md': '/_sidebar.md'
    },
    // 首页文件
    homepage: './docs/index.md',
    // 将页面副标题显示到侧边栏目录中
    subMaxLevel: 2,
    // 开始搜索功能
    search: {
        placeholder: '搜索...',
        noData: '找不到内容!',
    },
    // 切换页面后是否自动跳转到页面顶部
    auto2top: true,
    // 需要监听的网址（文档对应项目开发环境地址）
    monitorAppUrl: "http://localhost:7878/",
    // WebpackAnalyse 分析文件保存地址
    // 由webpack插件自动生成，需在vue.config.js配置生成地址
    reportFile: "./public/report.json",
    // iconfont项目ID
    iconfont: {
        projectId: "595381",
        mode: "file", // moitor | file
        iconfontFile: "/public/iconfont/iconfont.js"
    },
    lodashDoc: {
        mode: "report", // moitor | report
        // 本地lodash markdown文档保存地址
        docPath: "public/lodash/lodashDoc/",
    },
    fixedNavTag: true

};