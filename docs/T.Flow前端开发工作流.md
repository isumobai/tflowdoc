# T.Flow前端开发工作流

> T.Flow是一个基于Vue/Vuecli4.X设计的前端开发工作流程。 



## 需求分析 



### UI设计模式

#### 1、基于UI设计语言设计

 UI设计语言是UI的设计规范，这种设计模式下界面规格统一，模块清晰。

**前端实现：**

前端开发工程师根据设计师提供的 基础组件模版UI稿，基于前端UI框架做自定义配置，将组件模版代码化实现。

在实现过程中会出现： 简单的UI框架自定义配置无法满足需求，此时可以
1、和相关人员协商：参考UI框架修改基础组件UI模版或者交互
2、直接修改UI框架组件代码 或者自己开发组件（维护/开发成本高）

 

#### 2、基于前端UI框架设计

此设计模式当于 **基于UI设计语言设计模式** 的简化版，UI设计/交互设计都以团队所选择的UI框架设计标准为主，不再设计项目独有的设计标准，比如 [Ant Design](https://ant.design/index-cn)。

**前端实现：**

该设计模式基本上，前端开发工程师只需对UI框架做自定义配置即可满足需求，开发速度，维护成本都比 **基于UI设计语言设计**更快更低。



#### 3、基于前端UI模版设计

在一些后台管理型网站中对UI界面要求不那么高，我们可以选择一些**后台UI模版框架**直接搭建项目，比如 [iView Admin pro](https://pro.iviewui.com/admin-pro/introduce) ，这种设计模式下，基本上不需要UI设计和交互设计深度参与，前端开发人员即可完成网站界面搭建。

**前端实现：**

直接使用所选的前端UI模版提供的组件组装网站即可。

注意：要是UI设计/交互设计过度修改UI模版，前端开发工程师要根据项目情况确定是否仍然基于前端UI模版开发。前端UI模版设计本意就是让不懂UI设计的前端开发人员快速搭建项目，如果对UI模版代码修改过多反而会加大代码冗余增加维护成本。



#### 4、“自由式”设计

此设计模式是指：UI设计/交互设计没有遵循一定的设计规范或者设计规律设计，当然不推荐这种设计模式，如若前端开发工程师遇到这种情况，需要做了的是：
1、推动UI设计/交互设计朝规范上走
2、重新估计开发时间和做好代码冗余的打算。                                                                               



### UI栅格系统

需要确认UI稿是否基于UI栅格系统设计。

**1、如若UI稿没有基于UI栅格系统设计：**

前端开发工程师需要根据UI稿分析出UI布局的规律，以实现布局代码设计。

**2、如若UI稿基于UI栅格系统设计：**

需要判断UI稿遵循的栅格规则是否与前端UI框架里面的栅格布局规则版一致，如若不一致，需要重新选择栅格布局插件或者自己开发栅格布局模块。



### UI稿还原度要求

原则上是要求100%实现UI稿还原，当然100%还原UI稿需要的开发成本更高，事先确认UI稿的还原度，以便更准备的预估开发时间。



## 项目要求

1、兼容性要求：IE9+

2、SSR要求：否

3、分辨率要求：

|   xs   |   sm   |   md   |   lg    |   xl    |
| :----: | :----: | :----: | :-----: | :-----: |
| <768px | ≥768px | ≥992px | ≥1200px | ≥1920px |

4、是否使用CDN组件

5、...



## 场景方案

此步骤用来确定：多人协同开发下，一个项目内 `同一场景` 使用 `统一` 的解决方案（比如前后端数据交互统一用Axios库来处理），避免资源滥用。

| 场景           | 方案      | 备注                             |
| -------------- | --------- | -------------------------------- |
| UI框架         | ElementUI | https://element.eleme.cn/#/zh-CN |
| 数据可视化     | Echarts   |                                  |
| 前后端数据交互 | Axios.js  | http://www.axios-js.com          |
| ...            | ...       |                                  |





## 自动化流程



### CSS预编译

采用 `SASS` 预处理编辑项目CSS，细节请参考：[Vuecli预处理器介绍](https://cli.vuejs.org/zh/guide/css.html#预处理器)。



### CSS模块化

通过添加`scoped`属性为组件内 CSS 指定作用域，细节请参考：[Vue scoped属性介绍](https://cn.vuejs.org/v2/guide/comparison.html#组件作用域内的-CSS )。

```html
<style scoped >
  ...
</style>
```



### 资源优化

#### 1、小图直接转base64内联

修改项目默认图片转base64大小限制，[Vuecli官方介绍](https://cli.vuejs.org/zh/guide/html-and-static-assets.html#从相对路径导入)

```js
// vue.config.js

module.exports = {
  chainWebpack: config => {
   /**
    * 修改默认内联图片（转base64）大小限制
    * 修改其限制设置为 10kb，小于10kb的图片将被内联
    */
    config.module
      .rule('images')
        .use('url-loader')
          .loader('url-loader')
          .tap(options => Object.assign(options, {limit: 10240}))
  }
}
```



#### 2、大图做无损压缩处理

安装依赖：`npm install -D image-webpack-loader`

注：如遇到错误，可以尝试重新安装或者cnpm安装下

```js
// vue.config.js

module.exports = {
  chainWebpack: config => {
    // >10kb的图片做无损压缩处理                  
    if (process.env.NODE_ENV === 'production') {
			config.module
				.rule('images')
				.use('image-webpack-loader')
				.loader('image-webpack-loader')
				.options({
					bypassOnDebug: true //跳过调试
				})
				.end()
		}
                      
  }
}
```



#### 3、文件GZIP压缩

安装依赖：`npm install -D compression-webpack-plugin `

```js
// vue.config.js

module.exports = {
  
  configureWebpack: config => {
    // 配置GZIP压缩
  	if (process.env.NODE_ENV === 'production') {
			const CompressionPlugin = require("compression-webpack-plugin")
			config.plugins.push(new CompressionPlugin({
				test: /\.js$|\.html$|.\css$/, // 匹配文件名
				threshold: 1024, // 对超过1k的数据压缩
				deleteOriginalAssets: false // 不删除源文件
			}))
		}    
                      
  }
  
}
```

注：打包发布到服务器上，记得验证服务器是否开启GZIP压缩，[在线检测是否开启GZIP](http://tool.chinaz.com/Gzips/)



#### 4、去除生产环境中的JS调试代码

实现依赖`terser-webpack-plugin`,vuecli中自带此插件，下面只是修改插件的默认配置，去除代码中的console语句。

```js
// vue.config.js

module.exports = {
  
  configureWebpack: config => {
  
    if (process.env.NODE_ENV === 'production') {
			config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true;
		}
                      
  }
  
}
```



### 跨域处理

开发环境：通过配置Vuecli中[ devServer.proxy](https://cli.vuejs.org/zh/config/#devserver-proxy)参数实现本地跨域代理转发。
生产环境：修改服务器配置实现跨域。

```js
// vue.config.js

module.exports = {
  devServer: {
  	proxy: {
			'/': {
				target: 'http://localhost:3030', //目标主机
				ws: true,
				changeOrigin: true
			}
		}                   
  }
}
```



### 代码校检

Vuecli在创建项目时可以选配集成Eslint代码效验方案(含Vue代码效验)。T.Flow在此基础上实现代码效验方案：编码时静默，代码Git-commit提交仓库时候自动效验代码后再提交。

Eslint效验规则可以参考下面两个地址自行修改。

[Eslint]: https://eslint.bootcss.com
[eslint-plugin-vue]: https://eslint.vuejs.org



#### 1、关闭编码时校检

开发环境下关闭Eslint实时校检

```js
// user.config.js 

module.exports = {
  // 开发环境下关闭Eslit
  lintOnSave: process.env.NODE_ENV === 'development' ? false : true,
}
```



#### 2、Git-commit 时效验代码

注：此步骤可在Vuecli创建项目时候选配集成



**安装lint-staged**

`npm install -D lint-staged`

 lint-staged可以实现：只校验检查你git commit的部分内容，加快校检速度。



**配置Git Hook**

```js
// package.json

"gitHooks": {
  "pre-commit": "lint-staged"
},
"lint-staged": {
	"*.{js,vue}": [
		"vue-cli-service lint",
     "git add"
   ]
}
```

[Vuecli Git Hook]: https://cli.vuejs.org/zh/guide/cli-service.html#git-hook



### 代码风格

T.Flow 编码风格采用 `prettier`风格，通过Eslint + prettier代码自动格式化 和 编辑器prettier风格代码格式化插件来实现团队编码风格统一。

> 在Eslint做代码效验基础上，搭配`prettier`做自动代码格式化，此方案基于Eslint配置增强，因此需要安装Eslint相关插件及做对应配（**可以在创建项目时选配此方案**）



#### 1、Eslint自动代码格式化(prettier风格)

**安装依赖**

`npm install -D prettier eslint-plugin-prettier @vue/eslint-config-prettier`

**修改Eslint配置**

```js
{
  extends: ["plugin:vue/essential", "@vue/prettier"]
}
```



#### 2、编辑器prettier风格代码格式化

这里只做VS Code配置说明，其他编辑器可自行研究。

安装Vetur插件，Vetur可以实现vue文件代码格式化、代码高亮等功能，Vetur默认代码格式化风格配置就是prettier风格，你也可以在配置中修改。



> 注意：Vetur格式化后的代码略微与Eslint+Prettier格式化后的代码有区别，但不会提示错误信息。比如Vetur会将<template>中的标签属性自动换行，Eslint+Prettier则不会（超过其每行最大字数限制才会换行）

```vue
// Vetur格式化后

<template>
  <div id="app">
    <img
      alt="Vue logo"
      src="./assets/logo.png"
    >
  </div>
</template>
```

```vue
// Eslint+Prettier格式化后的代码

<template>
  <div id="app">
    <img alt="Vue logo" src="./assets/logo.png">
  </div>
</template>
```



### Mock数据

> 推荐使用平台型 ：API文档管理 + 自动化测试 + API Mock 管理工具。由后台开发工程师在平台上管理API文档、自动化测试接口、以及搭建供前端使用的Mock接口数据。前端开发工程师只需在接口完成开发后，在代码中将Mock接口替换为正式接口。

下面提供一种本地化Mock数据方案，方便前端工程师临时模拟数据，用来撑起页面展示骨架。

#### 1、安装依赖

`npm install -D mockjs json-server`

[Mockjs语法参考]: http://mockjs.com/examples.html

注意：每次修改配置文件要重启服务！！！



#### 2、创建Mock目录和文件

```html
├── tools
|   ├── mock			 				 # Mock数据目录
|   |   ├── index.js			 # 入口文件
|   |   ├── news.js			   # 模拟新闻接口模块
```

```js
// mock/index.js

/**
 * Mock Api 入口文件
 * Mockjs语法参考：http://mockjs.com/examples.html
 */

// 新闻接口mock
const news = require('./news.js')
// ...

// 导出接口
module.exports = () => {
  return {
    news
  }
}
```

```js
// mock/news.js

/**
 * Mock新闻接口模块
 */
const Mock = require('mockjs')
const Random = Mock.Random
module.exports = {

  // 导航接口
  nav: Mock.mock({
    "string|1-10": "8"
  }),

  // 新闻接口
  news: Mock.mock({
    "string|1-10": "@1",
    "imga": Random.image('200x100', '#50B347', '#FFF', 'Mock.js')
  })

}
```



#### 3、配置运行命令

```json
// package.json 

"scripts": {
  "mock": "json-server --watch --port 3030 ./tools/mock/index.js"
  ...
}
```

终端执行：`npm run mock`即可开启数据模拟服务



#### 4、项目使用Mock数据

如`/news`接口要使用mock数据，只需在开启Mock服务器后，添加代理配置`'/news': {target: 'http://localhost:3000'}`,所有项目中请求`/news`接口数据都是来自于Mock服务器下的数据。

```js
// vue.config.js

module.exports = {
  devServer: {
  	proxy: {
			// news接口使用mock数据
			// 如果要有设置跟'/'代理，'/news'要放置其前面
			'/news': {target: 'http://localhost:3000'},
			// 全局代理到后台接口服务器
			'/': {
				target: 'http://localhost:6060', //目标主机
				ws: true,
				changeOrigin: true
			}
		}                  
  }
}
```



### 文档管理

使用 [docsify](https://github.com/docsifyjs/docsify/) 做项目文档管理，docsify是基于markdown语法静态文档系统，对项目无侵入。类似项目还有 [book-cli](https://github.com/vvpvvp/book-cli.git )、[docute](https://github.com/egoist/docute)。你还可以用[vuese](https://github.com/shuidi-fed/vuese)组件自动生成文档管理。



#### 1、创建文档管理目录

项目根目录下创建文档目录，并手动初始化docsify，当然你也可以根据自己喜好修改配置(相关配置请参考[docsify](https://github.com/docsifyjs/docsify/) 官网)。

```html
├── tools
|   ├── doc									# 项目文档目录
|   |   ├── docs						# 在这里书写项目文档Markdown文件
|   |   ├── index.html			# docsify文档语言页面
|   |   ├── config.js				# docsify相关配置
```

```html
// index.html

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta charset="UTF-8">
  <link href="https://cdn.bootcss.com/docsify/4.9.4/themes/vue.css" rel="stylesheet">
</head>
<body>
  <div id="app"></div>

  <script async type="module">
    import config from './config.js';
    window.$docsify = config;
  </script>
  <script src="https://cdn.bootcss.com/docsify/4.9.4/docsify.min.js"></script>
  <script src="https://cdn.bootcss.com/docsify/4.9.4/plugins/search.min.js"></script>
  <script src="https://cdn.bootcss.com/docsify/4.9.4/plugins/zoom-image.min.js"></script>
</body>
</html>
```

```js
// config.js

export default {
  // 左侧顶部LOGO
  name: "项目名称",
  // 右侧 GitHub Corner 挂件URL指向地址
  repo: 'https://github.com/docsifyjs/docsify/',
  // 开启侧边栏 并指定侧边栏导航配置文件
  loadSidebar: './docs/sidebar.md',
  // 首页文件
  homepage: './docs/index.md',
  // 将页面副标题显示到侧边栏目录中
  subMaxLevel: 2,
  // 开始搜索功能
  search: {
    placeholder: '搜索...',
    noData: '找不到内容!',
  },
  // 切换页面后是否自动跳转到页面顶部
  auto2top: true
};
```



#### 2、配置文档运行静态服务器

安装依赖：`npm install -D serve`

```json
// package.json

{
  "scripts": {
    "doc": "serve ./tools/doc"
  }
}
```

终端中运行：`npm run doc` 即可访问文档管理页面



#### 3、文档书写&配置

在`tools/doc/docs/`目录下面书写项目的markdown文档即可，支持中文名/中文目录路径，每次添加新的markdown文档需要在指定的侧边栏导航目录配置markdown文件（比如siderbar.md）中做相应配置。详细配置请参考[docsify](https://github.com/docsifyjs/docsify/) 官网。



### 本地预览

项目build后，在本地搭建环境预览项目。具体配置可以参考 [Vuecli搭建本地预览环境](https://cli.vuejs.org/zh/guide/deployment.html#本地预览)

安装依赖：`npm install -D serve`

```json
// package.json

{
  "scripts": {
    "pro": "serve dist"
  }
}
```

终端中运行：`npm run pro` 即可访预览本地build后的项目



### 打包分析

想要分析项目webpack打包的资源大小以及组件关系，Vuecli中已经集成了相应插件，只需在终端运行`vue ui`即可通过vue项目UI界面工具平台查看分析项目相关信息。

也可以在打包命令后面添加`--report`,每次打包完成都会在`/dist`目录下生成`report.html`分析文件，请参考[vuecli bulid 命令参数](https://cli.vuejs.org/zh/guide/cli-service.html#vue-cli-service-build)

```js
// package.json
{
    "scripts": {
        "build": "vue-cli-service build --report"
    },
}
```





### NPM加速

nrm 是一个 npm 源管理器，使用 nrm 可以对每一个 npm 源快速测速，并快速切换当前 npm服务访问源。

安装依赖：`npm install -D nrm node-cmd`



#### 1、创建NPM服务加速管理目录

```html
├── tools
|   ├── npmFast							# NPM服务加速目录
|   |   ├── index.js				# 入口文件
```

```js
// tools/npmFast/index.js

/**
 * 自动设置最快NPM源
 * 依赖 npm install -g node-cmd nrm
 */
var cmd = require("node-cmd");

// 根据访问速度排序
var sortNumber = function(a, b) {
  return a.sd - b.sd;
};

// 测试每个源速度
var testSd = function(arr) {
  var linkList = arr;
  var sdList = [];

  for (i in linkList) {
    var rl = linkList[i];
    cmd.get(`nrm test ${rl}`, function(err, data) {
      var name = data.match(/\S+(?=\s+-*\s+)/g),
        name = name ? name[0] : null;
      var sd = data.match(/\d+(?=ms)/g),
        sd = sd ? sd[0] : "--";
      sdList.push({ name, sd });
      console.log(`NPM源: ${name} ---- 速度:${sd}ms`);

      if (linkList.length == sdList.length) {
        setLinkurl(sdList);
      }
    });
  }
};

// 将访问速度最快的源设置为当前源
var setLinkurl = function(sdList) {
  sdList.sort(sortNumber);
  var minName = sdList[0].name;
  cmd.get(`nrm use ${minName}`, function(err, data) {
    console.log(
      `\n\n>>>>>>  "${minName}"源速度最快，将设置为当前NPM源`
    );
    console.log(data);
    return false;
  });
};

// 查询可访问点列表
var sechLinkList = function() {
  cmd.get("nrm ls", function(err, data) {
    var linkList = data.match(/\S+(?=\s+--)/g);
    console.log("\n\n>>>>>>  开始查询可用NPM源\n");

    testSd(linkList);
  });
};

// 执行
var start = (function() {
  sechLinkList();
})();

```



#### 2、添加执行命令

```json
// package.json

{
  "scripts": {
    "npm-fast": "node tools/npmFast/"
  }
}
```

终端中运行：`npm run npm-fast`即可执行ZIP压缩



### ZIP打包

有时需要将build后项目发给别人，这时就需要将文件ZIP压缩。

安装依赖：`npm install -D compressing`



#### 1、创建ZIP管理目录

```html
├── tools
|   ├── zip									# ZIP目录
|   |   ├── file						# 目录用于保存压缩文件
|   |   ├── index.js				# 入口文件
|   |   ├── config.js				# 配置文件
```

```js
// tools/zip/config.js

module.exports = {
  entryPath: "./dist", // 需要压缩的文件夹
  outputPath: "./tools/zip/files", // 压缩后文件保存路径
  fileName: "dist.zip", // 压缩后的文件名
  copyDesktop: true //是否复制一份到‘电脑桌面’
}

```

```js
// tools/zip/index.js

// 依赖: npm install -D compressing
const fs = require("fs")
const compressing = require('compressing');

// 导入配置
const config = require('./config.js');
// 需要压缩的文件夹路径
const entryPath = config.entryPath;
// 压缩后的文件名
const fileName = config.fileName;
const isCopyDesktop = config.copyDesktop;
// 压缩后的文件夹保存路径
const outputFilePath = (config.outputPath).replace(/\/$/gi, "") + '/' + fileName;
// 压缩文件备份路径
const outputBackPath = outputFilePath.replace(/zip$/gi, "") + 'back.zip';

/* 复制一份到电脑桌面 */
const copyDesktop = function () {
  const os = require('os');
  const desktopPath = os.homedir() + '\\Desktop' + '\\dist.zip';
  fs.copyFile(outputFilePath, desktopPath, function (err) {
    if (err) {
      console.log('>>>>> 复制到系统桌面失败', err)
    } else {
      console.log('>>>>> 复制到系统桌面成功:', desktopPath)
    }
  })
}

/* 压缩 */
const zip = function () {
  compressing.zip.compressDir(entryPath, outputFilePath)
    .then(() => {
      console.log('>>>>> 压缩成功:', outputFilePath);
      // 开始复制到桌面
      if (isCopyDesktop) { copyDesktop(); }
    })
    .catch(err => {
      console.log('>>>>> 压缩失败', err);
    });

}

/* 备份 */
const back = function () {
  fs.rename(outputFilePath, outputBackPath, function (err) {
    if (err) {
      console.log('>>>>> 备份失败', err);
    } else {
      console.log(">>>>> 备份成功:", outputBackPath)
      // 开始压缩
      zip()
    }
  })
}

/* 执行 */
fs.exists(config.entryPath, function (exs) {
  if (exs) {
    console.log('\n>>>>> 开始压缩:', entryPath)
    fs.exists(outputFilePath, function (exists) {
      (exists) ? back() : zip()
    });
  } else {
    console.log('>>>>> [ '+config.entryPath+' ] 不存在, 请先执行: npm run build')
  }
});

```



#### 2、添加执行命令

```json
// package.json

{
  "scripts": {
    "zip": "node tools/zip/"
  }
}
```

终端中运行：`npm run zip`即可执行ZIP压缩



### 远程发布

通过SFTP将项目文件上传到服务器上。

安装依赖：`npm install -D scp2 ssh2`

#### 1、创建远程发布管理目录

```html
├── tools
|   ├── deploy							# 远程发布
|   |   ├── index.js				# 入口文件
|   |   ├── config.js				# 配置文件
```

```js
// index.js

const config = require("./config.js");
const scpClient = require("scp2");
const Client = require("ssh2").Client;
const readline = require("readline");

// 获取命令行的参数
// 默认为: 'test'
const typeKey =
  (function() {
    let type = process.argv.slice(2)[0] || "";
    type = type.replace(/--type=/, "");
    return type;
  })() || "test";

// 操作的服务器名
const serveTitle = config[typeKey].title;

// 用户登录信息
let serveInfo = {
  host: config[typeKey].host,
  port: config[typeKey].port,
  username: config[typeKey].username || "",
  password: config[typeKey].password || "",
  path: config[typeKey].path || ""
};
// 本地上传路径
const entryPath = config[typeKey].entry;
// 服务器存储路径
const outputPath = config[typeKey].output || "";

/* 上传文件到服务器 */
const uploadFile = function(callback) {
  console.log(`[ ${serveTitle} ] >>>>> 开始上传文件... \n`);
  scpClient.scp(entryPath, serveInfo, function(err) {
    if (err) {
      console.log(`[ ${serveTitle} ] >>>>> 文件上传失败 \n`);
      throw err;
    } else {
      console.log(`[ ${serveTitle} ] >>>>> 文件上传成功 \n`);

      if (callback) callback();
    }
  });
};

/* Shell执行函数 */
const shellFn = function(conn, shellArr, callback) {
  const shellArrList = shellArr;

  conn.shell(function(err, stream) {
    if (err) throw err;
    stream
      .on("close", function() {
        if (callback) callback();
      })
      .on("data", function(data) {
        console.log("STDOUT: " + data);
      });

    stream.end(shellArrList.join(""));
  });
};

/* 连接服务器 */
const linkServe = function() {
  const conn = new Client();
  const bfShell = config[typeKey].uploadBeforeShell;
  const afShell = config[typeKey].uploadAfterShell;
  conn
    .on("ready", function() {
      // 执行文件上传之后的Shell命令
      const afterShellFn = function(callback) {
        console.log(
          `\n\n[ ${serveTitle} ] >>>>> 开始执行: 文件上传后的Shell命令 \n`
        );
        shellFn(conn, afShell, function() {
          console.log(
            `[ ${serveTitle} ] >>>>> 执行完: 文件上传后的Shell命令 \n`
          );
          if (callback) callback();
        });
      };

      // 执行文件上传之前的Shell命令
      if (bfShell.length) {
        console.log(
          `\n\n[ ${serveTitle} ] >>>>> 开始执行: 文件上传前的Shell命令 \n`
        );
        shellFn(conn, bfShell, function() {
          console.log(
            `\n\n[ ${serveTitle} ] >>>>> 执行完: 文件上传前的Shell命令 \n`
          );

          uploadFile(function() {
            if (afShell.length) {
              afterShellFn(function() {
                process.exit();
              });
            } else {
              process.exit();
            }
          });
        });
      }
    })
    .on("error", function(err) {
      console.log(`[ ${serveTitle} ] >>>>> 连接服务器失败 \n`);
      throw err;
    })
    .connect(serveInfo);
};

/* 手动输入用户账户&密码 */
const inputInfo = function(fn) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  const inputUsername = function(callback) {
    rl.question(`[ ${serveTitle} ] >>>>> 请输入用户名: `, answer => {
      if (!answer) return;
      serveInfo.username = answer.replace(/\r\n$/, "");
      callback();
    });
  };

  const inputPassword = function(callback) {
    rl.question(`[ ${serveTitle} ] >>>>> 请输入密码: `, answer => {
      if (!answer) return;
      serveInfo.password = answer.replace(/\r\n$/, "");
      callback();
    });
  };

  if (!config[typeKey].username) {
    inputUsername(function() {
      inputPassword(fn);
    });
  } else if (!config[typeKey].password) {
    inputPassword(fn);
  } else {
    fn();
  }
};

/* 执行逻辑 */
const run = (function() {

  console.log(`\n██████████  ${serveTitle}  ██████████\n`)

  inputInfo(linkServe);
  
})();

```

```js
// config.js

module.exports = {
  // 测试服务器
  test: {
    // 服务器名
    title: "测试服务器",
    // 服务器地址
    host: "111.229.85.71",
    // 服务器端口
    port: 22,
    // 服务器登录用户名
    username: "root",
    // 服务器登录密码
    password: "root",
    // 本地上传文件路径
    entry: "./dist/",
    // 服务器存储文件路径
    path: "",
    // 文件上传服务器前执行的Shell命令
    uploadBeforeShell: [
      // 查看当前目录
      "echo `cd $(dirname $0); pwd -P` \n",
      // 删除备份文件
      "rm -f back.zip \n",
      // 将目录下所有文件ZIP压缩备份，并删除文件
      "zip -m -q -r -o back.zip ./ \n",
      "exit \n"
    ],
    // 文件上传服务器后执行的Shell命令
    uploadAfterShell: []
  },

  // 生产服务器
  production: {
    title: "生产服务器",
    host: "111.111.111.111",
    port: 22,
    username: "",
    password: "",
    entry: "./dist/",
    output: "",
    uploadBeforeShell: []
  }
};

```



#### 2、创建运行命令

```js
// package.json

{
  "scripts": {
    "deploy:test": "node tools/deploy/ --type=test",
    "deploy:pro": "node tools/deploy/ --type=production"
  }
}
```

`node tools/deploy/ --type=test` 中 `--tyoe={key}` key的值与 config.js 中的key值对应。比如：`--tyoe=test`代表连接的是 测试服务器，`--tyoe=production`代表连接的是 生产服务器。

终端执行：`npm run deploy:test`  发布到测试服务器；`npm run deploy:pro`  发布到生产服务器。



#### 3、服务器帐号密码相关配置

**方式一：在配置文件中设置帐号密码（不推荐）**

将帐号密码直接在配置文件`config.js`中明文书写，终端执行发布命令后即可执行相关逻辑。此种方式安全性低**不推荐**

```js
// config.js

module.exports = {
  // 测试服务器
  test: {
		...
                      
    // 服务器登录用户名
    username: "root",
    // 服务器登录密码
    password: "root",

    ...
  }
}
```



**方式二：帐号密码终端手动输入**

配置文件`config.js` 中 不设置帐号密码 或者 只设置帐号不设置密码，当终端执行发布命令后，终端会提示你手动输入：帐号&密码 或者 密码。

```js
// config.js

module.exports = {
  // 测试服务器
  test: {
		...
                      
    // 服务器登录用户名
    // username: "root",
                      
    // 服务器登录密码
    // password: "root",

    ...
  }
}
```



**方式三：配置文件记录帐号密码**

在`tools/deploy/`创建`user.config.js`文件，在其中记录帐号密码信息，并将其添加到GIT的忽略名单中。

```js
// user.config.js
module.exports = {
  test:{
    username: "root",
    password: "123456"
  }
}
```

配置文件`config.js`中引入：

```js
// config.js

const userConfig = require("./user.config.js");
module.exports = {
  // 测试服务器
  test: {
		...
                      
    // 服务器登录用户名
    username: userConfig.username,
    // 服务器登录密码
    password: userConfig.password,

    ...
  }
}
```

GIT忽略`user.config.js`提交：

```text
// .gitignore

# 个人服务器信息文件
user.config.js
```



### UI稿标注

借助UI标注，可以让前端开发工程师快速准确的获取UI稿上元素的相关数据。

1、要求UI设计师提供带标注的UI稿
2、使用UI稿标注工具自行获取数据（推荐）

**常用的UI稿标注工具:**
[PxCook](http://www.fancynode.com.cn/pxcook)、[标你妹呀](http://www.biaonimeia.com/)、[MarkMan](http://www.getmarkman.com/)





## 代码设计

基于Vuecli4.X做项目结构代码优化。



### 目录结构

```html
├── src
│   ├── api									# 接口管理
│   ├── assets							# 资源管理
│   |   ├── images					# 图片资源
│   |   ├── scripts					# 脚本资源
│   |   ├── styles					# 样式资源
│   ├── components					# 组件
│   ├── views								# 页面
│   ├── router							# 路由
├── tools										# 开发辅助工具
├── vue.config.js						# 配置文件

├── dist
├── node_modules
├── ...
```



### 全局样式和样式重置

#### 1、全局样式和重置样式代码

采用 [Normalize.css](http://necolas.github.io/normalize.css/) + 自定义(公共样式) 增强跨浏览器表现的一致性模式 ，替换传统的reset.css方式。

```scss
// 文件路径: src/assets/styles/common.scss

// 导入 normalize.css 文件
@import "./utils/normalize";

// 默认样式
...
```

#### 2、项目中引入

```js
//文件: src/main.js

import Vue from 'vue'
// 引入公公共样式 (需在App.vue前引入)
import './assets/styles/common.scss'
import App from './App.vue'
...
```



### SASS工具库

#### 1、创建文件目录

```html
src/assets/

├── styles
|   ├── utils
|   |   ├── _media.scss							# media查询管理
|   |   ├── _minix.scss							# Sass-minix管理
|   |   ├── _variables.scss					# 变量管理
|   |   ├── _selector.scss					# CSS3伪元素选择器库

|   ├── utils.scss									# 入口文件
```



#### 2、`utils.scss` 入口文件中导入文件

```scss
// utils.scss

// 工具函数库
@import "./utils/minix";
// CSS3伪元素选择器
@import "./utils/selector";
// z-index层级管理
@import "./utils/z-index";
...
```



#### 3、项目中引入SCSS工具函数

```js
// vue.config.js

module.exports = {
  css:{
    loaderOptions: {
			scss: {
				// 引入全局sass文件
				prependData: `@import "@/assets/styles/utils.scss";`
			}
		}                
  }
}
```



### JS工具函数 

工具函数库采用：Lodash 模块按需引入 + 自定封装 + 第三方模块 模式



#### 1、lodash工具函数库按需加载配置

[lodash]: https://www.lodashjs.com/

安装依赖：

`npm install lodash-es `   

`npm install lodash-webpack-plugin babel-plugin-lodash -D`

修改 .babelrc：

```js
// babel.config.js

module.exports = {
  'plugins': ['lodash']
  ...
}
```

添加webpack配置：

```js
// vue.config.js

module.exports = {
  
  configureWebpack: config => {
    
		// Lodash按需加载
      	// 自动去除没有使用的函数
		if (process.env.NODE_ENV === 'production') {
			const LodashModuleReplacementPlugin = require('lodash-webpack-plugin')
			config.plugins.push(new LodashModuleReplacementPlugin())
		}
                      
  }
  
}
```



#### 2、新建工具函数目录：

```html
src/

├── assets
|   ├── scripts	
|   |   ├── utils											# 工具函数目录
|   |   |   ├── _lodash-module.js			# lodash导入模块管理
|   |   |   ├── _localStorage.js			# localStorage封装

|   |   ├── utils.js									# 工具函数入口文件
```

```js
// script/utils/lodash-module.js

/**
 * 管理需要引入的 Lodash 函数
 * lodash文档：https://www.lodashjs.com/
 */

// 引入模块
import {
  debounce, 
  throttle, 
  once
} from 'lodash-es'

// 导出模块
export default {
  debounce, 
  throttle, 
  once
}
```

```js
// script/utils.js

/**
 * 工具函数入口文件
 */

import lodashModule from './utils/lodash-module.js'
import localStorage from './utils/localStorage.js'

export default {
  ...lodashModule,
  localStorage
}
```



#### 3、将工具函数挂载到Vue实例上

```js
// main.js

import utils from './assets/scripts/utils.js'

// 将所有工具函数挂载到Vue上
Vue.prototype.$util = utils;
```



### API管理

#### 1、Axios 自定义封装

安装依赖：`npm install axios`

```html
src/

├── api
|   ├── _http.js						# axios 封装文件

```

```js
// _http.js

/**
 * axios封装
 * 请求拦截、响应拦截、错误统一处理
 */
import axios from 'axios';

/**
 * 创建axios实例
 */
var instanceAxios = axios.create({
  timeout: 1000 // 设置请求超时时间
});

// 设置post请求头
instanceAxios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

/**
 * 请求拦截器
 */
instanceAxios.interceptors.request.use(
  config => {
    // 每次发送请求之前判断是否存在token，
    // 如果存在，则统一在http请求的header都加上token，
    // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断

    // const token = store.state.token;        
    // token && (config.headers.Authorization = token);     

    return config;
  },
  error => {
    return Promise.error(error);
  });


/**
 * 响应拦截器
 */
instanceAxios.interceptors.response.use(

  // 如果返回的状态码为200，说明接口请求成功
  // 否则的话抛出错误
  res => res.status === 200 ? Promise.resolve(res) : Promise.reject(res),

  // 请求失败
  error => {
    const { response } = error;
    if (response) {

      // 请求已发出，但是不在2xx的范围 
      // 根据和后台人员协商的状态码判断不同情形
      switch (response.status) {

        // 401: 未登录状态
        // 跳转登录页重新登录
        case 401:
          // ...
          break;

        // 403 token过期                
        // 登录过期对用户进行提示                
        // 清除本地token和清空vuex中token对象                
        // 跳转登录页面 
        case 403:
          // tip('登录过期，请重新登录');
          // localStorage.removeItem('token');
          // store.commit('loginSuccess', null);
          // setTimeout(() => {
          //   toLogin();
          // }, 1000);
          break;

        // 404 请求不存在
        // 跳转404页或者其他操作
        case 404:
          // ...
          break;

        // 其他情况
        default:
          console.log(response.data.message);
      }

      return Promise.reject(response);

    } else {
      // 处理断网的情况
      // ...
    }
  });

/**
 * 导出axios实例
 */
export default instanceAxios;
```



#### 2、接口域名管理文件

```html
src/

├── api
|   ├── _base.js						# 接口域名管理文件
```

```js
/**
 * 接口域名的管理
 */

const baseObj = {
  // 生产环境
  'production': {
    baseURL: 'http://xxx:9091/pro/',
    authBaseURL: ''
  },
  // 开发环境
  'development': {
    baseURL: 'http://xxxx:9091/dev/',
    authBaseURL: ''
  },
  // 测试环境
  'test': {
    baseURL: 'http://xxxx:9091/test/',
    authBaseURL: ''
  }
}

export default baseObj[process.env.NODE_ENV]
```



#### 3、需求模块接口文件

```html
src/

├── api
|   ├── _article.js						# article模块接口
|   ├── _user.js							# user模块接口
|   ├── ...
```

```js
// article.js

import base from './_base'; // 导入接口域名列表
import axios from './_http'; // 导入http中创建的axios实例
import qs from 'qs'; // 根据需求是否导入qs模块

export default {
  articleList () {        
     return axios.get(`${base.sq}/topics`);    
  }
  ...
}
```



#### 4、入口文件中引入所有接口文件

```html
src/

├── api
|   ├── index.js						# 接口入口文件
```

```js
// api/index.js

import article from './_article';
...

export default {
	article
	...
}
```



#### 5、将所有接口挂载到vue实例上

```js
// main.js

import api from './api/index.js'

// 将所有API挂载到Vue上
Vue.prototype.$api = api;
```



### 路由&组件懒加载

[官方介绍]: https://router.vuejs.org/zh/guide/advanced/lazy-loading.html



### 文件目录别名配置

```js
// vue.config.js

chainWebpack: config => {

		/**
		 ****** 文件路劲别名设置 ******
		 */
		const path = require('path')
		const resolve = (dir) => path.join(__dirname, dir);

		config.resolve.alias
			.set('@assets', resolve('src/assets'))
			.set('@images', resolve('src/assets/images'))
			.set('@styles', resolve('src/assets/styles'))
			.set('@scripts', resolve('src/assets/scripts'))

		// ****** END ****** //	

}
```



### Map文件生成配置

```js
// vue.config.js

module.exports = {
  
  // 生成Map文件(只在开发环境下生成)
	productionSourceMap: process.env.NODE_ENV === 'development' ? true : false,
	...
  
  css: {
  	// 生成 CSS source maps (只在开发环境下生成)
		sourceMap: process.env.NODE_ENV === 'development' ? true : false,
		...
  }
                      
}
```





## 解决方案

### CSS方案

#### 1、移动端1px细边框(Sass)

```scss
/**
 * 移动端1px边框解决方案
 * 使用: @include retina-1px($color,$direction)
 * $color: 边框颜色  默认: #d0d0d0
 * $direction: 边框方向  默认: bottom
 */
@mixin retina-1px($color:#d0d0d0,$direction:bottom) {

  // 参数类型转换为字符串
  $borderDirection: ''+$direction+'';

  // CSS
  position: relative;

  &:after {
    position: absolute;
    content: "";
    top: -50%;
    bottom: -50%;
    left: -50%;
    right: -50%;
    -webkit-transform: scale(0.5);
    transform: scale(0.5);
    border-#{$borderDirection}: 1px solid $color;
  }

  // 像素缩放比为3时设置为0.33
  @media (-webkit-min-device-pixel-radio: 3), (min-device-pixel-radio: 3) {
    &::after {
      -webkit-transform: scaleY(0.33333333);
      transform: scaleY(0.33333333);
    }
  }

  // 像素缩放比为2时设置为0.5
  @media (-webkit-min-device-pixel-radio: 2), (min-device-pixel-radio: 2) {
    &::after {
      -webkit-transform: scaleY(0.5);
      transform: scaleY(0.5);
    }
  }

}
```



#### 2、浏览器样式重置 - Normalize.css

[Normalize.css](http://necolas.github.io/normalize.css)是一种CSS reset的替代方案。它在默认的HTML元素样式上提供了跨浏览器的高度一致性。相比于传统的CSS reset，Normalize.css是一种现代的、为HTML5准备的优质替代方案。



#### 3、CSS实现 Footer始终保持在页面底部

html结构

```html
<body>
  <div id="root">
    <div id="root_footer"></div>
  </div>
  <div id="footer">
    Footer content goes here.
  </div>
</body>
```

CSS

```css
html, body {
  height: 100%;
}

#root {
  clear: both;
  min-height: 100%;
  height: auto !important;
  height: 100%;
  margin-bottom: -54px;
}
#root #root_footer {
  height: 54px;
}

#footer {
  clear: both;
  position: relative;
  height: 54px;
}
```



### Iconfont文件快速更新

在使用 Iconfont 图标的时候，每次Iconfont平台更新了图标库都需要手动下载到本地替换项目文件。下面基于Iconfont - Symbol 引用方式制作一个快速更新项目文件（当然你也可以直接使用阿里在线CDN直接省略此步骤）



#### 1、新建目录及文件

```html
├── src
...
├── tools			 				 	        	 
|   ├── iconfont      		# Iconfont快速更新		  	 
|   |   ├── index.js		  # 入口文件
|   |   ├── config.js		  # 配置文件

|   |   ├── preview		    # Icon预览文件夹  
```

```js
// index.js

const request = require("request");
const fs = require("fs");
const readline = require("readline");

// 导入配置文件
const config = require("./config.js");

// 清理内容多余参数
function clearFileinfo(callback) {
    fs.readFile(config.projectPath, function(err, data) {
        if (err) {
            throw err;
        }

        let readRst = data.toString();
        readRst = readRst.replace(/fill="#(\S*)"\s+/g, "");

        fs.writeFile(config.projectPath, readRst, function(err) {
            if (err) {
                console.log("清理内容多余参数失败", err);
            }
            if (callback) callback();
        });
    });
}

// 下载文件到项目使用
function downPojectFile(url, callback) {
    request(url, function(err) {
            if (err) {
                console.log("请求URL失败", err);
            }
        })
        .pipe(
            fs.createWriteStream(config.projectPath).on("error", error => {
                console.log("文件保存异常!", error);
            })
        )
        .on("close", function() {
            if (callback) callback();
        });
}

// 创建预览页面(html文件)
function createrPreviewHtml(url, callback) {
    const iconfontId = url.match(/_(\S*)_/)[1];

    fs.readFile(config.projectPath, function(err, data) {
        if (err) {
            throw err;
        }

        let readRst = data.toString();
        readRst = readRst.replace(/fill="#(\S*)"\s+/g, "");
        let svgHtml = readRst.match(/<svg>.*<\/svg>/g);
        svgHtml = svgHtml.toString();
        svgHtml = svgHtml.replace(
            /<svg>/g,
            '<svg aria-hidden="true" style="position: absolute; width: 0px; height: 0px; overflow: hidden;">'
        );
        const previewHtml = `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="ie=edge"><title>ICON PREVIEW</title><style type="text/css">a,body,p,span{padding:0;margin:0}#app{width:1000px;margin:0 auto 50px;}#app-head{margin:50px 0 30px;text-align:center}#app-head .title{font-size:34px;font-weight:700;padding-bottom:10px}#app-head .link,#app-head .tip{padding-top:10px;color:#666;font-size:14px;text-decoration:none}#app-head .link:hover{color:#007acc}#app-content:after{display:block;clear:both;content:""}#app-content .svg-box{float:left;box-sizing:border-box;position:relative;width:12.5%;height:125px;padding:20px 0;margin-top:-1px;margin-left:-1px;overflow:hidden;text-align:center;border:1px solid #d0d0d0;background-color:#fff}#app-content .svg-box:hover{background-color:#f0f0f0;cursor:pointer}#app-content .icon{width:56px;height:56px;margin-bottom:10px;vertical-align:-.15em;fill:currentColor;overflow:hidden}#app-content .icon-id{margin:0;padding:0 10px;height:22px;line-height:22px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;font-size:12px}#app-content .icon-tip{display:none;position:absolute;bottom:12px;left:0;width:100%;height:24px;line-height:24px;color:#fff;margin:0;background-color:rgba(0,0,0,.6);font-size:12px}</style></head><body>${svgHtml}<div id="app"><div id="app-head"><p class="title">Iconfont(Symbol) Preview</p><p class="tip">使用方法: 点击图标即可复制对应的ID</p><a class="link" href="https://www.iconfont.cn/manage/index?manage_type=myprojects&projectId=${iconfontId}" target="_blank">[ 查看Iconfont项目地址 ]</a></div><div id="app-content" class="clearfix"></div></div><script>window.onload=function(){var getSvgIdList=function(){var svgIdList=[];var elSvg=document.querySelectorAll('svg');var elSymbol=elSvg[0].querySelectorAll('symbol');Array.prototype.forEach.call(elSymbol,function(el){var symbolId=el.getAttribute("id");svgIdList.push(symbolId)});return svgIdList};var getSvgHtml=function(){var svgHtml='';var svgIdList=getSvgIdList();for(var i=0;i<svgIdList.length;i++){var id=svgIdList[i];svgHtml+='<div class="svg-box" data-id="'+id+'">'+'<svg class="icon" aria-hidden="true">'+'<use xlink:href="#'+id+'"></use>'+'</svg>'+'<p class="icon-id">'+id+'</p>'+'<p class="icon-tip">复制成功</p>'+'</div>'};return svgHtml};var svgHtml=getSvgHtml();var elApp=document.querySelectorAll('#app-content');elApp[0].innerHTML=svgHtml;var copySvgId=function(text,callback){var inputEl=document.createElement("input");document.body.appendChild(inputEl);inputEl.setAttribute("value",text);inputEl.select();if(document.execCommand("copy")){document.execCommand("copy");callback()}document.body.removeChild(inputEl)};var svgBox=elApp[0].querySelectorAll('.svg-box');Array.prototype.forEach.call(svgBox,function(el){el.onclick=function(){var id=el.getAttribute('data-id');var elClick=el.querySelectorAll('.icon-tip');Array.prototype.filter.call(el.parentNode.children,function(child){var elCn=child.querySelectorAll('.icon-tip');elCn[0].style.display='none'});copySvgId(id,function(){elClick[0].style.display='block'});setTimeout(()=>{elClick[0].style.display='none'},1500)}})}</script></body></html>`;

        fs.writeFile(config.previewHtmlPath, previewHtml, function(error) {
            if (error) {
                console.log("预览文件(HTML)页面创建失败", error);
            } else {
                callback();
            }
        });
    });
}

// 创建预览页面(MD文件)
function createrPreviewMD(url, callback) {
    fs.readFile(config.projectPath, function(err, data) {
        if (err) {
            throw err;
        }
        let iconJs = data.toString();
        iconJs = iconJs.replace(/fill="#(\S*)"\s+/g, "");
        let iconfontId = url.match(/_(\S*)_/)[1];

        let previewMD = `
# Iconfont 字体图标预览

?> **使用方法**: 点击图标即可复制对应的 ID  
**项目地址**: [ 点击查看 ](https://www.iconfont.cn/manage/index?manage_type=myprojects&projectId=${iconfontId} ":target=_blank")

<div id="iconPreviewBox" class="icon-box-wrap clearfix">
    <div
    class="icon-box"
    v-for="(item, ind) in iconLists"
    :key="ind"
    @click="copyId(item.id)"
    >
    <svg :class="'icon ' + item.id" :id="item.id" v-html="item.pathHtml" viewBox="0 0 1024 1024">
    ></svg>
    <p class="icon-id">{{ item.id}}</p>
    <p :class="'icon-tip icon-tip-' + item.id">复制成功</p>
    </div>
</div>

<script>
    // 添加iconfont.js到页面
    ${iconJs}

    new Vue({
        el: '#iconPreviewBox',
        data(){
            return{
                iconLists:[]
            }
        },
        methods:{
            // 复制 ID
            copyId(idname) {
                let inputEl = document.createElement("input");
                document.body.appendChild(inputEl);
                inputEl.setAttribute("value", idname);
                inputEl.select();
                if (document.execCommand("copy")) {
                    document.execCommand("copy");
                    const $el = document.querySelectorAll(".icon-tip-" + idname);
                    $el[0].style = "display:block";
    
                    setTimeout(function () {
                        $el[0].style = "display:none";
                    }, 1000);
                }
                document.body.removeChild(inputEl);
            },
        },
        mounted(){
            // 添加CSS代码
            const styleCssCode ='.clearfix:before,.clearfix:after{content:"";display:block;height:0;visibility:hidden;}.clearfix:after{clear:both;}.icon-box-wrap{background:#fafafa;}.icon-box{float:left;box-sizing:border-box;position:relative;width:14.39%;height:125px;padding:20px 0;margin-top:-1px;margin-left:-1px;overflow:hidden;text-align:center;border:1px solid #d0d0d0;background-color:#fff;}.icon-box:hover{background-color:rgba(66,185,131,.1);cursor:pointer;}.icon{width:56px;height:56px;margin-bottom:10px;vertical-align:-0.15em;fill:currentColor;overflow:hidden;}.icon-box-wrap .icon-id{margin:0;padding:0 10px;height:22px;line-height:22px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;font-size:12px;}.icon-box-wrap .icon-tip{display:none;position:absolute;bottom:12px;left:0;width:100%;height:24px;line-height:24px;color:#fff;margin:0;background-color:rgba(0,0,0,0.6);font-size:12px;}.icon-tip-show{display:block;}';
            const styleEl = document.createElement("style");
            styleEl.appendChild(document.createTextNode(styleCssCode));
            const headEl = document.getElementsByTagName("head")[0];
            headEl.appendChild(styleEl);
    
            //获取图标数组
            setTimeout(() => {
                let svgIdList = [];
                let elSvg = document.querySelectorAll('svg');
                let elSymbol = elSvg[0].querySelectorAll('symbol');
                Array.prototype.forEach.call(elSymbol,function(el) {
                    let symbolId = el.getAttribute("id");
                    let pathHtml = el.innerHTML;
                    svgIdList.push({id:symbolId,pathHtml:pathHtml})
                });
                this.iconLists = svgIdList
            }, 300);
    
        }
        
    });
</script>
`;

        fs.writeFile(config.previewMdPath, previewMD, function(error) {
            if (error) {
                console.log("预览文件页面(MD)创建失败", error);
            } else {
                callback();
            }
        });
    });
}


// 获取用户命令端输入的信息
function readSyncByRl(tips) {
    tips = ">>>>> 请输入Iconfont(Symbol模式)文件CDN地址:" || "> ";

    return new Promise(resolve => {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question(tips, answer => {
            rl.close();
            resolve(answer.trim());
        });
    });
}

// 执行
readSyncByRl().then(res => {
    const fileUrl = `http:${res}`;

    downPojectFile(fileUrl, function() {
        clearFileinfo(function() {
            console.log(">>>>> " + config.projectPath + "文件更新成功");
        });

        createrPreviewHtml(fileUrl, function() {
            console.log(">>>>> 预览页面(Html)创建成功", config.previewHtmlPath);
        });

        createrPreviewMD(fileUrl, function() {
            console.log(">>>>> 预览页面(MD)创建成功", config.previewMdPath);
        })
    });
});
```

```js
// config.js

/**
 * Iconfont快速更新
 * 注意: 路径相对于项目根目录
 */
module.exports = {
    //在项目中保存iconfont.js
    projectPath: "./src/assets/icons/iconfont.js",
    //Iconfont图标Html预览文件保存地址
    previewHtmlPath: "./tools/iconfont/preview/index.html",
    //Iconfont图标MarkDown预览文件保存地址
    //需要配合docsify使用 https://docsify.js.org/#/zh-cn/
    previewMdPath: "./tools/doc/docs/index.md"
}
```



#### 2、添加终端执行命令

```js
// package.json

{
  "scripts": {
    "icon:p": "serve ./tools/iconfont/preview",
    "icon:u": "node ./tools/iconfont/updata.js"
  },
}
```



#### 3、快速更新Iconfont.js方法

终端执行`npm run icon:u`后会提示你输入iconfont(Symbol)的cdn地址，输入完地址系统会自动下载最新文件到本地。

注意：1、iconfont使用Symbol引用方式，其他引用方式不支持 。2、url地址不要带http:/https: 即官方默认提供的地址就行。 

```shell
> npm run icon:u
> 请输入iconfont文件url地址://at.alicdn.com/t/font_595381_3g6pu90460h.js

项目文件保存成功 PATH:./src/assets/icons/iconfont.js
```



#### 4、预览当前使用的Svg图标库

终端执行`npm run icon:p`后即可访问当前项目所使用的Svg图标库。



### 使用Iconfont图标

这里推荐使用Iconfont-Symblo模式。



#### 1、从Iconfont下载文件到项目

从Iconfont官网将项目`iconfont.js`文件拷贝到`src/assets/icons/`下面，可以结合使用《Iconfont文件快速更新》方案。

```html
├── src		 				 	        	 
|   ├── assets
|   ├── icons
|   |   ├── iconfont.js
```



#### 2、封装IconSvg组件

```html
├── src		 				 	        	 
|   ├── components
|   |   ├── IconSvg.vue
```

```vue
// IconSvg.vue

<template>
  <svg :class="className" aria-hidden="true">
    <use :xlink:href="iconName"></use>
  </svg>
</template>

<script>
export default {
  name: "IconSvg",
  props: {
    name: {
      type: String,
      required: true
    }
  },
  computed: {
    iconName() {
      return `#${this.name}`;
    },
    className() {
      return `icon ${this.name}`;
    }
  }
};
</script>

<style scoped>
.icon {
  width: 1em;
  height: 1em;
  vertical-align: -0.15em;
  fill: currentColor;
  overflow: hidden;
}
</style>
```



#### 3、项目中引入

```js
// main.js

// 使用iconfont图标
import './assets/icons/iconfont.js'
import IconSvg from './components/IconSvg.vue'
// 全局使用IconSvg组件
Vue.component('icon-svg', IconSvg)
```



#### 4、项目中使用

```vue
<template>
	<icon-svg name="icon-item"></icon-svg>
</template>
```



### 移动端适配

#### 1、弹性布局方案lib-flexible

安装依赖：`npm install lib-flexible`   

项目中导入文件

```js
// main.js

import "lib-flexible";
```



#### 2、安装px转rem插件

安装依赖：`npm install -D postcss-px2rem`

```js
// vue.config.js

module.exports = {

  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-px2rem')({
            remUnit: 75   // 换算的基数(750的设计稿)
          }) 
        ]
      }
    }
  }

}
```



### 用SASS管理media查询 

```scss
/**
 * media查询管理
 */

// 设备尺寸管理
$mediaBase: (
  // 小屏幕
  s: (max-width: 540px),
  // 中屏幕
  m: (max-width: 720px),
  // 大屏幕
  l: (max-width: 960px ),
  // 大屏幕1号
  xl: (max-width: 1140px)
);

/*
 * 设置media查询函数
 * 使用：@include media($key){ //css... }
 */
@mixin media($key) {
  @media #{inspect(map-get($mediaBase, $key))} {
    @content;
  }
}

```



### 常用Sass Minix代码块 

```scss
/**
 * 等腰三角形函数
 * @include triangle($w, $b, $h, $color);
 * @$w 方向 T, R, B, L
 * @$b 腰长 6px
 * @$h 高度 8px
 * @$color 颜色 #333
 */

@mixin triangle($w, $b, $h, $color) {
  position: absolute;
  width: 0;
  height: 0;
  content: "";
  border-style: solid;
  @if $w== "T" {
    border-width: 0 $b $h $b;
    border-color: transparent transparent $color transparent;
  }
  @if $w== "R" {
    border-width: $b 0 $b $h;
    border-color: transparent transparent transparent $color;
  }
  @if $w== "B" {
    border-width: $h $b 0 $b;
    border-color: $color transparent transparent transparent;
  }
  @if $w== "L" {
    border-width: $b $h $b 0;
    border-color: transparent $color transparent transparent;
  }
}


/**
 * 文本两端分散对其
 */

@mixin justify {
  text-align: justify;
  text-justify: distribute-all-lines;
  /*ie6-8*/
  text-align-last: justify;
  /* ie9*/
  -moz-text-align-last: justify;
  /*ff*/
  -webkit-text-align-last: justify;
  /*chrome 20+*/
}

/**
 * 设置文本不能被选中
 */

@mixin not-select-text {
  -webkit-touch-callout: none;
  /* iOS Safari */
  -webkit-user-select: none;
  /* Chrome/Safari/Opera */
  -khtml-user-select: none;
  /* Konqueror */
  -moz-user-select: none;
  /* Firefox */
  -ms-user-select: none;
  /* Internet Explorer/Edge */
  user-select: none;
  /* Non-prefixed version, currently
    not supported by any browser */
}

/**
 * CSS3居中定位
 * @include position($type);
 * @$type 定位类型 fixed/absoult
 */

@mixin position($type) {
  position: $type;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  /* IE 9 */
  -moz-transform: translate(-50%, -50%);
  /* Firefox */
  -webkit-transform: translate(-50%, -50%);
  /* Safari 和 Chrome */
  -o-transform: translate(-50%, -50%);
  /* Opera */
}

/**
 * 英文段落文本不截断换行
 */

@mixin wordwrap {
  word-break: keep-all;
  word-wrap: break-word;
  white-space: pre-wrap;
}

/**
 * 文本超出...
 */

@mixin ellipsis {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}

/**
 * 清除浮动
 */

@mixin clearfix() {
  &::after {
    display: block;
    clear: both;
    content: "";
  }
}

/**
 * placeholder颜色
 */

@mixin placeholder($color) {
  ::-webkit-input-placeholder {
    color: $color;
  }
  ::-moz-placeholder {
    color: $color;
  }
  ::-ms-input-placeholder {
    color: $color;
  }
  .placeholder {
    color: $color;
  }
}

```



### 事件总线EvenBus最佳注册方式

推荐使用方式一，方式一比方式二效率更高。

```js
// main.js

// 方式一(推荐，效率更高)：
new Vue({
    beforeCreate() {
        // 设置事件总线
        Vue.prototype.$bus = this
    },
    router,
    render: h => h(App),
}).$mount('#app')

// 方式二
Vue.prototype.$bus = new Vue();
```





