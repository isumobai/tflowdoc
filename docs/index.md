# 文档构建工具 

本文档由[docsify](https://docsify.js.org/#/zh-cn/)构建，



<div type="preview">
	<div class="demo">0000</div>
</div>

<div type="preview-tab">
	<tab>html</tab><tab>CSS</tab><tab>JS</tab>
</div>


<div type="preview-code">

```html
<div class="flex ...">
	<div class="flex-none w-16 h-16 ...">
		<!-- 111111This item will not grow -->
	</div>
	<div class="flex-grow h-16 ... flex-grow h-16 ... flex-grow h-16 ...flex-grow h-16 ...flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
</div>
```

</div>

<div type="preview-code">

```html
<div class="flex ...">
	<div class="flex-none w-16 h-16 ...">
		<!-- 222222This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
</div>
```

</div>

<div type="preview-code">

```html
<div class="flex ...">
	<div class="flex-none w-16 h-16 ...">
		<!-- 3333333This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
</div>
```

</div>




## 0000000




<div type="preview">
	<div class="demo">0000</div>
</div>

<div type="preview-tab">
	<tab>html</tab><tab>CSS</tab><tab>JS</tab>
</div>


<div type="preview-code">

```html
<div class="flex ...">
	<div class="flex-none w-16 h-16 ...">
		<!-- 111111This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
</div>
```

</div>

<div type="preview-code">

```html
<div class="flex ...">
	<div class="flex-none w-16 h-16 ...">
		<!-- 222222This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
</div>
```

</div>

<div type="preview-code">

```html
<div class="flex ...">
	<div class="flex-none w-16 h-16 ...">
		<!-- 3333333This item will not grow -->
	</div>
	<div class="flex-grow h-16 ...">
		<!-- This item will grow -->
	</div>
	<div class="flex-none w-16 h-16 ...">
		<!-- This item will not grow -->
	</div>
</div>
```

</div>