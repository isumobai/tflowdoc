# JS工具函数文档

> **小提示:** 可以`CTRL+F`搜索函数名哦!


## add


```js
_.add(augend, addend)
```

两个数相加。

### 参数:

+ `augend` (number) : 相加的第一个数。  

+ `addend` (number) : 相加的第二个数。  

### 例子:

```js
_.add(6, 4);
// => 10
```


[^.^ fixed-nav ]: 开启子导航页面浮动

[^.^ lodash-doc ]: 在此处插入lodash文档



## add


```js
_.add(augend, addend)
```

两个数相加。

### 参数:

+ `augend` (number) : 相加的第一个数。  

+ `addend` (number) : 相加的第二个数。  

### 例子:

```js
_.add(6, 4);
// => 10
```